/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   nrf24l01.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/ic/nrf24l01.h>
#include <string.h>
#include <ubeam/drivers/ic/nrf24l01_defs.h>

#ifdef __cplusplus
extern "C"
{
#endif

//! NRF24L01 mode
typedef enum
{
	NRFM_STANDBY,                       //!< Standby mode
	NRFM_RECEIVE,                       //!< Reception mode
	NRFM_TRANSMIT                       //!< Transmission mode
} NrfMode;

static NrfResult Command(Nrf24l01* nrf, uint8_t command, uint8_t* txBuffer, uint8_t* rxBuffer, uint8_t count)
{
    if (UBSpiMaster_GetState(nrf->spi) != UBSMS_IDLE)
        return NRFR_NOT_INITIALIZED;
    UBGpioPin_Clear(nrf->pinCsn);

    uint8_t rx;

    if (UBSpiMaster_TransceiveSync(nrf->spi, &command, &rx, 1) != UBSMR_OK)
    {
        UBGpioPin_Set(nrf->pinCsn);
        return NRFR_SPI_ERROR;
    }
    if (UBSpiMaster_TransceiveSync(nrf->spi, txBuffer, rxBuffer, count) != UBSMR_OK)
    {
		UBGpioPin_Set(nrf->pinCsn);
		return NRFR_SPI_ERROR;
    }
        UBGpioPin_Set(nrf->pinCsn);
    return NRFR_OK;
}

static NrfResult WriteRegisterMulti(Nrf24l01* nrf, uint8_t addr, uint8_t* value, uint8_t count)
{
    return Command(nrf, NRF_INSTR_WREGISTER | (addr & 0x1f), value, 0, count);
}

static NrfResult WriteRegister(Nrf24l01* nrf, uint8_t addr, uint8_t value)
{
    return Command(nrf, NRF_INSTR_WREGISTER | (addr & 0x1f), &value, 0, 1);
}

static NrfResult ReadRegister(Nrf24l01* nrf, uint8_t addr, uint8_t* value)
{
    return Command(nrf, addr & 0x1f, 0, value, 1);
}

static NrfResult BusyCheckAndLock(Nrf24l01* nrf)
{
	if (nrf == 0)
		return NRFR_NOT_CONFIGURED;

	if ((nrf->state == NRFS_BUSY))
		return NRFR_BUSY;

	if (!UBMutex_TryLock(&nrf->mutex))
		return NRFR_BUSY;
	return NRFR_OK;
}

static void SetMode(Nrf24l01* nrf, NrfMode mode)
{
    if (nrf->mode == mode)
        return;

    UBGpioPin_Clear(nrf->pinCe);

    if (mode == NRFM_RECEIVE)
    {
        if (nrf->ackPayload == false)
        {
            Command(nrf, NRF_INSTR_FLUSHRX, 0, 0, 0);
            WriteRegister(nrf, NRF_REG_STATUS, NRF_BIT_RX_DR | NRF_BIT_TX_DS | NRF_BIT_MAX_RT);
        }
        else
            WriteRegister(nrf, NRF_REG_STATUS, NRF_BIT_TX_DS | NRF_BIT_MAX_RT);

        // Recover pipe 0 address
        Nrf_SetPipeAddress(nrf, 0, nrf->address);

        uint8_t config;
        ReadRegister(nrf, NRF_REG_CONFIG, &config);
        config |= NRF_BIT_PRIM_RX;
        WriteRegister(nrf, NRF_REG_CONFIG, config);
        //UBSystemTime::WaitMicro(130);
        if (nrf->rxEnabled)
        {
        	UBGpioPin_Set(nrf->pinCe);
        }
    }
    else if (mode == NRFM_TRANSMIT)
    {
        uint8_t config;
        ReadRegister(nrf, NRF_REG_CONFIG, &config);
        config &= ~NRF_BIT_PRIM_RX;
        WriteRegister(nrf, NRF_REG_CONFIG, config);

        if (nrf->ackPayload)
        {
            Command(nrf, NRF_INSTR_FLUSHRX, 0, 0, 0);
            WriteRegister(nrf, NRF_REG_STATUS, NRF_BIT_RX_DR | NRF_BIT_TX_DS | NRF_BIT_MAX_RT);
        }
        else
            WriteRegister(nrf, NRF_REG_STATUS, NRF_BIT_TX_DS | NRF_BIT_MAX_RT);
    }
    else
    {
    	UBGpioPin_Clear(nrf->pinCe);

        uint8_t config;
        ReadRegister(nrf, NRF_REG_CONFIG, &config);
        config |= NRF_BIT_PRIM_RX;
        WriteRegister(nrf, NRF_REG_CONFIG, config);
    }
    nrf->mode = mode;
    return;
}

NrfResult ReadPayload(Nrf24l01* nrf, const uint8_t* buffer, uint8_t size)
{
    if (size)
        return Command(nrf, NRF_INSTR_RRXPAY, 0, (uint8_t*)buffer, size);
    return NRFR_BAD_PARAM;
}

void Nrf_Configure(Nrf24l01* nrf, UBSpiMaster* spi, UBGpioPin* pinCsn, UBGpioPin* pinCe)
{
    if ((nrf == 0) || (spi == 0) || (pinCsn == 0) || (pinCe == 0))
    {
        return;
    }

    UBMutex_Configure(&nrf->mutex, true, false);

    if (!UBMutex_TryLock(&nrf->mutex))
		return;

    nrf->spi = spi;
    nrf->pinCsn = pinCsn;
    nrf->pinCe = pinCe;
    nrf->state = NRFS_NOT_INITIALIZED;
    UBMutex_Unlock(&nrf->mutex);
}

NrfResult Nrf_Init(Nrf24l01* nrf, NrfAddress address, bool autoAck, bool dynamicPayload, NrfDataRate dataRate, bool ackPayload)
{
	if (nrf == 0)
		return NRFR_NOT_CONFIGURED;

	if ((nrf->state != NRFS_NOT_INITIALIZED) && (nrf->state != NRFS_BUSY))
		return NRFR_NOT_CONFIGURED;

	if (!UBMutex_TryLock(&nrf->mutex))
		return NRFR_BUSY;

	nrf->ackPayload = false;
	nrf->mode = NRFM_STANDBY;
	nrf->rxPollPeriod = UBSysTime_GetIntervalMilli(NRF24L01_DEFAULT_RX_POLL_PERIOD);
	nrf->rxEnabled = false;
	UBGpioPin_Output(nrf->pinCsn);
	UBGpioPin_Output(nrf->pinCe);
	UBGpioPin_Set(nrf->pinCsn);
	UBGpioPin_Clear(nrf->pinCe);

    // Check communication with chip
    NrfResult result = WriteRegister(nrf, NRF_REG_CONFIG, NRF_BIT_EN_CRC);

    if (result != NRFR_OK)
    {
        return result;
    }

    uint8_t temp;
    result = ReadRegister(nrf, NRF_REG_CONFIG, &temp);

    if ((result != NRFR_OK) || (temp != NRF_BIT_EN_CRC))
    {
        // Chip did not respond correctly
        return NRFR_SPI_ERROR;
    }

    result |= Nrf_EnablePower(nrf, true);
    memcpy(nrf->address, address, NRF24L01_ADDRESS_SIZE);

    result |= Nrf_SetPipeAddress(nrf, 0, address);
    result |= Nrf_EnableAutoAckAll(nrf, autoAck);
    result |= Nrf_EnableDynamicPayloadAll(nrf, dynamicPayload);
    result |= Nrf_SetDataRate(nrf, dataRate);
    result |= Nrf_EnableAckPayload(nrf, ackPayload);

    // Set maximal payload size by default for all pipes
    result |= Nrf_SetPipePayloadSizeAll(nrf, NRF24L01_MAX_PAYLOAD_SIZE);

    nrf->rxTimeStamp = UBSysTime_GetTimestamp() + nrf->rxPollPeriod;

    // Start in reception mode by default
    SetMode(nrf, NRFM_RECEIVE);

    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return NRFR_ERROR;
    }

    nrf->state = NRFS_IDLE;

	UBMutex_Unlock(&nrf->mutex);
    return NRFR_OK;
}

NrfResult Nrf_EnablePower(Nrf24l01* nrf, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    uint8_t config;
    result = ReadRegister(nrf, NRF_REG_CONFIG, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    if (enable)
        config |= NRF_BIT_PWR_UP;
    else
        config &= ~NRF_BIT_PWR_UP;

    result = WriteRegister(nrf, NRF_REG_CONFIG, config);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_EnableReception(Nrf24l01* nrf, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (nrf->mode == NRFM_RECEIVE)
    {
        if (enable)
            UBGpioPin_Set(nrf->pinCe);
        else
            UBGpioPin_Clear(nrf->pinCe);
    }

    nrf->rxEnabled = enable;
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_FlushReceptionFifo(Nrf24l01* nrf)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    result = Command(nrf, NRF_INSTR_FLUSHRX, 0, 0, 0);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_SetPipeAddress(Nrf24l01* nrf, uint8_t pipe, NrfAddress address)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (pipe >= NRF24L01_PIPE_COUNT)
    {
    	UBMutex_Unlock(&nrf->mutex);
        return NRFR_BAD_PARAM;
    }

    uint8_t reg = NRF_REG_RX_ADDR_P0 + pipe;
    if (pipe <= 1)
        result = WriteRegisterMulti(nrf, reg, &address[0], 5);
    else
        result = WriteRegister(nrf, reg, address[0]);

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_EnablePipeReception(Nrf24l01* nrf, uint8_t pipe, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (pipe >= NRF24L01_PIPE_COUNT)
    {
    	UBMutex_Unlock(&nrf->mutex);
        return NRFR_BAD_PARAM;
    }

    uint8_t config;

    result = ReadRegister(nrf, NRF_REG_EN_RXADDR, &config);
	if (result != NRFR_OK)
	{
		UBMutex_Unlock(&nrf->mutex);
		return result;
	}

    if (enable)
        config |= (1 << pipe);
    else
        config &= ~(1 << pipe);

    result = WriteRegister(nrf, NRF_REG_EN_RXADDR, config);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_SetPipePayloadSize(Nrf24l01* nrf, uint8_t pipe, uint8_t size)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (pipe >= NRF24L01_PIPE_COUNT)
    {
    	UBMutex_Unlock(&nrf->mutex);
        return NRFR_BAD_PARAM;
    }
    result = WriteRegister(nrf, NRF_REG_RX_PW_P0 + pipe, size);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_SetPipePayloadSizeAll(Nrf24l01* nrf, uint8_t size)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

	uint8_t data[5] = {size, size, size, size, size};

	result = WriteRegisterMulti(nrf, NRF_REG_RX_PW_P0, data, size);

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_EnableDynamicPayload(Nrf24l01* nrf, uint8_t pipe, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (pipe >= NRF24L01_PIPE_COUNT)
    {
    	UBMutex_Unlock(&nrf->mutex);
        return NRFR_BAD_PARAM;
    }

    uint8_t config;
    uint8_t feat;

    result = ReadRegister(nrf, NRF_REG_DYNPD, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    result = ReadRegister(nrf, NRF_REG_FEATURE, &feat);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    if (enable)
        config |= 1 << pipe;
    else
        config &= ~(1 << pipe);

    bool setSize;

    if (config)
    {
        setSize = true;
        feat |= NRF_BIT_EN_DPL;
    }
    else
    {
        setSize = false;
        feat &= ~NRF_BIT_EN_DPL;
    }

    result = WriteRegister(nrf, NRF_REG_FEATURE, feat);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    result = WriteRegister(nrf, NRF_REG_DYNPD, config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    if (setSize)
    {
        result = Nrf_SetPipePayloadSizeAll(nrf, NRF24L01_MAX_PAYLOAD_SIZE);
    }

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_EnableDynamicPayloadAll(Nrf24l01* nrf, bool enable)
{
	NrfResult result;
	result = Nrf_EnableDynamicPayload(nrf, 0, enable);
	if (result != NRFR_OK)
		return result;
	result = Nrf_EnableDynamicPayload(nrf, 1, enable);
	if (result != NRFR_OK)
		return result;
	result = Nrf_EnableDynamicPayload(nrf, 2, enable);
	if (result != NRFR_OK)
		return result;
	result = Nrf_EnableDynamicPayload(nrf, 3, enable);
	if (result != NRFR_OK)
		return result;
	result = Nrf_EnableDynamicPayload(nrf, 4, enable);

    return result;
}

NrfResult Nrf_EnableAckPayload(Nrf24l01* nrf, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    uint8_t feat;
    result = ReadRegister(nrf, NRF_REG_FEATURE, &feat);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    if (enable)
        feat |= NRF_BIT_EN_ACK_PAY;
    else
        feat &= ~NRF_BIT_EN_ACK_PAY;

    result = WriteRegister(nrf, NRF_REG_FEATURE, feat);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }
    nrf->ackPayload = enable;

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_ReadPayload(Nrf24l01* nrf, const uint8_t* buffer, uint8_t size)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (size)
        result = Command(nrf, NRF_INSTR_RRXPAY, 0, (uint8_t*)buffer, size);

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_WritePayload(Nrf24l01* nrf, const uint8_t* buffer, uint8_t size)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (size)
        result = Command(nrf, NRF_INSTR_WTXPAY, (uint8_t*)buffer, 0, size);

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_WriteAckPayload(Nrf24l01* nrf, uint8_t pipe, const uint8_t* buffer, uint8_t size)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (pipe >= NRF24L01_PIPE_COUNT)
    {
    	UBMutex_Unlock(&nrf->mutex);
        return NRFR_BAD_PARAM;
    }

    result = Command(nrf, NRF_INSTR_WACKPAY | pipe, (uint8_t*)buffer, 0, size);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_EnableAutoAck(Nrf24l01* nrf, uint8_t pipe, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (pipe >= NRF24L01_PIPE_COUNT)
    {
    	UBMutex_Unlock(&nrf->mutex);
        return NRFR_BAD_PARAM;
    }

    uint8_t config;

	result = ReadRegister(nrf, NRF_REG_EN_AA, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

	if (enable)
		config |= 1 << pipe;
	else
		config &= ~(1 << pipe);

	result = WriteRegister(nrf, NRF_REG_EN_AA, config);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_EnableAutoAckAll(Nrf24l01* nrf, bool enable)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    if (enable)
        result = WriteRegister(nrf, NRF_REG_EN_AA, 0x3f);
    else
        result = WriteRegister(nrf, NRF_REG_EN_AA, 0x00);

    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_SetOutputPower(Nrf24l01* nrf, NrfOutputPower power)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    uint8_t config;
    result = ReadRegister(nrf, NRF_REG_RF_SETUP, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    config &= 0xf9;
    switch(power)
    {
    case NRFOP_18DBM:
        break;
    case NRFOP_12DBM:
        config |= 0x02;
        break;
    case NRFOP_6DBM:
        config |= 0x04;
        break;
    case NRFOP_0DBM:
        config |= 0x06;
        break;
    }
    result = WriteRegister(nrf, NRF_REG_RF_SETUP, config);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfOutputPower Nrf_GetOutputPower(Nrf24l01* nrf)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return NRFOP_0DBM;

    NrfOutputPower power;
    uint8_t config;

    result = ReadRegister(nrf, NRF_REG_RF_SETUP, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return NRFOP_0DBM;
    }

    config &= 0x06;
    if (config == 0x00)
        power = NRFOP_18DBM;
    else if (config == 0x02)
        power = NRFOP_12DBM;
    else if (config == 0x04)
        power = NRFOP_6DBM;
    else
        power = NRFOP_0DBM;

    UBMutex_Unlock(&nrf->mutex);
    return power;
}

NrfResult Nrf_SetDataRate(Nrf24l01* nrf, NrfDataRate dataRate)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    uint8_t config;
    result = ReadRegister(nrf, NRF_REG_RF_SETUP, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    config &= ~(NRF_BIT_RF_DR_LOW | NRF_BIT_RF_DR_HIGH);
    switch(dataRate)
    {
    case NRFRT_250KBPS:
        config |= NRF_BIT_RF_DR_LOW;
        break;
    case NRFRT_1MBPS:
        break;
    case NRFRT_2MBPS:
        config |= NRF_BIT_RF_DR_HIGH;
        break;
    }
    result = WriteRegister(nrf, NRF_REG_RF_SETUP, config);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfDataRate Nrf_GetDataRate(Nrf24l01* nrf)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return NRFRT_250KBPS;

    NrfDataRate dataRate;
    uint8_t config;

    result = ReadRegister(nrf, NRF_REG_RF_SETUP, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return NRFRT_250KBPS;
    }

    if ((config & NRF_BIT_RF_DR_LOW) == 0)
        dataRate = NRFRT_250KBPS;
    else
    {
        if (config & NRF_BIT_RF_DR_HIGH)
            dataRate = NRFRT_2MBPS;
        else
            dataRate = NRFRT_1MBPS;
    }

    UBMutex_Unlock(&nrf->mutex);
    return dataRate;
}

NrfResult Nrf_SetCrcLength(Nrf24l01* nrf, NrfCrcLength crcLength)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    uint8_t config;
    result = ReadRegister(nrf, NRF_REG_CONFIG, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return NRFRT_250KBPS;
    }

    config &= ~(NRF_BIT_CRCO | NRF_BIT_EN_CRC);

    switch(crcLength)
    {
    case NRFCL_NONE:
        break;
    case NRFCL_1BYTE:
        config |= NRF_BIT_EN_CRC;
        break;
    case NRFCL_2BYTES:
        config |= NRF_BIT_EN_CRC | NRF_BIT_CRCO;
        break;
    }
    result = WriteRegister(nrf, NRF_REG_CONFIG, config);
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfCrcLength Nrf_GetCrcLength(Nrf24l01* nrf)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return NRFCL_NONE;

    NrfCrcLength crcLength;
    uint8_t config;
    result = ReadRegister(nrf, NRF_REG_CONFIG, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return NRFCL_NONE;
    }

    if ((config & NRF_BIT_EN_CRC) == 0)
        crcLength = NRFCL_NONE;
    else
    {
        if (config & NRF_BIT_CRCO)
            crcLength = NRFCL_2BYTES;
        else
            crcLength = NRFCL_1BYTE;
    }
    UBMutex_Unlock(&nrf->mutex);
    return crcLength;
}

NrfResult Nrf_SetRetrySettings(Nrf24l01* nrf, uint8_t retryDelay, uint8_t retryCount)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

	result = WriteRegister(nrf, NRF_REG_SETUP_RETR, (retryDelay & 0x0f) << 4 | (retryCount & 0x0f));
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_GetRetrySettings(Nrf24l01* nrf, uint8_t* retryDelay, uint8_t* retryCount)
{
	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    uint8_t config;

    result = ReadRegister(nrf, NRF_REG_SETUP_RETR, &config);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return result;
    }

    *retryDelay = config >> 4;
    *retryCount = config & 0x0f;
    UBMutex_Unlock(&nrf->mutex);

    return result;
}

NrfResult Nrf_Transmit(Nrf24l01* nrf, NrfAddress address, uint8_t* payload, uint8_t payloadSize, uint8_t* ackPayloadBuffer, uint8_t* ackPayloadSize)
{
	if (payload == NULL)
		return NRFR_BAD_PARAM;

	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    result = Command(nrf, NRF_INSTR_FLUSHTX, 0, 0, 0);

    result |= WriteRegisterMulti(nrf, NRF_REG_TX_ADDR, &address[0], 5);
    // We have to set pipe 0 address to TX addres to be able to receive ACK
    result |= Nrf_SetPipeAddress(nrf, 0, address);

    result |= Nrf_WritePayload(nrf, payload, payloadSize);

    SetMode(nrf, NRFM_TRANSMIT);
    if (result != NRFR_OK)
    {
    	UBMutex_Unlock(&nrf->mutex);
    	return NRFR_ERROR;
    }

    // Start transmission
    UBGpioPin_Set(nrf->pinCe);

    UBTimestamp ts = UBSysTime_GetFutureMilli(NRF24L01_TIMEOUT_TX);

    uint8_t status;
    bool timeout;
    do
    {
    	timeout = UBSysTime_HasPassed(ts);
        if (timeout)
            break;

        result |= ReadRegister(nrf, NRF_REG_STATUS, &status);

    } while ((status & (NRF_BIT_TX_DS | NRF_BIT_MAX_RT)) == 0);

    // Disable transmission
    UBGpioPin_Clear(nrf->pinCe);

    if ((timeout) || (status & NRF_BIT_MAX_RT))
    {
        // Recover RX mode
        SetMode(nrf, NRFM_RECEIVE);
        UBMutex_Unlock(&nrf->mutex);
        return NRFR_TIMEOUT;
    }

    // Get ACK payload when enabled
    if ((nrf->ackPayload) && (ackPayloadBuffer) && (ackPayloadSize))
    {
        *ackPayloadSize = 0;
        if ((status & NRF_BIT_RX_DR))
        {
            result |=Command(nrf, NRF_INSTR_RRXPAYWIDTH, 0, ackPayloadSize, 1);
            result |= Nrf_ReadPayload(nrf, ackPayloadBuffer, *ackPayloadSize);
        }
    }

    // Recover RX mode
    SetMode(nrf, NRFM_RECEIVE);
    UBMutex_Unlock(&nrf->mutex);

    return NRFR_OK;
}

NrfResult Nrf_Receive(Nrf24l01* nrf, uint8_t* pipe, uint8_t* payloadBuffer, uint8_t* payloadSize, UBTimeInterval timeout)
{
	if ((pipe == NULL) || (payloadBuffer == NULL) || (payloadSize == NULL))
		return NRFR_BAD_PARAM;

	NrfResult result = BusyCheckAndLock(nrf);
	if (result != NRFR_OK)
		return result;

    bool noTimeout = false;
    UBTimestamp ts;

    if (timeout == 0)
        noTimeout = true;
    else
        ts = UBSysTime_GetFuture(timeout);

    while(1)
    {
        if ((noTimeout == false) && UBSysTime_HasPassed(ts))
        {
        	UBMutex_Unlock(&nrf->mutex);
            return NRFR_TIMEOUT;
        }

        uint8_t status;
        result = ReadRegister(nrf, NRF_REG_STATUS, &status);
        if (result != NRFR_OK)
        {
        	UBMutex_Unlock(&nrf->mutex);
        	return result;
        }

        if (status & 0x80)
        {
        	UBMutex_Unlock(&nrf->mutex);
            return NRFR_SPI_ERROR;
        }

        // Check for data in FIFO
        *pipe = (status & 0x0e) >> 1;
        if (*pipe > NRF24L01_PIPE_COUNT)
            continue;

        result = Command(nrf, NRF_INSTR_RRXPAYWIDTH, 0, payloadSize, 1);
        if (result != NRFR_OK)
        {
        	UBMutex_Unlock(&nrf->mutex);
        	return result;
        }

        result = ReadPayload(nrf, payloadBuffer, *payloadSize);
        if (result != NRFR_OK)
        {
        	UBMutex_Unlock(&nrf->mutex);
        	return result;
        }

        // RX_DR must be set on successful transmission
        if ((status & NRF_BIT_RX_DR) == 0)
            continue;

        result = WriteRegister(nrf, NRF_REG_STATUS, NRF_BIT_RX_DR);
        if (result != NRFR_OK)
        {
        	UBMutex_Unlock(&nrf->mutex);
        	return result;
        }

        UBMutex_Unlock(&nrf->mutex);
        return NRFR_OK;
    }
}

#ifdef __cplusplus
}
#endif
