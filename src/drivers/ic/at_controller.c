/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   at_controller.c
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/ic/at_controller.h>
#include <string.h>
#include <ubeam/drivers/memory.h>

#ifdef __cplusplus
extern "C"
{
#endif

static AtCtrlCbDesc* GetCallback(AtCtrl* ctrl, char* command)
{
	for(uint8_t i = 0; i < ctrl->cbDescCount; i++)
	{
		if (strcmp(ctrl->cbDescs[i]->command, command) == 0)
			return ctrl->cbDescs[i];
	}
	return 0;
}

static void ProcessRaw(AtCtrl* ctrl)
{
	if (ctrl->rawDataLineSkip)
	{
		ctrl->rawDataLineSkip--;
		if (ctrl->rawDataLineSkip == 0)
			ctrl->state = ATCS_RAW_READ;
	}
	else
	{
		ctrl->state = ATCS_RAW_READ;
	}
    if ((ctrl->state == ATCS_RAW_READ) && (ctrl->atBufferCount))
    {
        if (ctrl->atBufferCount >= ctrl->rawDataRemaining)
        {
            memcpy(ctrl->rawData, ctrl->atBuffer, ctrl->rawDataRemaining);
            memmove(ctrl->atBuffer, ctrl->atBuffer + ctrl->rawDataRemaining, ctrl->atBufferCount - ctrl->rawDataRemaining);
            ctrl->atBufferCount -= ctrl->rawDataRemaining;
            ctrl->rawDataRemaining = 0;
            ctrl->state = ATCS_IDLE;
        }
        else
        {
            memcpy(ctrl->rawData, ctrl->atBuffer, ctrl->atBufferCount);
            ctrl->rawData += ctrl->atBufferCount;
            ctrl->rawDataRemaining -= ctrl->atBufferCount;
            ctrl->atBufferCount = 0;
            ctrl->atBuffer[0] = 0;
        }
    }
}

static void Process(UBThread* thread)
{
	AtCtrl* ctrl = UBThread_GetCustomData(thread);
	uint32_t cnt = UBFifo_GetCount(ctrl->uart->base.rxFifo);
	if (cnt)
	{
        if (ctrl->state == ATCS_RAW_READ)
        {
            uint32_t cnt = UBFifo_GetData(ctrl->uart->base.rxFifo, ctrl->rawData, ctrl->rawDataRemaining);
            ctrl->rawData += cnt;
            ctrl->rawDataRemaining -= cnt;
            if (ctrl->rawDataRemaining == 0)
            {
                ctrl->state = ATCS_COMMAND;
            }
        }
        else
        {
            if (ctrl->atBufferCount + cnt > ATCTRL_MAX_RESULT_LENGTH)
                cnt = ATCTRL_MAX_RESULT_LENGTH - ctrl->atBufferCount;
            UBFifo_GetData(ctrl->uart->base.rxFifo, (uint8_t*)&ctrl->atBuffer[ctrl->atBufferCount], cnt);
            ctrl->atBufferCount += cnt;
            if (ctrl->atBufferCount > ATCTRL_MAX_RESULT_LENGTH)
            {
            	ctrl->atBufferCount = ATCTRL_MAX_RESULT_LENGTH;
            }
            // Null char is unwanted - filter it out
            //for(uint16_t i = 0; i < ctrl->atBufferCount; i++)
            //    if (ctrl->atBuffer[i] == 0)
            //        ctrl->atBuffer[i] = '-';
            ctrl->atBuffer[ctrl->atBufferCount] = 0;
            char* pos = strstr(ctrl->atBuffer, "\r\n");
            while (pos)
            {
                *pos = 0;
                strcat(ctrl->atResult, ctrl->atBuffer);
                pos++;
                //uint16_t prevCnt = ctrl->atBufferCount;
                uint16_t n = (pos - ctrl->atBuffer + 1);
                if (ctrl->atBufferCount >= n)
                	ctrl->atBufferCount = ctrl->atBufferCount - n;
                else
                	ctrl->atBufferCount = 0;
                if (ctrl->atBufferCount > ATCTRL_MAX_RESULT_LENGTH)
                {
                	ctrl->atBufferCount = ATCTRL_MAX_RESULT_LENGTH;
                }
                memmove(ctrl->atBuffer, pos + 1, ctrl->atBufferCount);
                if (strlen(ctrl->atResult))
                {
                    switch(ctrl->state)
                    {
                    case ATCS_WAIT_FOR_TURN_ON:
                        if (strcmp(ctrl->atResult, "OK") == 0)
                        {
                            ctrl->state = ATCS_IDLE;
                            ctrl->atBuffer[0] = 0;
                            ctrl->atBufferCount = 0;
                        }
                        break;
                    case ATCS_IDLE:
                    {
                        pos = strchr(ctrl->atResult, '=');
                        char* pos1 = strchr(ctrl->atResult, ':');
                        if (pos == 0)
                            pos = pos1;
                        if ((pos1) && (pos1 < pos))
                            pos = pos1;
                        if (pos)
                            *pos = 0;
                        AtCtrlCbDesc* desc = GetCallback(ctrl, ctrl->atResult);
                        //ctrl->state = ATCS_IDLE_LINE;
                        ctrl->currentDesc = desc;
                        ctrl->line = 0;
                        ctrl->timeout = UBSysTime_GetFutureMilli(ATCTRL_RESP_TIMEOUT_MILLI);
                        if ((pos) && (ctrl->syncResult))
                        {
                            strncpy(ctrl->syncResult, ctrl->atResult, ctrl->syncResultLength);
                        }
                        if (ctrl->currentDesc)
                        {
                            if (pos)
                                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, pos + 1, (uint16_t)strlen(pos + 1), ctrl->line, ATCR_ASYNC_DATA);
                            else
                                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, &ctrl->atResult[strlen(ctrl->atResult)], 0, ctrl->line, ATCR_ASYNC_DATA);
                            ctrl->currentDesc = 0;
                        }
                        break;
                    }
                    case ATCS_COMMAND:
                        ctrl->line = 0;
                        ctrl->state = ATCS_COMMAND_LINE;
                        if (ctrl->syncResult)
                        {
                            strncpy(ctrl->syncResult, ctrl->atResult, ctrl->syncResultLength);
                        }
                        if (((ctrl->isAtCustomResultStrict) && (strcmp(ctrl->atResult, ctrl->atCustomResult) == 0)) ||
                            ((!ctrl->isAtCustomResultStrict) && (strstr(ctrl->atResult, ctrl->atCustomResult) == ctrl->atResult)))
                        {
                            if (ctrl->syncResult)
                            {
                                strncpy(ctrl->syncResult, ctrl->atResult, ctrl->syncResultLength);
                            }
                            *ctrl->atResult = 0;
                            strcpy(ctrl->atCustomResult, "OK");
                            if (ctrl->currentDesc)
                                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, ctrl->atResult, 0, ctrl->line, ATCR_OK);
                            ctrl->state = ATCS_IDLE;
                            ctrl->currentDesc = 0;
                        }
                        else if (ctrl->rawData)
                        {
                        	ProcessRaw(ctrl);
                        }
                        else
                        {
                            if (ctrl->syncResult)
                            {
                                strncpy(ctrl->syncResult, ctrl->atResult, ctrl->syncResultLength);
                            }
                            if (ctrl->currentDesc)
                            {
                                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, ctrl->atResult, (uint16_t)strlen(ctrl->atResult), ctrl->line, ATCR_ASYNC_DATA);
                            }
                            if (ctrl->rawData)
                            {
                            	ProcessRaw(ctrl);
                            }
                            ctrl->line++;
                        }
                        break;
                    case ATCS_IDLE_LINE:
                    case ATCS_COMMAND_LINE:
                        if (strcmp(ctrl->atResult, ctrl->atCustomResult) == 0)
                        {
                            if (ctrl->currentDesc)
                                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, ctrl->atResult, 0, ctrl->line, ATCR_OK);
                            strcpy(ctrl->atCustomResult, "OK");
                            ctrl->state = ATCS_IDLE;
                            ctrl->currentDesc = 0;
                            *ctrl->atResult = 0;
                        }
                        else if (ctrl->rawData)
                        {
                        	ProcessRaw(ctrl);
                        }
                        else
                        {
                            if (ctrl->syncResult)
                            {
                                strncpy(ctrl->syncResult, ctrl->atResult, ctrl->syncResultLength);
                            }
                            if (ctrl->currentDesc)
                            {
                                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, ctrl->atResult, (uint16_t)strlen(ctrl->atResult), ctrl->line, ATCR_ASYNC_DATA);
                            }
                            if (ctrl->rawData)
                            {
                            	ProcessRaw(ctrl);
                            }
                            ctrl->line++;
                        }
                        break;
                    default:
                        break;
                    }
                }

                if (ctrl->state == ATCS_RAW_READ)
                	break;

                ctrl->atResult[0] = 0;
                pos = strstr(ctrl->atBuffer, "\r\n");
            }
        }
	}
	switch(ctrl->state)
	{
	case ATCS_IDLE_LINE:
    case ATCS_COMMAND_LINE:
	case ATCS_COMMAND:
    case ATCS_RAW_READ:
		if (UBSysTime_HasPassed(ctrl->timeout))
		{
			ctrl->state = ATCS_IDLE;
			ctrl->atBufferCount = 0;
			ctrl->wasError = true;
            ctrl->rawDataRemaining = 0;
            ctrl->rawDataLineSkip = 0;
            strcpy(ctrl->atCustomResult, "OK");
            if ((ctrl->currentDesc) && (ctrl->currentDesc->callback))
                ((AtCtrlCallback*)ctrl->currentDesc->callback)(ctrl, ctrl->currentDesc, ctrl->atResult, 0, ctrl->line, ATCR_TIMEOUT);
            ctrl->currentDesc = 0;
        }
		*ctrl->atResult = 0;
		break;
	default:
		break;
	}
}

void AtCtrl_Configure(AtCtrl* ctrl, UBUart* uart, UBFifo* txMonitor)
{
    if ((ctrl == 0) || (uart == 0))
    {
        return;
    }

    UBMutex_Configure(&ctrl->mutex, true, false);

    if (!UBMutex_TryLock(&ctrl->mutex))
		return;

    UBThread_Configure(&ctrl->thread, 0, 0, 0, Process);
    UBThread_SetCustomData(&ctrl->thread, ctrl);
    ctrl->uart = uart;
    ctrl->txMonitor = txMonitor;
    ctrl->customData = 0;
   	ctrl->state = ATCS_NOT_INITIALIZED;
    ctrl->cbDescs = 0;
    ctrl->cbDescCount = 0;
    ctrl->atBufferCount = 0;
    ctrl->atResult[0] = 0;
    ctrl->currentDesc = 0;
    strcpy(ctrl->atCustomResult, "OK");
    UBThread_Start(&ctrl->thread);
    UBMutex_Unlock(&ctrl->mutex);
}

void AtCtrl_SetCustomData(AtCtrl* ctrl, void* customData)
{
	if (ctrl == 0)
		return;
	ctrl->customData = customData;
}

void* AtCtrl_GetCustomData(const AtCtrl* ctrl)
{
	if (ctrl == 0)
		return 0;
	return ctrl->customData;
}

AtCtrlState AtCtrl_GetState(const AtCtrl* ctrl)
{
	if (ctrl == 0)
        return ATCS_NOT_CONFIGURED;
	return ctrl->state;
}

AtCtrlResult AtCtrl_Init(AtCtrl* ctrl, UBFrequency baudrate, bool forceIdle)
{
    if ((ctrl == 0) || (ctrl->state != ATCS_NOT_INITIALIZED))
    {
        return ATCR_NOT_CONFIGURED;
    }

    if (!UBMutex_TryLock(&ctrl->mutex))
		return ATCR_BUSY;

    if (UBUart_Init(ctrl->uart, baudrate) != UBUR_OK)
    	return ATCR_UART_ERROR;

    if (forceIdle)
    	ctrl->state = ATCS_IDLE;
    else
    	ctrl->state = ATCS_WAIT_FOR_TURN_ON;

    UBMutex_Unlock(&ctrl->mutex);
    return ATCR_OK;
}

AtCtrlResult AtCtrl_EnterSoftwareReset(AtCtrl* ctrl)
{
    if ((ctrl == 0) || (ctrl->state != ATCS_NOT_INITIALIZED))
    {
        return ATCR_NOT_CONFIGURED;
    }

    if (!UBMutex_TryLock(&ctrl->mutex))
		return ATCR_BUSY;

   	ctrl->state = ATCS_WAIT_FOR_TURN_ON;
    ctrl->atBufferCount = 0;
    ctrl->atResult[0] = 0;

    UBMutex_Unlock(&ctrl->mutex);
    return ATCR_OK;
}

AtCtrlResult AtCtrl_HookCallback(AtCtrl* ctrl, const char* command, AtCtrlCallback* callback)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if (callback == 0)
    	return ATCR_BAD_PARRAM;

    if (strlen(command) > ATCTRL_MAX_COMMAND_LENGTH)
    	return ATCR_BAD_PARRAM;

    if (!UBMutex_TryLock(&ctrl->mutex))
		return ATCR_BUSY;

    AtCtrlCbDesc** newDescList = (AtCtrlCbDesc**)UBMemAlloc(sizeof(AtCtrlCbDesc*) * (ctrl->cbDescCount + 1));
    if (newDescList == 0)
    {
    	UBMutex_Unlock(&ctrl->mutex);
    	return ATCR_ERROR;
    }

    AtCtrlCbDesc* desc = (AtCtrlCbDesc*)UBMemAlloc(sizeof(AtCtrlCbDesc));
    if (desc == 0)
    {
    	UBMemFree(newDescList);
    	UBMutex_Unlock(&ctrl->mutex);
    	return ATCR_ERROR;
    }

    if (ctrl->cbDescs)
    {
		for(uint8_t i = 0; i < ctrl->cbDescCount; i++)
			newDescList[i] = ctrl->cbDescs[i];
    }

    desc->callback = callback;
    strcpy(desc->command, command);

    newDescList[ctrl->cbDescCount] = desc;
    AtCtrlCbDesc** oldDescList = ctrl->cbDescs;
    ctrl->cbDescs = newDescList;
    ctrl->cbDescCount++;
    UBMemFree(oldDescList);

    UBMutex_Unlock(&ctrl->mutex);
    return ATCR_OK;
}

AtCtrlResult AtCtrl_SetCustomOkResult(AtCtrl* ctrl, const char* result, bool isStrict)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;
    strcpy(ctrl->atCustomResult, result);
    ctrl->isAtCustomResultStrict = isStrict;
    return ATCR_OK;
}

AtCtrlResult AtCtrl_PrepareRawRead(AtCtrl* ctrl, uint8_t* buffer, uint16_t sizeToRead, uint8_t linesToSkip)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if ((buffer == 0) || (sizeToRead == 0))
        return ATCR_BAD_PARRAM;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if (ctrl->state != ATCS_IDLE)
        return ATCR_BUSY;

    if (!UBMutex_TryLock(&ctrl->mutex))
        return ATCR_BUSY;

    ctrl->rawData = buffer;
    ctrl->rawDataRemaining = sizeToRead;
    ctrl->rawDataLineSkip = linesToSkip;

    UBMutex_Unlock(&ctrl->mutex);
    return ATCR_OK;
}

AtCtrlResult AtCtrl_PrepareRawReadUnsafe(AtCtrl* ctrl, uint8_t* buffer, uint16_t sizeToRead, uint8_t linesToSkip)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if ((buffer == 0) || (sizeToRead == 0))
        return ATCR_BAD_PARRAM;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if (!UBMutex_TryLock(&ctrl->mutex))
        return ATCR_BUSY;

    ctrl->rawData = buffer;
    ctrl->rawDataRemaining = sizeToRead;
    ctrl->rawDataLineSkip = linesToSkip;

    UBMutex_Unlock(&ctrl->mutex);
    return ATCR_OK;
}

AtCtrlResult AtCtrl_TerminateRawRead(AtCtrl* ctrl)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if (!UBMutex_TryLock(&ctrl->mutex))
        return ATCR_BUSY;

    ctrl->rawData = 0;
    ctrl->rawDataRemaining = 0;
    ctrl->rawDataLineSkip = 0;

    UBMutex_Unlock(&ctrl->mutex);
    return ATCR_OK;
}

AtCtrlResult AtCtrl_SendAtPartial(AtCtrl* ctrl, bool isEnd, bool sendCrlf, const char* command, char* result, uint16_t maxResultLength, UBTimeInterval timeout)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if ((ctrl->state != ATCS_COMMAND) && (ctrl->state != ATCS_IDLE))
        return ATCR_BUSY;

    if (command == 0)
    	return ATCR_BAD_PARRAM;

    if (!UBMutex_TryLock(&ctrl->mutex))
		return ATCR_BUSY;

    ctrl->state = ATCS_COMMAND;
    if (result)
    	*result = 0;
    ctrl->syncResult = result;
    ctrl->syncResultLength = maxResultLength;

    const uint8_t* ptr = (const uint8_t*)command;
    uint16_t remaining = (uint16_t)strlen(command);
    while(remaining)
    {
		uint32_t enq;
		UBUart_Enqueue(ctrl->uart, ptr, remaining, &enq);
		if (ctrl->txMonitor)
			UBFifo_Put(ctrl->txMonitor, ptr, remaining);
		UBUart_Transmit(ctrl->uart);
		if (remaining >= enq)
			remaining -= enq;
		ptr += enq;
    }

    while(UBUart_TxFifoAvailable(ctrl->uart) < 2)
    {
    	UBThread_Yield();
    }
    if ((isEnd) && (sendCrlf))
    	UBUart_Enqueue(ctrl->uart, (const uint8_t*)"\r\n", 2, 0);
    if ((ctrl->txMonitor) && (isEnd) && (sendCrlf))
    {
    	UBFifo_Put(ctrl->txMonitor, (const uint8_t*)"\r\n", 2);
    }
	UBUart_Transmit(ctrl->uart);

    ctrl->wasError = false;
	if (isEnd)
	{
		ctrl->timeout = UBSysTime_GetTimestamp() + timeout;

		while(ctrl->state != ATCS_IDLE)
		{
			UBThread_Yield();
		}
	    ctrl->syncResult = 0;
	    if (ctrl->wasError)
	    {
	    	UBMutex_Unlock(&ctrl->mutex);
	    	return ATCR_TIMEOUT;
	    }
	}
	UBMutex_Unlock(&ctrl->mutex);
	return ATCR_OK;
}

AtCtrlResult AtCtrl_SendAtSync(AtCtrl* ctrl, const char* command, char* result, uint16_t maxResultLength, UBTimeInterval timeout)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if (ctrl->state != ATCS_IDLE)
        return ATCR_BUSY;

    if (command == 0)
    	return ATCR_BAD_PARRAM;

    if (!UBMutex_TryLock(&ctrl->mutex))
		return ATCR_BUSY;

    ctrl->state = ATCS_COMMAND;
    if (ctrl->rawDataRemaining)
    {
        if (ctrl->rawDataLineSkip == 0)
            ctrl->state = ATCS_RAW_READ;
    }
    if (result)
    	*result = 0;
    ctrl->syncResult = result;
    ctrl->syncResultLength = maxResultLength;

    uint32_t remaining = (uint32_t)strlen(command);
    uint32_t ofs = 0;
    while(remaining)
    {
        uint32_t enqueued;
        UBUart_Enqueue(ctrl->uart, (const uint8_t*)command + ofs, (uint16_t)remaining, &enqueued);
        UBUart_Transmit(ctrl->uart);
        remaining -= enqueued;
        ofs += enqueued;
        if (remaining)
        {
            while(UBUart_TxFifoAvailable(ctrl->uart) == 0)
            {
//                UBThread_Yield();
            }
        }
    }
    while(UBUart_TxFifoAvailable(ctrl->uart) < 3)
    {
//        UBThread_Yield();
    }
    UBUart_Enqueue(ctrl->uart, (const uint8_t*)"\r\n", 2, 0);
    UBUart_Transmit(ctrl->uart);
    if (ctrl->txMonitor)
    {
        UBFifo_Put(ctrl->txMonitor, (const uint8_t*)command, (uint16_t)strlen(command));
        UBFifo_Put(ctrl->txMonitor, (const uint8_t*)"\r\n", 2);
    }

    ctrl->wasError = false;
    ctrl->timeout = UBSysTime_GetTimestamp() + timeout;

    while(ctrl->state != ATCS_IDLE)
    {
    	UBThread_Yield();
    }

    ctrl->syncResult = 0;
    if (ctrl->wasError)
    {
    	UBMutex_Unlock(&ctrl->mutex);
    	return ATCR_TIMEOUT;
    }
    else
    {
    	UBMutex_Unlock(&ctrl->mutex);
    	return ATCR_OK;
    }
}

AtCtrlResult AtCtrl_SendAtAsync(AtCtrl* ctrl, const char* command, AtCtrlCallback* callback, UBTimeInterval timeout)
{
    if (ctrl == 0)
        return ATCR_NOT_CONFIGURED;

    if (ctrl->state <= ATCS_NOT_INITIALIZED)
        return ATCR_NOT_INITIALIZED;

    if (ctrl->state != ATCS_IDLE)
        return ATCR_BUSY;

    if (command == 0)
    	return ATCR_BAD_PARRAM;

    if (!UBMutex_TryLock(&ctrl->mutex))
		return ATCR_BUSY;

    ctrl->state = ATCS_COMMAND;
    if (ctrl->rawDataRemaining)
    {
        if (ctrl->rawDataLineSkip == 0)
            ctrl->state = ATCS_RAW_READ;
    }

    ctrl->asyncDesc.command[0] = 0;
    ctrl->asyncDesc.callback = callback;
    ctrl->currentDesc = &ctrl->asyncDesc;
    ctrl->wasError = false;
    ctrl->timeout = UBSysTime_GetTimestamp() + timeout;

    UBUart_Enqueue(ctrl->uart, (const uint8_t*)command, (uint16_t)strlen(command), 0);
    UBUart_Enqueue(ctrl->uart, (const uint8_t*)"\r\n", 2, 0);
    UBUart_Transmit(ctrl->uart);

    UBMutex_Unlock(&ctrl->mutex);

    return ATCR_OK;
}

#ifdef __cplusplus
}
#endif
