/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   dht12.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/ic/dht12.h>

#ifdef __cplusplus
extern "C"
{
#endif

void Dht12_Configure(Dht12* dht, UBI2cMaster* i2c)
{
      if ((dht == 0) || (i2c == 0))
    {
        return;
    }

    UBMutex_Configure(&dht->mutex, true, false);

    if (!UBMutex_TryLock(&dht->mutex))
        return;

    dht->i2c = i2c;
    dht->state = DHT12S_NOT_INITIALIZED;
    UBMutex_Unlock(&dht->mutex);
}

Dht12Result Dht12_Init(Dht12* dht)
{
    if (dht == 0)
        return DHT12R_NOT_CONFIGURED;

    if ((dht->state != DHT12S_NOT_INITIALIZED) && (dht->state != DHT12S_BUSY))
        return DHT12R_NOT_CONFIGURED;

    if (!UBMutex_TryLock(&dht->mutex))
        return DHT12R_BUSY;
    
    dht->state = DHT12S_IDLE;
    UBMutex_Unlock(&dht->mutex);
    return DHT12R_OK;
}

Dht12Result Dht12_GetTempHumSync(Dht12* dht, int16_t* temperature, uint16_t* humidity)
{
    if (dht == 0)
        return DHT12R_NOT_CONFIGURED;

    if (dht->state == DHT12S_NOT_CONFIGURED)
        return DHT12R_NOT_CONFIGURED;

    if (dht->state == DHT12S_NOT_INITIALIZED)
        return DHT12R_NOT_INITIALIZED;

    if (dht->state != DHT12S_IDLE)
        return DHT12R_BUSY;

    if (!UBMutex_TryLock(&dht->mutex))
        return DHT12R_BUSY;
    
    if (!UBMutex_TryLock(&dht->i2c->base.peri.mutex))
    {
        UBMutex_Unlock(&dht->mutex);
        return DHT12R_I2C_ERROR;
    }
        
    uint8_t data[5];
    data[0] = 0;
    if (UBI2cMaster_WriteSync(dht->i2c, 0x5c, data, 1) != UBI2MR_OK)
    {
        UBMutex_Unlock(&dht->i2c->base.peri.mutex);
        UBMutex_Unlock(&dht->mutex);
        return DHT12R_I2C_ERROR;
    }
    if (UBI2cMaster_ReadSync(dht->i2c, 0x5c, data, 5)  != UBI2MR_OK)
    {
        UBMutex_Unlock(&dht->i2c->base.peri.mutex);
        UBMutex_Unlock(&dht->mutex);
        return DHT12R_I2C_ERROR;
    }
    
    if (temperature)
    {
        *temperature = data[2] * 10 + data[3];
    }

    if (humidity)
    {
        *humidity = data[0] * 10 + data[1];
    }
    
    UBMutex_Unlock(&dht->i2c->base.peri.mutex);
    UBMutex_Unlock(&dht->mutex);
    return DHT12R_OK;
}

#ifdef __cplusplus
}
#endif
