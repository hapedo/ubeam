/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   nmea_controller.c
 * \brief  microBeam SDK
 * \date   08.01.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/ic/nmea_controller.h>
#include <string.h>
#include <ubeam/drivers/memory.h>

#ifdef __cplusplus
extern "C"
{
#endif

static NmeaCtrlCbDesc* GetCallback(NmeaCtrl* ctrl, char* command)
{
	for(uint8_t i = 0; i < ctrl->cbDescCount; i++)
	{
		if (strcmp(ctrl->cbDescs[i]->command, command) == 0)
			return ctrl->cbDescs[i];
	}
	return 0;
}

static void Process(UBThread* thread)
{
	NmeaCtrl* ctrl = UBThread_GetCustomData(thread);
	uint32_t cnt = UBFifo_GetCount(ctrl->uart->base.rxFifo);
	if (cnt)
	{
		if (ctrl->nmeaBufferCount + cnt > NMEACTRL_MAX_RESULT_LENGTH)
			cnt = NMEACTRL_MAX_RESULT_LENGTH - ctrl->nmeaBufferCount;
		UBFifo_GetData(ctrl->uart->base.rxFifo, (uint8_t*)&ctrl->nmeaBuffer[ctrl->nmeaBufferCount], cnt);
		ctrl->nmeaBufferCount += cnt;
		ctrl->nmeaBuffer[ctrl->nmeaBufferCount] = 0;
		// Check for overflow
		if (ctrl->nmeaBufferCount >= NMEACTRL_MAX_RESULT_LENGTH)
		{
			// Overflow - clear buffer and return. This should not happen
			ctrl->nmeaBufferCount = 0;
			return;
		}
		// Null char is unwanted - filter it out
		for(uint16_t i = 0; i < ctrl->nmeaBufferCount; i++)
			if (ctrl->nmeaBuffer[i] == 0)
				ctrl->nmeaBuffer[i] = '-';
		char* posStart = strchr(ctrl->nmeaBuffer, '$');
		char* posEnd = strstr(ctrl->nmeaBuffer, "\r\n");
		char* lastEnd = posEnd;
		while ((posStart) && (posEnd) && (posEnd > posStart))
		{
			memcpy(ctrl->nmeaResult, posStart, posEnd - posStart + 1);
			ctrl->nmeaResult[posEnd - posStart + 1] = 0;
			if (strlen(ctrl->nmeaResult))
			{
				char* args[NMEACTRL_MAX_ARG_COUNT];
				for(uint8_t i = 0; i < NMEACTRL_MAX_ARG_COUNT; i++)
					args[i] = 0;
				uint8_t cnt = 1;
				args[0] = ctrl->nmeaResult;
				char* pos = strchr(ctrl->nmeaResult, ',');
				if (pos == 0)
					pos = strchr(ctrl->nmeaResult, '\r');
				if (pos == 0)
					continue;
				*pos = 0;
				pos++;
				do
				{
					char* argEnd = strchr(pos, ',');
					if (argEnd == 0)
						argEnd = strchr(pos, '\r');
					if (argEnd == 0)
						break;
					*argEnd = 0;
					args[cnt] = pos;
					cnt++;
					pos = argEnd + 1;
					if (cnt >= NMEACTRL_MAX_ARG_COUNT)
						break;
				} while(pos);
				NmeaCtrlCbDesc* desc = GetCallback(ctrl, args[0]);
				if (desc)
					((NmeaCtrlCallback*)desc->callback)(ctrl, desc, (const char**)&args, cnt);
			}
			posStart = strchr(posEnd + 2, '$');
			posEnd = strstr(posEnd + 2, "\r\n");
			if (posEnd)
				lastEnd = posEnd;
		}
		if (lastEnd)
		{
			memcpy(ctrl->nmeaBuffer, lastEnd + 2, NMEACTRL_MAX_RESULT_LENGTH - (lastEnd - ctrl->nmeaBuffer) - 2);
            if (ctrl->nmeaBufferCount >= (uint16_t)(lastEnd - ctrl->nmeaBuffer) + 2)
                ctrl->nmeaBufferCount -= (uint16_t)(lastEnd - ctrl->nmeaBuffer) + 2;
			else
				ctrl->nmeaBufferCount = 0;
		}
	}
}

void NmeaCtrl_Configure(NmeaCtrl* ctrl, UBUart* uart)
{
    if ((ctrl == 0) || (uart == 0))
    {
        return;
    }

    UBMutex_Configure(&ctrl->mutex, true, false);

    if (!UBMutex_TryLock(&ctrl->mutex))
		return;

    UBThread_Configure(&ctrl->thread, 0, 0, 0, Process);
    UBThread_SetCustomData(&ctrl->thread, ctrl);
    ctrl->uart = uart;
    ctrl->customData = 0;
   	ctrl->state = NMEACS_NOT_INITIALIZED;
    ctrl->cbDescs = 0;
    ctrl->cbDescCount = 0;
    ctrl->nmeaBufferCount = 0;
    UBThread_Start(&ctrl->thread);
    UBMutex_Unlock(&ctrl->mutex);
}

void NmeaCtrl_SetCustomData(NmeaCtrl* ctrl, void* customData)
{
	if (ctrl == 0)
		return;
	ctrl->customData = customData;
}

void* NmeaCtrl_GetCustomData(const NmeaCtrl* ctrl)
{
	if (ctrl == 0)
		return 0;
	return ctrl->customData;
}

NmeaCtrlState NmeaCtrl_GetState(const NmeaCtrl* ctrl)
{
	if (ctrl == 0)
		return NMEACR_NOT_CONFIGURED;
	return ctrl->state;
}

NmeaCtrlResult NmeaCtrl_Init(NmeaCtrl* ctrl, UBFrequency baudrate)
{
    if ((ctrl == 0) || (ctrl->state != NMEACS_NOT_INITIALIZED))
    {
        return NMEACR_NOT_CONFIGURED;
    }

    if (!UBMutex_TryLock(&ctrl->mutex))
		return NMEACR_BUSY;

    if (UBUart_Init(ctrl->uart, baudrate) != UBUR_OK)
    	return NMEACR_UART_ERROR;

    ctrl->state = NMEACS_IDLE;

    UBMutex_Unlock(&ctrl->mutex);
    return NMEACR_OK;
}

NmeaCtrlResult NmeaCtrl_HookCallback(NmeaCtrl* ctrl, const char* command, NmeaCtrlCallback* callback)
{
    if (ctrl == 0)
        return NMEACR_NOT_CONFIGURED;

    if (ctrl->state <= NMEACS_NOT_INITIALIZED)
        return NMEACR_NOT_INITIALIZED;

    if (callback == 0)
    	return NMEACR_BAD_PARRAM;

    if (strlen(command) > NMEACTRL_MAX_COMMAND_LENGTH)
    	return NMEACR_BAD_PARRAM;

    if (!UBMutex_TryLock(&ctrl->mutex))
		return NMEACR_BUSY;

    NmeaCtrlCbDesc** newDescList = (NmeaCtrlCbDesc**)UBMemAlloc(sizeof(NmeaCtrlCbDesc*) * (ctrl->cbDescCount + 1));
    if (newDescList == 0)
    {
    	UBMutex_Unlock(&ctrl->mutex);
    	return NMEACR_ERROR;
    }

    NmeaCtrlCbDesc* desc = (NmeaCtrlCbDesc*)UBMemAlloc(sizeof(NmeaCtrlCbDesc));
    if (desc == 0)
    {
    	UBMemFree(newDescList);
    	UBMutex_Unlock(&ctrl->mutex);
    	return NMEACR_ERROR;
    }

    if (ctrl->cbDescs)
    {
		for(uint8_t i = 0; i < ctrl->cbDescCount; i++)
			newDescList[i] = ctrl->cbDescs[i];
    }

    desc->callback = callback;
    strcpy(desc->command, command);

    newDescList[ctrl->cbDescCount] = desc;
    NmeaCtrlCbDesc** oldDescList = ctrl->cbDescs;
    ctrl->cbDescs = newDescList;
    ctrl->cbDescCount++;
    UBMemFree(oldDescList);

    UBMutex_Unlock(&ctrl->mutex);
    return NMEACR_OK;
}

NmeaCtrlResult NmeaCtrl_SendCommand(NmeaCtrl* ctrl, const char* command)
{
    if (ctrl == 0)
        return NMEACR_NOT_CONFIGURED;

    if (ctrl->state <= NMEACS_NOT_INITIALIZED)
        return NMEACR_NOT_INITIALIZED;

    if (ctrl->state != NMEACS_IDLE)
        return NMEACR_BUSY;

    if (command == 0)
    	return NMEACR_BAD_PARRAM;

    if (!UBMutex_TryLock(&ctrl->mutex))
		return NMEACR_BUSY;

    ctrl->state = NMEACS_COMMAND;

    UBUart_Enqueue(ctrl->uart, (const uint8_t*)command, (uint16_t)strlen(command), 0);
    UBUart_Enqueue(ctrl->uart, (const uint8_t*)"\r\n", 2, 0);
    UBUart_Transmit(ctrl->uart);

   	UBMutex_Unlock(&ctrl->mutex);
    ctrl->state = NMEACS_IDLE;

   	return NMEACR_OK;
}

#ifdef __cplusplus
}
#endif
