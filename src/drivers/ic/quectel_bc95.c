/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   quectel_bc95.c
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/ic/quectel_bc95.h>
#include <string.h>
#include <stdlib.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/utils/system_time.h>

#define MODEM_POWER_ON_TIMEOUT_SEC					10
#define MODEM_WAIT_SERVICE_CHECK_PERIOD_SEC			5
#define MODEM_WAIT_SERVICE_CHECK_PERIOD_IDLE_SEC	10
#define MODEM_WAIT_SERVICE_TIMEOUT_STEPS			60

#ifdef __cplusplus
extern "C"
{
#endif

static void SetStateTimeout(QuectelBC95* bc, BC95State newState, UBTimeInterval timeout)
{
	if (bc == 0)
		return;
	bc->state = newState;
	bc->stateStep = 0;
	bc->stateTimer = timeout;
}

static void Process(UBThread* thread)
{
	QuectelBC95* bc = (QuectelBC95*)UBThread_GetCustomData(thread);
	if (bc == 0)
		return;
	switch(bc->state)
	{
	case BC95S_INITIALISING:
		if (bc->stateStep == 0)
		{
		    if (bc->resetPin)
		    {
                UBPower_EnablePower(bc->resetPin->port.base.handle);
		    	UBGpioPin_SetFunction(bc->resetPin, UBGPF_GPIO);
		    	UBGpioPin_Output(bc->resetPin);
		    	if (bc->resetActiveHigh)
		    	{
		    		UBGpioPin_Set(bc->resetPin);
		    		UBSysTime_DelayMilli(10);
		    		UBGpioPin_Clear(bc->resetPin);
		    	}
		    	else
		    	{
		    		UBGpioPin_Clear(bc->resetPin);
		    		UBSysTime_DelayMilli(10);
		    		UBGpioPin_Set(bc->resetPin);
		    	}
		    }
		    bc->stateStep = 1;
		    bc->stateTimeout = UBSysTime_GetFutureMilli(MODEM_POWER_ON_TIMEOUT_SEC * 1000);
		}
		else if (bc->stateStep == 1)
		{
			if (UBSysTime_HasPassed(bc->stateTimeout))
				bc->stateStep = 0;
			else
			{
				if (AtCtrl_GetState(&bc->atCtrl) == ATCS_IDLE)
					bc->stateStep = 2;
			}
		}
		else if (bc->stateStep == 2)
		{
			char atRes[16];
			AtCtrlResult result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+NCONFIG=AUTOCONNECT,FALSE", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(5000));
			if (result != ATCR_OK)
			{
				bc->stateStep = 0;
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+NRB", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(5000));
			AtCtrl_EnterSoftwareReset(&bc->atCtrl);
			bc->stateTimeout = UBSysTime_GetFutureMilli(MODEM_POWER_ON_TIMEOUT_SEC * 1000);
			bc->stateStep = 3;
		}
		else if (bc->stateStep == 3)
		{
			if (UBSysTime_HasPassed(bc->stateTimeout))
				bc->stateStep = 0;
			else
			{
				if (AtCtrl_GetState(&bc->atCtrl) == ATCS_IDLE)
				{
					bc->stateStep = 4;
					bc->stateTimer = UBSysTime_GetFutureMilli(3000);
				}
			}
		}
		else if (bc->stateStep == 4)
		{
			if (!UBSysTime_HasPassed(bc->stateTimer))
				break;
			char atRes[48];
			AtCtrlResult result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CFUN=1", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				bc->stateStep = 0;
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CEDRXS=0,5", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				bc->stateStep = 0;
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CGATT=1", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				bc->stateStep = 0;
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+NCCID?", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				bc->stateStep = 0;
				break;
			}
			char* pos = strchr(atRes, ':');
			if (pos)
				strcpy(bc->iccid, pos + 1);
			else
				bc->iccid[0] = 0;
			SetStateTimeout(bc, BC95S_WAITING_FOR_SERVICE, 0);
		}
		break;

	case BC95S_WAITING_FOR_SERVICE:
		if (UBSysTime_HasPassed(bc->stateTimer))
		{
			char atRes[16];
			AtCtrlResult result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CGATT?", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				SetStateTimeout(bc, BC95S_INITIALISING, 0);
			}
			else
			{
				bc->stateTimer = UBSysTime_GetFutureMilli(MODEM_WAIT_SERVICE_CHECK_PERIOD_SEC * 1000);
				if (strcmp(atRes, "+CGATT:1") == 0)
				{
					SetStateTimeout(bc, BC95S_IDLE, UBSysTime_GetFutureMilli(MODEM_WAIT_SERVICE_CHECK_PERIOD_IDLE_SEC * 1000));
				}
				else
				{
					bc->stateStep++;
					if (bc->stateStep >= MODEM_WAIT_SERVICE_TIMEOUT_STEPS)
					{
						SetStateTimeout(bc, BC95S_SERVICE_RECOVER, 0);
					}
				}
			}
		}
		break;
	case BC95S_IDLE:
		if (UBSysTime_HasPassed(bc->stateTimer))
		{
			char atRes[16];
			AtCtrlResult result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CGATT?", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				SetStateTimeout(bc, BC95S_INITIALISING, 0);
			}
			else
			{
				bc->stateTimer = UBSysTime_GetFutureMilli(MODEM_WAIT_SERVICE_CHECK_PERIOD_IDLE_SEC * 1000);
				if (strcmp(atRes, "+CGATT:1") == 0)
				{
					bc->stateTimer = UBSysTime_GetFutureMilli(MODEM_WAIT_SERVICE_CHECK_PERIOD_IDLE_SEC * 1000);
				}
				else
				{
					SetStateTimeout(bc, BC95S_WAITING_FOR_SERVICE, UBSysTime_GetFutureMilli(MODEM_WAIT_SERVICE_CHECK_PERIOD_SEC * 1000));
				}
			}
		}
		break;
	case BC95S_SERVICE_RECOVER:
		if (bc->stateStep == 0)
		{
			char atRes[16];
			AtCtrl_SendAtSync(&bc->atCtrl, "AT+NRB", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(5000));
			AtCtrl_EnterSoftwareReset(&bc->atCtrl);
			bc->stateTimeout = UBSysTime_GetFutureMilli(MODEM_POWER_ON_TIMEOUT_SEC * 1000);
			bc->stateStep = 1;
		}
		else if (bc->stateStep == 1)
		{
			if (UBSysTime_HasPassed(bc->stateTimeout))
				bc->stateStep = 0;
			else
			{
				if (AtCtrl_GetState(&bc->atCtrl) == ATCS_IDLE)
				{
					bc->stateStep = 2;
					bc->stateTimer = UBSysTime_GetFutureMilli(3000);
				}
			}
		}
		else if (bc->stateStep == 2)
		{
			if (!UBSysTime_HasPassed(bc->stateTimer))
				break;
			char atRes[16];
			AtCtrlResult result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CFUN=0", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				SetStateTimeout(bc, BC95S_INITIALISING, 0);
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+NCSEARFCN", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				SetStateTimeout(bc, BC95S_INITIALISING, 0);
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CFUN=1", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				SetStateTimeout(bc, BC95S_INITIALISING, 0);
				break;
			}
			result = AtCtrl_SendAtSync(&bc->atCtrl, "AT+CGATT=1", atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(10000));
			if (result != ATCR_OK)
			{
				SetStateTimeout(bc, BC95S_INITIALISING, 0);
				break;
			}
			SetStateTimeout(bc, BC95S_WAITING_FOR_SERVICE, 0);
		}
		break;
	default:
		break;
	}
}

void BC95_Configure(QuectelBC95* bc, UBUart* uart, UBFifo* txModemMonitor, UBGpioPin* resetPin, bool resetActiveHigh)
{
    if ((bc == 0) || (uart == 0))
    {
        return;
    }

    UBMutex_Configure(&bc->mutex, true, false);
    if (!UBMutex_TryLock(&bc->mutex))
		return;

    AtCtrl_Configure(&bc->atCtrl, uart, txModemMonitor);
    AtCtrl_SetCustomData(&bc->atCtrl, bc);
    UBThread_Configure(&bc->thread, 0, 0, 0, Process);
    UBThread_SetCustomData(&bc->thread, bc);
    bc->uart = uart;
    bc->resetPin = resetPin;
    bc->resetActiveHigh = resetActiveHigh;
    bc->state = BC95S_NOT_INITIALIZED;
    bc->stateStep = 0;
    UBMutex_Unlock(&bc->mutex);
}

BC95Result BC95_Init(QuectelBC95* bc, UBFrequency baudrate)
{
    if (bc == 0)
        return BC95R_NOT_CONFIGURED;

    if (!UBMutex_TryLock(&bc->mutex))
		return BC95R_BUSY;

    UBUart_Init(bc->uart, baudrate);
    AtCtrl_Init(&bc->atCtrl, baudrate, bc->resetPin == 0);
    bc->state = BC95S_INITIALISING;
    bc->stateStep = 0;

    UBThread_Start(&bc->thread);
    UBMutex_Unlock(&bc->mutex);
    return BC95R_OK;
}

bool BC95_HasService(QuectelBC95* bc)
{
	if (bc == 0)
		return false;
	return bc->state >= BC95S_IDLE;
}

void BC95_UdpSocketConfigure(QuectelBC95Socket* socket, QuectelBC95* bc)
{
	if ((bc == 0) || (socket == 0))
        return;
	socket->opened = false;
	socket->id = 0xff;
	socket->bc = bc;
}

BC95Result BC95_OpenUdpSocket(QuectelBC95Socket* socket, uint16_t port)
{
	if ((socket == 0) || (socket->bc == 0))
		return BC95R_NOT_CONFIGURED;
	if (socket->bc->state == BC95S_NOT_INITIALIZED)
		return BC95R_NOT_INITIALIZED;
	if (socket->bc->state < BC95S_IDLE)
		return BC95R_NO_SERVICE;

    if (!UBMutex_TryLock(&socket->bc->mutex))
		return BC95R_BUSY;

	socket->opened = false;
	socket->id = 0xff;

	char atRes[16];
	char portStr[7];
	itoa(port, portStr, 10);

	char request[32];
	request[0] = 0;
	strcat(request, "AT+NSOCR=DGRAM,17,");
	strcat(request, portStr);

	AtCtrlResult result = AtCtrl_SendAtSync(&socket->bc->atCtrl, request , atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(2000));
	if (result != ATCR_OK)
	{
		UBMutex_Unlock(&socket->bc->mutex);
		return BC95R_AT_ERROR;
	}

	socket->id = (uint8_t)atoi(atRes);
	socket->opened = true;

	UBMutex_Unlock(&socket->bc->mutex);

	return BC95R_OK;
}

BC95Result BC95_CloseUdpSocket(QuectelBC95Socket* socket)
{
	if ((socket == 0) || (socket->bc == 0))
		return BC95R_NOT_CONFIGURED;
	if (socket->bc->state == BC95S_NOT_INITIALIZED)
		return BC95R_NOT_INITIALIZED;
	if (socket->bc->state < BC95S_IDLE)
		return BC95R_NO_SERVICE;

    if (!UBMutex_TryLock(&socket->bc->mutex))
		return BC95R_BUSY;

	if (!socket->opened)
		return BC95R_SOCKET_ERROR;

	char atRes[16];
	char sockStr[7];
	itoa(socket->id, sockStr, 10);

	char request[32];
	request[0] = 0;
	strcat(request, "AT+NSOCL=");
	strcat(request, sockStr);

	AtCtrlResult result = AtCtrl_SendAtSync(&socket->bc->atCtrl, request , atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(2000));
	if (result != ATCR_OK)
	{
		UBMutex_Unlock(&socket->bc->mutex);
		return BC95R_AT_ERROR;
	}

	socket->id = 0xff;
	socket->opened = false;

	UBMutex_Unlock(&socket->bc->mutex);

	return BC95R_OK;
}

bool BC95_IsUdpSocketOpened(QuectelBC95Socket* socket)
{
	if (socket == 0)
		return false;
	return socket->opened;
}

BC95Result BC95_SendUdp(QuectelBC95Socket* socket, QuectelIpAddress destination, uint16_t port, const uint8_t* data, uint16_t size)
{
	if (data == 0)
		return BC95R_BAD_PARAM;
	if ((socket == 0) || (socket->bc == 0))
		return BC95R_NOT_CONFIGURED;
	if (socket->bc->state == BC95S_NOT_INITIALIZED)
		return BC95R_NOT_INITIALIZED;
	if (socket->bc->state < BC95S_IDLE)
		return BC95R_NO_SERVICE;

    if (!UBMutex_TryLock(&socket->bc->mutex))
		return BC95R_BUSY;

	if (!socket->opened)
		return BC95R_SOCKET_ERROR;

	if (size == 0)
		return BC95R_OK;

	char atRes[16];
	char numStr[7];
	itoa(socket->id, numStr, 10);

	char request[49];
	request[0] = 0;
	strcat(request, "AT+NSOST=");
	strcat(request, numStr);
	strcat(request, ",");
	itoa(destination[0], numStr, 10);
	strcat(request, numStr);
	strcat(request, ".");
	itoa(destination[1], numStr, 10);
	strcat(request, numStr);
	strcat(request, ".");
	itoa(destination[2], numStr, 10);
	strcat(request, numStr);
	strcat(request, ".");
	itoa(destination[3], numStr, 10);
	strcat(request, numStr);
	strcat(request, ",");
	itoa(port, numStr, 10);
	strcat(request, numStr);
	strcat(request, ",");
	itoa(size, numStr, 10);
	strcat(request, numStr);
	strcat(request, ",");

	AtCtrlResult result = AtCtrl_SendAtPartial(&socket->bc->atCtrl, false, false, request , atRes, sizeof(atRes) - 1, 0);
	if (result != ATCR_OK)
	{
		UBMutex_Unlock(&socket->bc->mutex);
		return BC95R_AT_ERROR;
	}

	const uint8_t* ptr = data;
	uint16_t sz = (sizeof(request) - 1) >> 1;
	while(size)
	{
		uint16_t chunk = size;
		if (chunk > sz)
			chunk = sz;
		for(uint16_t i = 0; i < chunk; i++)
		{
			if ((ptr[i] >> 4) >= 10)
				request[i << 1] = 'A' + (ptr[i] >> 4) - 10;
			else
				request[i << 1] = '0' + (ptr[i] >> 4);
			if ((ptr[i] & 0xf) >= 10)
				request[(i << 1) + 1] = 'A' + (ptr[i] & 0xf) - 10;
			else
				request[(i << 1) + 1] = '0' + (ptr[i] & 0xf);
		}
		request[chunk << 1] = 0;
		result = AtCtrl_SendAtPartial(&socket->bc->atCtrl, chunk == size, true, request , atRes, sizeof(atRes) - 1, UBSysTime_GetIntervalMilli(2000));
		if (result != ATCR_OK)
		{
			UBMutex_Unlock(&socket->bc->mutex);
			return BC95R_AT_ERROR;
		}
		size -= chunk;
		ptr += chunk;
	}

	UBMutex_Unlock(&socket->bc->mutex);

	return BC95R_OK;
}

#ifdef __cplusplus
}
#endif
