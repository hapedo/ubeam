/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   gpio.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/gpio.h>

#ifdef __cplusplus
extern "C"
{
#endif

void UBGpioPort_Configure(UBGpioPort* port, UBPeriHandle portHandle)
{
    port->base.handle = portHandle;
}

void UBGpioPin_Configure(UBGpioPin* pin, UBPeriHandle portHandle, UBGpioPinMask mask)
{
    pin->port.base.handle = portHandle;
	pin->mask = mask;
}

void UBGpioPin_Set(UBGpioPin* pin)
{
	UBGpioPort_Set(&pin->port, pin->mask);
}

void UBGpioPin_Clear(UBGpioPin* pin)
{
	UBGpioPort_Clear(&pin->port, pin->mask);
}

void UBGpioPin_Invert(UBGpioPin* pin)
{
	UBGpioPort_Invert(&pin->port, pin->mask);
}

void UBGpioPin_Write(UBGpioPin* pin, bool value)
{
	if (value)
		UBGpioPort_Set(&pin->port, pin->mask);
	else
		UBGpioPort_Clear(&pin->port, pin->mask);
}

bool UBGpioPin_Read(UBGpioPin* pin)
{
	if (UBGpioPort_IsSet(&pin->port, pin->mask))
		return true;
	else
		return false;
}

bool UBGpioPin_IsSet(UBGpioPin* pin)
{
	return UBGpioPort_IsSet(&pin->port, pin->mask);
}

bool UBGpioPin_IsCleared(UBGpioPin* pin)
{
	return UBGpioPort_IsCleared(&pin->port, pin->mask);
}

void UBGpioPin_Output(UBGpioPin* pin)
{
	UBGpioPort_Output(&pin->port, pin->mask);
}

void UBGpioPin_Input(UBGpioPin* pin)
{
	UBGpioPort_Input(&pin->port, pin->mask);
}

bool UBGpioPin_IsOutput(UBGpioPin* pin)
{
	return UBGpioPort_IsOutput(&pin->port, pin->mask);
}

bool UBGpioPin_IsInput(UBGpioPin* pin)
{
	return UBGpioPort_IsInput(&pin->port, pin->mask);
}

bool UBGpioPin_SetFunction(UBGpioPin* pin, UBGpioPinFunc func)
{
	return UBGpioPort_SetFunction(&pin->port, pin->mask, func);
}

void UBGpioPin_SetSpeed(UBGpioPin* pin, UBGpioPinSpeed speed)
{
	UBGpioPort_SetSpeed(&pin->port, pin->mask, speed);
}

void UBGpioPin_SetPullResistor(UBGpioPin* pin, UBGpioPinPull pull)
{
	UBGpioPort_SetPullResistor(&pin->port, pin->mask, pull);
}

#ifdef __cplusplus
}
#endif
