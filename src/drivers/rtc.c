/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   rtc.c
 * \brief  microBeam SDK
 * \date   31.7.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/rtc.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define DAYS2SECONDS(x) ((uint32_t)(x) * 24 * 60 * 60)

static const uint16_t s_month[12] = {31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};

UBRtcResult UBRtc_GetTimestamp(UBRtc* rtc, UBRtcTimestamp* timestamp)
{
    if ((timestamp == 0) || (rtc == 0))
        return UBRTCR_BAD_PARAM;
    UBRtcDateTime date;
    UBRtcResult res = UBRtc_GetTime(rtc, &date);
    if (res != UBRTCR_OK)
        return res;
    if (!UBRtc_ToTimestamp(&date, timestamp))
        return UBRTCR_ERROR;
    return UBRTCR_OK;
}

bool UBRtc_ToTimestamp(const UBRtcDateTime* dateTime, UBRtcTimestamp* timestamp)
{
    UBRtcTimestamp result = 0;
    if (timestamp == 0)
        return false;
    if ((dateTime->year < 1970) || (dateTime->year > 2099) ||
            (dateTime->month < 1) || (dateTime->month > 12) ||
            (dateTime->day > 31) || (dateTime->day < 1) ||
            (dateTime->hour > 23) || (dateTime->minute > 59) || (dateTime->second > 59))
        return false;
    uint8_t years = dateTime->year - 1970;
    uint8_t leaps = 0;
    if (years >= 2)
        leaps = (years + 1) >> 2;
    bool isLeap = (((years + 2) & 0x03) == 0);
    if (dateTime->month > 1)
    {
        result = DAYS2SECONDS(years * 365);
        result += DAYS2SECONDS(leaps);
        result += DAYS2SECONDS(s_month[dateTime->month - 2]);
        result += DAYS2SECONDS(dateTime->day - 1);
        result += dateTime->hour * 60 * 60;
        result += dateTime->minute * 60;
        result += dateTime->second;
        if ((dateTime->month > 2) && (isLeap))
            result += DAYS2SECONDS(1);
    }
    else
    {
        result = DAYS2SECONDS(years * 365);
        result += DAYS2SECONDS(leaps);
        result += DAYS2SECONDS(dateTime->day - 1);
        result += dateTime->hour * 60 * 60;
        result += dateTime->minute * 60;
        result += dateTime->second;
    }
    *timestamp = result;
    return true;
}

bool UBRtc_FromTimestamp(UBRtcTimestamp timestamp, UBRtcDateTime* dateTime)
{
    if (dateTime == 0)
        return false;
    dateTime->second = timestamp % 60;
    timestamp /= 60;
    dateTime->minute = timestamp % 60;
    timestamp /= 60;
    dateTime->hour = timestamp % 24;
    timestamp /= 24;
    uint32_t daysPre = 0;
    uint32_t daysPost = 0;
    uint16_t year;
    bool isLeap = false;
    for(year = 0; year <= (2099 - 1970); year++)
    {
        daysPre = daysPost;
        daysPost += 365;
        if (((year + 2) & 0x03) == 0)
        {
            daysPost++;
            isLeap = true;
        }
        else
            isLeap = false;
        if (timestamp < daysPost)
            break;
    }
    if (year == 130)
        return false;
    dateTime->year = year + 1970;
    timestamp -= daysPre;
    uint8_t month;
    daysPre = 0;
    daysPost = 0;
    for(month = 0; month <= 11; month++)
    {
        daysPre = daysPost;
        daysPost = s_month[month];
        if ((isLeap) && (month >= 1))
            daysPost++;
        if (timestamp < daysPost)
            break;
    }
    if (month == 12)
        return false;
    dateTime->month = month + 1;
    timestamp -= daysPre;
    dateTime->day = timestamp + 1;
    dateTime->milli = 0;
    static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    int y = dateTime->year - (dateTime->month < 3);
    int r = ((y + y/4 - y/100 + y/400 + t[dateTime->month - 1] + dateTime->day) % 7);
    dateTime->weekDay = (UBRtcWeekDay)(r);
    return true;
}

#ifdef __cplusplus
}
#endif
