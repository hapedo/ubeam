/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   fifo.c
 * \brief  microBeam SDK
 * \date   7.11.2019
 * \note
 * \todo
 */
#include <ubeam/utils/fifo.h>
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

void UBFifo_Configure(UBFifo* fifo, uint8_t* buffer, uint32_t size)
{
	if (fifo == 0)
		return;
	fifo->buffer = buffer;
	fifo->size = size;
	fifo->start = 0;
	fifo->end = 0;
}

void UBFifo_Clear(UBFifo* fifo)
{
	if (fifo == 0)
		return;
	fifo->start = 0;
	fifo->end = 0;
}

bool UBFifo_PutByte(UBFifo* fifo, uint8_t data)
{
	if (fifo == 0)
		return false;
	if (fifo->buffer == 0)
		return false;
	if (UBFifo_GetAvailable(fifo) == 0)
		return false;
	uint32_t end = fifo->end;
	fifo->buffer[end++] = data;
	if (end >= fifo->size)
		end = 0;
	fifo->end = end;
	return true;
}

uint32_t UBFifo_Put(UBFifo* fifo, const uint8_t* data, uint32_t dataSize)
{
	if (fifo == 0)
		return 0;
	if (fifo->buffer == 0)
		return 0;
	if (UBFifo_GetAvailable(fifo) < dataSize)
		dataSize = UBFifo_GetAvailable(fifo);
	if (dataSize == 0)
		return 0;

	uint32_t end = fifo->end;
	uint32_t cnt = fifo->size - fifo->end;
	uint32_t enq;
	if (cnt >= dataSize)
	{
		memcpy(&fifo->buffer[end], data, dataSize);
		end += dataSize;
		enq = dataSize;
	}
	else
	{
		memcpy(&fifo->buffer[end], data, cnt);
		memcpy(fifo->buffer, &data[cnt], dataSize - cnt);
		end = dataSize - cnt;
		enq = dataSize;
	}
	if (end >= fifo->size)
		end = 0;
	fifo->end = end;
	return enq;
}

uint32_t UBFifo_GetCount(UBFifo* fifo)
{
	if (fifo == 0)
		return 0;
	if (fifo->buffer == 0)
		return 0;
	uint32_t end = fifo->end;
	uint32_t start = fifo->start;
	if (end >= start)
		return end - start;
	else
		return end + fifo->size - start;
}

uint32_t UBFifo_GetAvailable(UBFifo* fifo)
{
	if (fifo == 0)
		return 0;
	if (fifo->buffer == 0)
		return 0;
	return fifo->size - UBFifo_GetCount(fifo) - 1;
}

uint32_t UBFifo_GetByte(UBFifo* fifo, uint8_t* data)
{
	if (fifo == 0)
		return 0;
	if (fifo->buffer == 0)
		return 0;
	if (UBFifo_GetCount(fifo) == 0)
		return 0;
	uint32_t start = fifo->start;
	if (data)
		*data = fifo->buffer[start];
	start++;
	if (start >= fifo->size)
		start = 0;
	fifo->start = start;
	return 1;
}

uint32_t UBFifo_GetData(UBFifo* fifo, uint8_t* data, uint32_t dataSize)
{
	if (fifo == 0)
		return 0;
	if (fifo->buffer == 0)
		return 0;
	uint32_t fcnt = UBFifo_GetCount(fifo);
	if (fcnt == 0)
		return 0;
	else if (fcnt < dataSize)
		dataSize = fcnt;
	uint32_t start = fifo->start;
	uint32_t cnt = fifo->size - start;
	if (cnt >= dataSize)
	{
		memcpy(data, &fifo->buffer[start], dataSize);
		start += dataSize;
	}
	else
	{
		memcpy(data, &fifo->buffer[start], cnt);
		memcpy(&data[cnt], fifo->buffer, dataSize - cnt);
		start = dataSize - cnt;
	}
	if (start >= fifo->size)
		start = 0;
	fifo->start = start;
	return dataSize;
}

uint32_t UBFifo_Move(UBFifo* fifoTo, UBFifo* fifoFrom)
{
	if ((fifoTo == 0) || (fifoFrom == 0))
		return 0;
	uint32_t cnt = UBFifo_GetCount(fifoFrom);
	uint32_t tr = 0;
	while(cnt--)
	{
		uint8_t data;
		if (UBFifo_GetCount(fifoTo) >= (fifoTo->size - 1))
			break;
		if (!UBFifo_GetByte(fifoFrom, &data))
			break;
		if (!UBFifo_PutByte(fifoTo, data))
			break;
		tr++;
	}
	return tr;
}

#ifdef __cplusplus
}
#endif
