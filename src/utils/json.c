/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   json.c
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#include <ubeam/utils/json.h>
#include <string.h>
#include <stdlib.h>
#include <ubeam/drivers/memory.h>

#define MALLOC(size)            UBMemAlloc(size)
#define FREE(ptr)               UBMemFree(ptr)

#define LF_SIZE                 2
#define LF                      "\r\n"
#define MAX_STRING_SIZE         64

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum
{
    PARSER_OBJECT_START,
    PARSER_VALUE_NAME,
    PARSER_VALUE_COLON,
    PARSER_VALUE_VALUE,
    PARSER_VALUE_SEPARATOR,
    PARSER_VALUE_TYPE_STRING,
    PARSER_VALUE_TYPE_NUMBER,
    PARSER_VALUE_TYPE_NULL,
    PARSER_VALUE_TYPE_BOOL,
    PARSER_VALUE_TYPE_OBJECT
} ParserState;

static void disposeObject(JsonObject* object);
static uint16_t stringfyObject(JsonObject* objectPtr, char* dest, uint16_t tabCount, uint16_t maxSize);

//static int allocated = 0;
//static int allocatedSize = 0;
static char s_stringBuffer[MAX_STRING_SIZE];
static JsonObject** s_parserObjects;
static JsonValue* s_valueBuffer;
static uint16_t s_maxNestedObjectCount;
static uint16_t s_maxValueCount;

/*static void* debugMalloc(size_t size)
{
    allocated++;
    allocatedSize += size;
    return malloc(size);
}

static void debugFree(void* ptr)
{
    allocated--;
    free(ptr);
}*/
static JsonValue* allocateValue()
{
    uint16_t i;
    for(i = 0; i < s_maxValueCount; i++)
        if (s_valueBuffer[i].__allocated == 0)
        {
            s_valueBuffer[i].__allocated = 1;
            return &s_valueBuffer[i];
        }
    return 0;
}

static void freeValue(JsonValue* value)
{
    value->__allocated = 0;
}

static void reverse(char* str, int length)
{
    int start = 0;
    int end = length -1;
    char temp;
    while (start < end)
    {
        temp = *(str+start);
        *(str+start) = *(str+end);
        *(str+end) = temp;
        start++;
        end--;
    }
}

static char* ___itoa(int num, char* str, int base)
{
    int i = 0;
    uint8_t isNegative = 0;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = 1;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    reverse(str, i);

    return str;
}

static int __atoi(char *str)
{
    int i;
    int res = 0; // Initialize result

    // Iterate through all characters of input string and
    // update result
    for (i = 0; str[i] != '\0'; ++i)
        res = res*10 + str[i] - '0';

    // return result.
    return res;
}

/*
static void updateString(char** ptr, const char* value)
{
    if (*ptr)
        FREE(*ptr);
    *ptr = (char*)MALLOC(strlen(value) + 1);
    strcpy(*ptr, value);
}*/

static void disposeValueContent(JsonValue* value)
{
    if (value->array)
    {
        uint16_t i;
        for(i = 0; i < value->arrayCount; i++)
            if (value->array[i])
                json_DisposeValue(value->array[i]);
        FREE(value->array);
    }
    if (value->object)
        disposeObject(value->object);
    value->valueString[0] = 0;
    value->array = 0;
    value->object = 0;
    value->valueNumber = 0;
    value->type = JSON_NULL;
}

static uint16_t appendString(char* dest, char* source, uint16_t maxSize)
{
    uint16_t destLen;
    uint16_t len = (uint16_t)strlen(source);
    if (maxSize == 0)
        return 0;
    destLen = (uint16_t)strlen(dest);
    if (len > maxSize)
        len = maxSize;
    memcpy(&dest[destLen], source, len);
    dest[destLen + len] = 0;
    return len;
}

static uint16_t appendStringEscaped(char* dest, char* source, uint16_t maxSize)
{
    uint16_t destLen;
    uint16_t i;
    uint16_t index;
    uint16_t len = (uint16_t)strlen(source);
    if (maxSize == 0)
        return 0;
    destLen = (uint16_t)strlen(dest);
    if (len > maxSize)
        len = maxSize;
    dest += destLen;
    index = 0;
    for(i = 0; i < len; i++)
    {
        if (index > maxSize)
            break;
        if (source[i] == '\t')
        {
            dest[index++] = '\\';
            dest[index++] = 't';
            continue;
        }
        if (source[i] == '"')
        {
            dest[index++] = '\\';
            dest[index++] = '"';
            continue;
        }
        if (source[i] == '\r')
        {
            dest[index++] = '\\';
            dest[index++] = 'r';
            continue;
        }
        if (source[i] == '\n')
        {
            dest[index++] = '\\';
            dest[index++] = 'n';
            continue;
        }
        if (source[i] == '\\')
        {
            dest[index++] = '\\';
            dest[index++] = '\\';
            continue;
        }
        dest[index++] = source[i];
    }
    dest[index] = 0;
    return index;
}

static uint16_t stringfyValue(JsonValue* valuePtr, char* dest, uint16_t tabCount, uint16_t maxSize)
{
    uint16_t i;
    char buffer[20];

    if (valuePtr == 0)
        return maxSize;
    if (maxSize == 0)
        return maxSize;
    switch(valuePtr->type)
    {
    case JSON_NULL:
        maxSize -= appendString(dest, "null", maxSize);
        break;
    case JSON_NUMBER:
    	___itoa(valuePtr->valueNumber, buffer, 10);
        maxSize -= appendString(dest, buffer, maxSize);
        break;
    case JSON_STRING:
        maxSize -= appendString(dest, "\"", maxSize);
        maxSize -= appendStringEscaped(dest, valuePtr->valueString, maxSize);
        maxSize -= appendString(dest, "\"", maxSize);
        break;
    case JSON_ARRAY:
        maxSize -= appendString(dest, "[", maxSize);
        if (maxSize == 0)
            return 0;
        for(i = 0; i < valuePtr->arrayCount; i++)
        {
            maxSize = stringfyValue(valuePtr->array[i], dest, tabCount, maxSize);
            if (maxSize == 0)
                return 0;
            if (i < (valuePtr->arrayCount - 1))
            {
                maxSize -= appendString(dest, ", ", maxSize);
                if (maxSize == 0)
                    return 0;
            }
        }
        maxSize -= appendString(dest, "]", maxSize);
        break;
    case JSON_BOOLEAN:
        if (valuePtr->valueNumber)
            maxSize -= appendString(dest, "true", maxSize);
        else
            maxSize -= appendString(dest, "false", maxSize);
        break;
    case JSON_OBJECT:
        maxSize -= appendString(dest, "\r\n", maxSize);
        maxSize = stringfyObject(valuePtr->object, dest, tabCount + 1, maxSize);
        break;
    }
    return maxSize;
}

static uint16_t stringfyValueWithName(JsonValue* valuePtr, char* dest, uint16_t tabCount, uint16_t maxSize)
{
    uint16_t i;
    if (valuePtr == 0)
        return maxSize;
    for(i = 0; i < tabCount; i++)
        maxSize -= appendString(dest, "\t", maxSize);
    maxSize -= appendString(dest, "\"", maxSize);
    maxSize -= appendString(dest, valuePtr->name, maxSize);
    maxSize -= appendString(dest, "\": ", maxSize);
    maxSize = stringfyValue(valuePtr, dest, tabCount, maxSize);
    return maxSize;
}

static uint16_t stringfyObject(JsonObject* objectPtr, char* dest, uint16_t tabCount, uint16_t maxSize)
{
    uint16_t i;
    if ((objectPtr == 0) || (dest == 0))
        return 0;
    for(i = 0; i < tabCount; i++)
        maxSize -= appendString(dest, "\t", maxSize);
    maxSize -= appendString(dest, "{\r\n", maxSize);
    for(i = 0; i < objectPtr->count; i++)
    {
        if (objectPtr->values[i])
            maxSize = stringfyValueWithName(objectPtr->values[i], dest, tabCount, maxSize);
        if (i < (objectPtr->count - 1))
        {
            maxSize -= appendString(dest, ", \r\n", maxSize);
//            for(i = 0; i < tabCount + 1; i++)
//                maxSize -= appendString(dest, "\t", maxSize);
        }
        if (maxSize == 0)
            break;
    }
    maxSize -= appendString(dest, "\r\n", maxSize);
    for(i = 0; i < tabCount; i++)
        maxSize -= appendString(dest, "\t", maxSize);
    maxSize -= appendString(dest, "}", maxSize);
    return maxSize;
}


void json_DisposeValue(JsonValue* valuePtr)
{
    disposeValueContent(valuePtr);
    //FREE(valuePtr);
    freeValue(valuePtr);
}

static void disposeObject(JsonObject* object)
{
    uint16_t i;

    if (object == 0)
        return;

    if (object->values)
    {
        for(i = 0; i < object->count; i++)
        {
            if (object->values[i] == 0)
                continue;
            json_DisposeValue(object->values[i]);
        }
        FREE(object->values);
    }
    FREE(object);
}

void json_Init(uint16_t maxNestedObjectCount, uint16_t maxValueCount)
{
    uint16_t i;
    s_maxNestedObjectCount = maxNestedObjectCount;
    s_maxValueCount = maxValueCount;
    s_parserObjects = MALLOC(sizeof(JsonObject*) * maxNestedObjectCount);
    s_valueBuffer = MALLOC(sizeof(JsonValue) * maxValueCount);
    for(i = 0; i < s_maxValueCount; i++)
        s_valueBuffer[i].__allocated = 0;
}

JsonValue* json_CreateValue(const char* name)
{
    JsonValue* val;
    if (strlen(name) > JSON_VALUE_NAME_LENGTH - 1)
        return 0;
    //JsonValue* val = (JsonValue*)MALLOC(sizeof(JsonValue));
    val = allocateValue();
    if (val == 0)
        return 0;
    val->name[0] = 0;
    val->valueString[0] = 0;
    val->object = 0;
    val->array = 0;
    val->arrayCount = 0;
    val->valueNumber = 0;
    val->type = JSON_NULL;
    //updateString(&val->name, name);
    strcpy((char*)&val->name, name);
    return val;
}


void json_SetStringValue(JsonValue* valuePtr, const char* value)
{
    if (valuePtr == 0)
        return;
    if (strlen(value) >= JSON_VALUE_STRING_VALUE_LENGTH - 1)
        return;
    disposeValueContent(valuePtr);
    valuePtr->type = JSON_STRING;
    //updateString(&valuePtr->valueString, value);
    strcpy((char*)&valuePtr->valueString, value);
}

void json_SetNumberValue(JsonValue* valuePtr, int value)
{
    if (valuePtr == 0)
        return;
    disposeValueContent(valuePtr);
    valuePtr->type = JSON_NUMBER;
    valuePtr->valueNumber = value;
}

void json_SetBoolValue(JsonValue* valuePtr, uint8_t value)
{
    if (valuePtr == 0)
        return;
    disposeValueContent(valuePtr);
    valuePtr->type = JSON_BOOLEAN;
    valuePtr->valueNumber = (int)value;
}

void json_SetNullValue(JsonValue* valuePtr)
{
    if (valuePtr == 0)
        return;
    disposeValueContent(valuePtr);
    valuePtr->type = JSON_NULL;
}

void json_SetArrayValueCount(JsonValue* valuePtr, uint16_t count)
{
    JsonValue** ar = 0;
    if (valuePtr == 0)
        return;
    if (valuePtr->type != JSON_ARRAY)
        disposeValueContent(valuePtr);
    if (count)
    {
        uint16_t copycnt = count;
        uint16_t i;
        if (count > valuePtr->arrayCount)
            copycnt = valuePtr->arrayCount;
        if (count)
        {
            ar = (JsonValue**)MALLOC(sizeof(JsonValue*) * count);
            if (copycnt)
                memcpy(ar, valuePtr->array, sizeof(JsonValue*) * copycnt);
        }
        for(i = copycnt; i < valuePtr->arrayCount; i++)
        {
            if (valuePtr->array[i])
                json_DisposeValue(valuePtr->array[i]);
            valuePtr->array[i] = 0;
        }
        if (valuePtr->array)
            FREE(valuePtr->array);
        valuePtr->array = ar;
        for(i = valuePtr->arrayCount; i < count; i++)
        {
            JsonValue* val = json_CreateValue("ARRAY");
            valuePtr->array[i] = val;
        }
    }
    valuePtr->type = JSON_ARRAY;
    valuePtr->arrayCount = count;
    valuePtr->array = ar;
}

uint16_t json_GetArrayValueCount(JsonValue* valuePtr)
{
    if (valuePtr == 0)
        return 0;
    if (valuePtr->type != JSON_ARRAY)
        return 0;
    return valuePtr->arrayCount;
}

JsonValue* json_GetArrayValue(JsonValue* valuePtr, uint16_t index)
{
    if (valuePtr == 0)
        return 0;
    if (valuePtr->type != JSON_ARRAY)
        return 0;
    if (valuePtr->arrayCount <= index)
        return 0;
    return valuePtr->array[index];
}

void json_SetJsonObjectValue(JsonValue* valuePtr, JsonObject* object)
{
    if (valuePtr == 0)
        return;
    disposeValueContent(valuePtr);
    valuePtr->type = JSON_OBJECT;
    valuePtr->object = object;
    object->parent = object;
}

JsonObject* json_CreateObject(JsonValue* parentValue)
{
    JsonObject* result = (JsonObject*)MALLOC(sizeof(JsonObject));
    result->allocatedCount = 10;
    result->values = (JsonValue**)MALLOC(sizeof(JsonValue*) * (result->allocatedCount));;
    result->count = 0;
    result->parent = 0;
    if (parentValue)
        json_SetJsonObjectValue(parentValue, result);
    return result;
}

void json_DisposeObject(JsonObject* objectPtr)
{
    disposeObject(objectPtr);
}

void json_AppendValue(JsonObject* objectPtr, JsonValue* valuePtr)
{
    JsonValue** ar;
    if ((objectPtr == 0) || (valuePtr == 0))
        return;
    if (objectPtr->count >= objectPtr->allocatedCount)
    {
        objectPtr->allocatedCount *= 2;
        ar = (JsonValue**)MALLOC(sizeof(JsonValue*) * (objectPtr->allocatedCount));
    if (objectPtr->count)
        memcpy(ar, objectPtr->values, sizeof(JsonValue*) * objectPtr->count);
    if (objectPtr->values)
        FREE(objectPtr->values);
    objectPtr->values = ar;
    }
    objectPtr->values[objectPtr->count] = valuePtr;
    objectPtr->count++;
}

JsonValue* json_GetValue(JsonObject* objectPtr, const char* name)
{
    uint16_t i;
    if ((objectPtr == 0) || (objectPtr->values == 0))
        return 0;
    for(i = 0; i < objectPtr->count; i++)
        if ((objectPtr->values[i]) && (strcmp(objectPtr->values[i]->name, name) == 0))
            return objectPtr->values[i];
    return 0;
}

uint16_t json_Stringfy(JsonObject* objectPtr, char* dest, uint16_t maxSize)
{
    uint16_t origMax = maxSize;
    *dest = 0;
    maxSize = stringfyObject(objectPtr, dest, 0, maxSize);
    return origMax - maxSize;
}

static uint16_t parserSkipBlank(const char* source, uint8_t* ok)
{
    uint16_t index = 0;
    *ok = 1;
    while(*source != 0)
    {
        if ((*source != ' ') && (*source != '\t') && (*source != '\r') && (*source != '\n'))
            return index;
        source++;
        index++;
    }
    *ok = 0;
    return index;
}

static uint16_t parserGetString(const char* source, char* dest, int maxSize, uint8_t* ok)
{
    uint16_t index = 0;
    index += parserSkipBlank(source, ok);
    if (*ok == 0)
        return index;
    if (source[index] != '"')
    {
        *ok = 0;
        return index;
    }
    index++;
    while(maxSize - 1 > 0)
    {
        if (source[index] == 0)
        {
            *ok = 0;
            break;
        }
        if (source[index] == '\\')
        {
            index++;
            if (source[index] == 'r')
                *dest++ = '\r';
            else if (source[index] == 'n')
                *dest++ = '\n';
            else if (source[index] == 't')
                *dest++ = '\t';
            else if (source[index] == '\\')
                *dest++ = '\\';
            else if (source[index] == '"')
                *dest++ = '"';
            else
            {
                *ok = 0;
                break;
            }
            continue;
        }
        if (source[index] == '"')
        {
            *dest = 0;
            *ok = 1;
            return index + 1;
        }
        *dest = source[index];
        dest++;
        index++;
        maxSize--;
    }
    *ok = 0;
    return index;
}

static uint16_t parserGetWord(const char* source, char* dest, int maxSize, uint8_t* ok)
{
    uint16_t index = 0;
    *ok = 1;
    while((*source != 0) && (maxSize - 1 > 0))
    {
        if (!(((*source >= 'a') && (*source <= 'z')) ||
            ((*source >= 'A') && (*source <= 'Z')) ||
            ((*source >= '0') && (*source <= '9')) ||
            (*source == '_') ||
            (*source == '.')))
        {
            *dest = 0;
            return index;
        }
        *dest = *source;
        source++;
        dest++;
        index++;
    }
    *ok = 0;
    return index;

}

JsonObject* json_Parse(const char* source)
{
    JsonObject* result;
    JsonValue* value;
    ParserState state;
    uint16_t size;
    uint16_t index;
    uint8_t ok;
    uint8_t isArray;
    int objectNesting;
    uint8_t temp;
    if (source == 0)
        return 0;
    state = PARSER_OBJECT_START;
    size = (uint16_t)strlen(source);
    index = 0;
    ok = 1;
    result = 0;
    isArray = 0;
    objectNesting = 0;
    temp = 0;
    while(index < size)
    {
        switch(state)
        {
        case PARSER_OBJECT_START:
            index += parserSkipBlank(&source[index], &ok);
            if (ok == 0)
                break;
            if (source[index] != '{')
            {
                ok = 0;
                break;
            }
            index++;
            result = json_CreateObject(0);
            state = PARSER_VALUE_NAME;
            break;

        case PARSER_VALUE_NAME:
            index += parserGetString(&source[index], s_stringBuffer, MAX_STRING_SIZE, &ok);
            if (ok == 0)
                break;
            value = json_CreateValue(s_stringBuffer);
            json_AppendValue(result, value);
            state = PARSER_VALUE_COLON;
            break;

        case PARSER_VALUE_COLON:
            index += parserSkipBlank(&source[index], &ok);
            if (ok == 0)
                break;
            if (source[index] != ':')
            {
                ok = 0;
                break;
            }
            index++;
            state = PARSER_VALUE_VALUE;
            break;

        case PARSER_VALUE_VALUE:
            index += parserSkipBlank(&source[index], &ok);
            if (ok == 0)
                break;
            if (source[index] == '{')
            {
                if (isArray)
                {
                    ok = 0;
                    break;
                }
                state = PARSER_VALUE_TYPE_OBJECT;
                index++;
            }
            else if (source[index] == '[')
            {
                if (isArray)
                {
                    ok = 0;
                    break;
                }
                state = PARSER_VALUE_VALUE;
                isArray = 1;
                index++;
            }
            else if (source[index] == ']')
            {
                if (isArray)
                {
                    isArray = 0;
                    state = PARSER_VALUE_SEPARATOR;
                }
                else
                {
                    ok = 0;
                    break;
                }
                index++;
            }
            else if (source[index] == '"')
                state = PARSER_VALUE_TYPE_STRING;
            else if ((source[index] >= '0') && (source[index] <= '9'))
                state = PARSER_VALUE_TYPE_NUMBER;
            else if (source[index] == 'n')
                state = PARSER_VALUE_TYPE_NULL;
            else if ((source[index] == 't') || (source[index] == 'f'))
                state = PARSER_VALUE_TYPE_BOOL;
            else
                ok = 0;
            break;

        case PARSER_VALUE_TYPE_STRING:
            index += parserGetString(&source[index], s_stringBuffer, MAX_STRING_SIZE, &ok);
            if (ok == 0)
                break;
            if (isArray)
            {
                JsonValue* ar;
                int index = json_GetArrayValueCount(value);
                json_SetArrayValueCount(value, index + 1);
                ar = json_GetArrayValue(value, index);
                json_SetStringValue(ar, s_stringBuffer);
            }
            else
                json_SetStringValue(value, s_stringBuffer);
            state = PARSER_VALUE_SEPARATOR;
            break;

        case PARSER_VALUE_TYPE_NUMBER:
            index += parserGetWord(&source[index], s_stringBuffer, MAX_STRING_SIZE, &ok);
            if (ok == 0)
                break;
            if (isArray)
            {
                JsonValue* ar;
                int index = json_GetArrayValueCount(value);
                json_SetArrayValueCount(value, index + 1);
                ar = json_GetArrayValue(value, index);
                json_SetNumberValue(ar, __atoi(s_stringBuffer));
            }
            else
                json_SetNumberValue(value, __atoi(s_stringBuffer));
            state = PARSER_VALUE_SEPARATOR;
            break;

        case PARSER_VALUE_TYPE_NULL:
            index += parserGetWord(&source[index], s_stringBuffer, MAX_STRING_SIZE, &ok);
            if (ok == 0)
                break;
            if (strcmp(s_stringBuffer, "null") != 0)
            {
                ok = 0;
                break;
            }
            if (isArray)
            {
                JsonValue* ar;
                int index = json_GetArrayValueCount(value);
                json_SetArrayValueCount(value, index + 1);
                ar = json_GetArrayValue(value, index);
                json_SetNullValue(ar);
            }
            else
                json_SetNullValue(value);
            state = PARSER_VALUE_SEPARATOR;
            break;

        case PARSER_VALUE_TYPE_BOOL:
            index += parserGetWord(&source[index], s_stringBuffer, MAX_STRING_SIZE, &ok);
            if (ok == 0)
                break;
            if (strcmp(s_stringBuffer, "true") == 0)
                temp = 1;
            else if (strcmp(s_stringBuffer, "false") == 0)
                temp = 0;
            else
            {
                ok = 0;
                break;
            }
            if (isArray)
            {
                JsonValue* ar;
                int index = json_GetArrayValueCount(value);
                json_SetArrayValueCount(value, index + 1);
                ar = json_GetArrayValue(value, index);
                json_SetBoolValue(ar, temp);
            }
            else
                json_SetBoolValue(value, temp);
            state = PARSER_VALUE_SEPARATOR;
            break;

        case PARSER_VALUE_TYPE_OBJECT:
            s_parserObjects[objectNesting] = result;
            objectNesting++;
            if (objectNesting >= s_maxNestedObjectCount)
            {
                ok = 0;
                break;
            }
            result = json_CreateObject(value);
            state = PARSER_VALUE_NAME;
            break;

        case PARSER_VALUE_SEPARATOR:
            index += parserSkipBlank(&source[index], &ok);
            if (ok == 0)
                break;
            if ((source[index] == ',') && (isArray))
                state = PARSER_VALUE_VALUE;
            else if ((source[index] == ']') && (isArray))
            {
                isArray = 0;
                state = PARSER_VALUE_SEPARATOR;
            }
            else if (source[index] == ',')
                state = PARSER_VALUE_NAME;
            else if ((source[index] == '}') && (isArray == 0))
            {
                objectNesting--;
                if (objectNesting < 0)
                    index = size;
                else
                    result = s_parserObjects[objectNesting];
                state = PARSER_VALUE_SEPARATOR;
            }
            else
                ok = 0;
            index++;
            break;

        }
        if (ok == 0)
            break;
    }
    if (objectNesting != -1)
        ok = 0;
    if ((ok == 0) && (result))
    {
        json_DisposeObject(result);
        result = 0;
    }
    return result;
}

#ifdef __cplusplus
}
#endif
