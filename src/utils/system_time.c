/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   system_time.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/utils/system_time.h>
#include <ubeam/drivers/clock_management.h>
#if (UBEAM_CPU_CORE_ARCH == UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M)
#include <ubeam/cortex-m/utils/system_time.h>
#elif (UBEAM_CPU_CORE_ARCH == UBEAM_CPU_CORE_ARCH_X86_64)
#include <ubeam/x86/utils/system_time.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

uint64_t s_ticksPerSec = 0;
uint64_t s_ticksPerMilli = 0;
uint64_t s_ticksPerMicro = 0;
uint64_t s_nanosPerTick = 0;

void UBSysTime_Configure()
{
#if (UBEAM_CPU_CORE_ARCH == UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M)
	UBCortexSysTime_Configure();
	s_ticksPerSec = UBCortexSysTime_GetTicksPerSec();
    s_ticksPerMilli = s_ticksPerSec / 1000;
    s_ticksPerMicro = s_ticksPerSec / 1000000;
    s_nanosPerTick = 1000000000 / s_ticksPerSec;
#elif (UBEAM_CPU_CORE_ARCH == UBEAM_CPU_CORE_ARCH_X86_64)
    s_ticksPerSec = UBx86SysTime_GetTicksPerSec();
    s_ticksPerMilli = s_ticksPerSec / 1000;
    s_ticksPerMicro = s_ticksPerSec / 1000000;
    s_nanosPerTick = 1000000000 / s_ticksPerSec;
#endif
}

void UBSysTime_Stop()
{
#if (UBEAM_CPU_CORE_ARCH == UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M)
	UBCortexSysTime_Stop();
#endif
}

bool UBSysTime_HasPassed(UBTimestamp ts)
{
	return UBSysTime_GetTimestamp() >= ts;
}

UBTimestamp UBSysTime_GetFuture(UBTimeInterval interval)
{
    return UBSysTime_GetTimestamp() + interval;
}

UBTimestamp UBSysTime_GetFutureMilli(uint32_t milli)
{
    return UBSysTime_GetTimestamp() + milli * s_ticksPerMilli;
}

UBTimestamp UBSysTime_GetFutureMicro(uint32_t micro)
{
	return UBSysTime_GetTimestamp() + micro * s_ticksPerMicro;
}

UBTimestamp UBSysTime_GetFutureNano(uint32_t nano)
{
	return UBSysTime_GetTimestamp() + (nano / s_nanosPerTick);
}

UBTimeInterval UBSysTime_GetIntervalMilli(uint32_t milli)
{
	return milli * s_ticksPerMilli;
}

UBTimeInterval UBSysTime_GetIntervalMicro(uint32_t micro)
{
	return micro * s_ticksPerMicro;
}

UBTimeInterval UBSysTime_GetIntervalNano(uint32_t nano)
{
	return nano / s_nanosPerTick;
}

UBTimeInterval UBSysTime_GetInterval(UBTimestamp ts1, UBTimestamp ts2);

void UBSysTime_Delay(UBTimeInterval interval)
{
	UBTimestamp ts = UBSysTime_GetTimestamp() + interval;
	while(!UBSysTime_HasPassed(ts))
	{}
}

void UBSysTime_DelayMilli(uint32_t milli)
{
	UBTimestamp ts = UBSysTime_GetFutureMilli(milli);
	while(!UBSysTime_HasPassed(ts))
	{}
}

void UBSysTime_DelayMicro(uint32_t micro)
{
	UBTimestamp ts = UBSysTime_GetFutureMicro(micro);
	while(!UBSysTime_HasPassed(ts))
	{}
}

void UBSysTime_DelayNano(uint32_t nano)
{
	UBTimestamp ts = UBSysTime_GetFutureNano(nano);
	while(!UBSysTime_HasPassed(ts))
	{}
}

#ifdef __cplusplus
}
#endif
