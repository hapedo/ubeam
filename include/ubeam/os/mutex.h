/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   mutex.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_OS_MUTEX_H_
#define _UBEAM_OS_MUTEX_H_

#include <stdbool.h>
#include <ubeam/setup.h>

#if (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_NONE)
#include <ubeam/none/os/mutex_types.h>
#elif (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_WINDOWS)
#include <ubeam/windows/os/mutex_types.h>
#endif

/*! \defgroup ubeam_os_mutex Mutex
 *  \ingroup ubeam_os
 *  \brief Mutex
 */

/*! \addtogroup ubeam_os_mutex
 *  @{
 */

//! Mutex struct
typedef struct
{
#ifdef UBEAM_MUTEX_PLATFORM
	UBMutex_Pl platform;						//!< Platfrom dependent data
#endif
	bool isRecursive;							//!< Mutex is recursive
} UBMutex;

#if (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_NONE)
#include <ubeam/none/os/mutex.h>
#elif (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_WINDOWS)
#include <ubeam/windows/os/mutex.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure mutex
 *  \param mutex UBMutex struct instance ptr (must not be NULL)
 *  \param isRecursive Mutex is created as recursive when true
 *  \param lock Lock on create when true
 */
void UBMutex_Configure(UBMutex* mutex, bool isRecursive, bool lock);

/*! Lock mutex. Asserts when already locked (when not recursive) or locked by another thread (when recursive).
 *  \param mutex UBMutex struct instance ptr (must not be NULL)
 */
void UBMutex_Lock(UBMutex* mutex);

/*! Try to lock mutex
 *  \param mutex UBMutex struct instance ptr (must not be NULL)
 *  \return true on success
 */
bool UBMutex_TryLock(UBMutex* mutex);

/*! Unlock mutex. Asserts when already not locked or locked by another thread.
 *  \param mutex UBMutex struct instance ptr (must not be NULL)
 */
void UBMutex_Unlock(UBMutex* mutex);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_OS_MUTEX_H_
