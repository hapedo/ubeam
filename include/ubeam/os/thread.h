/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   thread.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_OS_THREAD_H_
#define _UBEAM_OS_THREAD_H_

#include <stdbool.h>
#include <stdint.h>
#include <ubeam/setup.h>

#if (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_NONE)
#include <ubeam/none/os/thread_types.h>
#elif (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_WINDOWS)
#include <ubeam/windows/os/thread_types.h>
#endif

/*! \defgroup ubeam_os_thread Thread
 *  \ingroup ubeam_os
 *  \brief Thread
 */

/*! \addtogroup ubeam_os_thread
 *  @{
 */

//! Thread state
typedef enum
{
	UBTHRS_NOT_CONFIGURED = 0,					//!< Created but not configured
	UBTHRS_STOPPED,								//!< Stopped
	UBTHRS_STARTING,							//!< Starting (not started yet)
	UBTHRS_RUNNING,								//!< Running
	UBTHRS_STOPPING,							//!< Stopping
	UBTHRS_SUSPENDING,							//!< Suspending
	UBTHRS_SUSPENDED,							//!< Suspended
	UBTHRS_FINISHED								//!< Finished
} UBThreadState;

//! Thread struct
typedef struct
{
#ifdef UBEAM_THREAD_PLATFORM
	UBThread_Pl platform;						//!< Platfrom dependent data
#endif
	UBThreadState state;						//!< Current thread state
	UBThreadId id;								//!< Thread ID
	void* onInitCb;								//!< Callback that gets called on thread init
	void* onStartCb;							//!< Callback that gets called on thread start
	void* onStopCb;								//!< Callback that gets called on thread stop
	void* execCb;								//!< Thread entry point
	void* customData;							//!< Custom data (user data)
} UBThread;

#if (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_NONE)
#include <ubeam/none/os/mutex.h>
#elif (UBEAM_PLATFORM_TYPE == UBEAM_PLATFORM_TYPE_WINDOWS)
#include <ubeam/windows/os/mutex.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

//! Thread init callback
typedef void(UBThreadOnInitCb)(UBThread* thread);
//! Thread start callback
typedef void(UBThreadOnStartCb)(UBThread* thread);
//! Thread stop callback
typedef void(UBThreadOnStopCb)(UBThread* thread);
//! Thread entry point callback
typedef void(UBThreadExecuteCb)(UBThread* thread);

/*! Get current thread ID
 *  \return Current thread ID. Returns 0 when scheduller not running.
 */
UBThreadId UBThread_GetCurrentId();

/*! Get current thread
 * \return Pointer to current thread struct instance
 */
const UBThread* UBThread_GetCurrent();

/*! This shall be called from the beggining of main()
*/
void UBThread_Init();

/*! Start OS scheduler
 */
void UBThread_StartScheduler();

/*! Wait for all threads to finish. Blocks until all threads get finished.
 */
void UBThread_WaitForAllThreads();

/*! Get thread current state
 *  \param thread Pointer to thread struct instance (must not be NULL)
 */
UBThreadState UBThread_GetState(const UBThread* thread);

/*! Configure thread
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \param initCb Init callback (may be NULL)
 *  \param startCb Start callback (may be NULL)
 *  \param stopCb Stop callback (may be NULL)
 *  \param exec Thread entry point
 */
void UBThread_Configure(UBThread* thread, UBThreadOnInitCb* initCb, UBThreadOnStartCb* startCb, UBThreadOnStopCb* stopCb, UBThreadExecuteCb* exec);

/*! Set thread custom data (user data)
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \param customData Data to be set
 */
void UBThread_SetCustomData(UBThread* thread, void* customData);

/*! Get thread custom data (user data)
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \return Thread custom data (user data)
 */
void* UBThread_GetCustomData(const UBThread* thread);

/*! Start thread
 *  \param thread Pointer to thread struct instance (must not be NULL)
 */
void UBThread_Start(UBThread* thread);

/*! Stop thread
 *  \param thread Pointer to thread struct instance (must not be NULL)
 */
void UBThread_Stop(UBThread* thread);

/*! Suspend thread
 *  \param thread Pointer to thread struct instance (must not be NULL)
 */
void UBThread_Suspend(UBThread* thread);

/*! Sleep thread for given time
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \param interval Time to sleep for
 */
void UBThread_Sleep(UBThread* thread, UBTimeInterval interval);

/*! Sleep thread for given time in milliseconds
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \param milli Time to sleep for (in milliseconds)
 */
void UBThread_SleepMilli(UBThread* thread, uint32_t milli);

/*! Check whether thread should terminate. This is typically used in thread loop to notify terminating.
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \return TRUE when thread is in UBTHRS_STOPPING state
 */
bool UBThread_ShouldTerminate(const UBThread* thread);

/*! Check whether thread should suspend. This is typically used in thread loop to notify suspending.
 *  \param thread Pointer to thread struct instance (must not be NULL)
 *  \return TRUE when thread is in UBTHRS_SUSPENDING state
 */
bool UBThread_ShouldSuspend(const UBThread* thread);

/*! Force OS scheduler to switch threads
 */
void UBThread_Yield();

/*! Force OS to terminate all running threads
*/
void UBThread_TerminateAll();

/*! Check whether all threads should be terminated
 *  \return true when terminate all
 */
bool UBThread_ShouldTerminateAll();

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_OS_THREAD_H_
