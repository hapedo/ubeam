/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   json.h
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_UTILS_JSON_H_
#define _UBEAM_UTILS_JSON_H_

#include <stdint.h>

/*! \defgroup ubeam_utils_json JSON encoder and decoder
 *  \ingroup ubeam_utils
 *  \brief JSON encoder and decoder
 */

/*! \addtogroup ubeam_utils_json
 *  @{
 */

//! Maximal value name string length
#define JSON_VALUE_NAME_LENGTH          32
//! Maximal value string length
#define JSON_VALUE_STRING_VALUE_LENGTH  64

typedef struct JsonValueStruct JsonValue;

typedef struct JsonObjectStruct JsonObject;

//! JSON value type
typedef enum
{
    JSON_STRING,                    //!< String value
    JSON_NUMBER,                    //!< Numeric value
    JSON_ARRAY,                     //!< Array value
    JSON_BOOLEAN,                   //!< Boolean value (true/false)
    JSON_NULL,                      //!< Null value (null)
    JSON_OBJECT                     //!< Object value
} JsonType;

//! JSON value
typedef struct JsonValueStruct
{
    JsonType type;                  //!< Value type
    char name[JSON_VALUE_NAME_LENGTH];                     //!< Value name
    char valueString[JSON_VALUE_STRING_VALUE_LENGTH];      //!< Value string value
    int valueNumber;                //!< Value numeric value
    JsonValue** array;              //!< Value array
    uint16_t arrayCount;            //!< Value array item count
    JsonObject* object;             //!< Value object
    uint8_t __allocated;
} JsonValue;

//! JSON object
typedef struct JsonObjectStruct
{
    JsonValue** values;             //!< Object values
    uint16_t count;                 //!< Object value count
    uint16_t allocatedCount;        //!< Object allocated value count
    JsonObject* parent;             //!< Parent value (object owner)
} JsonObject;

#ifdef __cplusplus
extern "C"
{
#endif

void json_Init(uint16_t maxNestedObjectCount, uint16_t maxValueCount);

/*! Create JSON value
 *  \param name Value name
 *  \return New JSON value pointer
 */
JsonValue* json_CreateValue(const char* name);

/*! Dispose JSON value
 *  \param valuePtr Value pointer to dispose
 */
void json_DisposeValue(JsonValue* valuePtr);

/*! Set string value
 *  \param valuePtr Value instance
 *  \param value String to be set
 */
void json_SetStringValue(JsonValue* valuePtr, const char* value);

/*! Set number value
 *  \param valuePtr Value instance
 *  \param value Value to be set
 */
void json_SetNumberValue(JsonValue* valuePtr, int value);

/*! Set boolean value
 *  \param valuePtr Value instance
 *  \param value Boolean value to be set (1 = true, 0 = false)
 */
void json_SetBoolValue(JsonValue* valuePtr, uint8_t value);

/*! Set null value
 *  \param valuePtr Value instance
 */
void json_SetNullValue(JsonValue* valuePtr);

/*! Set array item count
 *  \param valuePtr Value instance
 *  \param count New array item count
 */
void json_SetArrayValueCount(JsonValue* valuePtr, uint16_t count);

/*! Get array item count
 *  \param valuePtr Value instance
 *  \return Number of items (0 if not array type)
 */
uint16_t json_GetArrayValueCount(JsonValue* valuePtr);

/*! Get array item value instance
 *  \param valuePtr Value instance
 *  \param index Array item index
 *  \return Array value item instance, 0 if not array type or index out of range
 */
JsonValue* json_GetArrayValue(JsonValue* valuePtr, uint16_t index);

/*! Set value object
 *  \param valuePtr Value instance
 *  \param object JSON object to be set
 */
void json_SetJsonObjectValue(JsonValue* valuePtr, JsonObject* object);

/*! Create JSON object
 *  \param parentValue Object parent value instance (object owner), 0 if no parent
 *  \return New JSON object instance
 */
JsonObject* json_CreateObject(JsonValue* parentValue);

/*! Dispose JSON object with all content
 *  \param objectPtr Object instance to be disposed
 */
void json_DisposeObject(JsonObject* objectPtr);

/*! Append value to JSON object
 *  \param objectPtr Object instance
 *  \param valuePtr Value instance
 */
void json_AppendValue(JsonObject* objectPtr, JsonValue* valuePtr);

/*! Search JSON object for value
 *  \param name Value name to be found
 *  \return Object value instance, 0 if not found
 */
JsonValue* json_GetValue(JsonObject* objectPtr, const char* name);

/*! Stringfy JSON object
 *  \param objectPtr Object instance to be stringfied
 *  \param dest Destination buffer
 *  \param maxSize Maximal size of output buffer
 *  \return Stringied size
 */
uint16_t json_Stringfy(JsonObject* objectPtr, char* dest, uint16_t maxSize);

/*! Parse text JSON and biuld JSON object
 *  \param source Source text buffer
 *  \return New JSON object instance, 0 on parse error
 */
JsonObject* json_Parse(const char* source);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_UTILS_JSON_H_
