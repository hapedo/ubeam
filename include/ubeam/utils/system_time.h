/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   system_time.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_UTILS_SYSTEM_TIME_H_
#define _UBEAM_UTILS_SYSTEM_TIME_H_

#include <stdbool.h>
#include <ubeam/setup.h>

/*! \defgroup ubeam_utils_systime System time
 *  \ingroup ubeam_utils
 *  \brief System time functions
 */

/*! \addtogroup ubeam_utils_systime
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure system time
 */
void UBSysTime_Configure();

/*! Stop system time
 */
void UBSysTime_Stop();

/*! Get current timestamp
 *  \return Current timestamp
 */
UBTimestamp UBSysTime_GetTimestamp();

/*! Check whether timestamp has passed (current > given)
 *  \param timestamp Timestamp to be checked
 *  \return true when given timestamp has passed (current > given)
 */
bool UBSysTime_HasPassed(UBTimestamp timestamp);

/*! Get future timestamp
 *  \param interval Interval from now
 *  \return Future timestamp (current + interval)
 */
UBTimestamp UBSysTime_GetFuture(UBTimeInterval interval);

/*! Get future timestamp. Interval in milliseconds.
 *  \param milli Interval in milliseconds from now
 *  \return Future timestamp (current + interval)
 */
UBTimestamp UBSysTime_GetFutureMilli(uint32_t milli);

/*! Get future timestamp. Interval in microseconds.
 *  \param micro Interval in microseconds from now
 *  \return Future timestamp (current + interval)
 */
UBTimestamp UBSysTime_GetFutureMicro(uint32_t micro);

/*! Get future timestamp. Interval in nanoseconds.
 *  \param nano Interval in nanoseconds from now
 *  \return Future timestamp (current + interval)
 */
UBTimestamp UBSysTime_GetFutureNano(uint32_t nano);

/*! Convert milliseconds to UBTimeInterval
 *  \param milli Milliseconds to convert
 *  \return UBTimeInterval value
 */
UBTimeInterval UBSysTime_GetIntervalMilli(uint32_t milli);

/*! Convert microseconds to UBTimeInterval
 *  \param micro Microseconds to convert
 *  \return UBTimeInterval value
 */
UBTimeInterval UBSysTime_GetIntervalMicro(uint32_t micro);

/*! Convert nanoseconds to UBTimeInterval
 *  \param nano Nanoseconds to convert
 *  \return UBTimeInterval value
 */
UBTimeInterval UBSysTime_GetIntervalNano(uint32_t nano);

/*! Calculate time interval between two timestamps
 *  \param ts1 First timestamp
 *  \param ts2 Second timestamp
 *  \return ts1 - ts2. Returns 0 when ts1 <= ts2
 */
UBTimeInterval UBSysTime_GetInterval(UBTimestamp ts1, UBTimestamp ts2);

/*! Active waiting for given amount of time
 *  \param interval Interval to wait
 */
void UBSysTime_Delay(UBTimeInterval interval);

/*! Active waiting for given amount of milliseconds
 *  \param milli Milliseconds to wait
 */
void UBSysTime_DelayMilli(uint32_t milli);

/*! Active waiting for given amount of microseconds
 *  \param micro Microseconds to wait
 *  \return UBTimeInterval value
 */
void UBSysTime_DelayMicro(uint32_t micro);

/*! Active waiting for given amount of nanoseconds
 *  \param nano Nanoseconds to wait
 *  \return UBTimeInterval value
 */
void UBSysTime_DelayNano(uint32_t nano);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_UTILS_SYSTEM_TIME_H_
