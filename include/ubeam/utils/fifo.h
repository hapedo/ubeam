/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   fifo.h
 * \brief  microBeam SDK
 * \date   7.11.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_UTILS_FIFO_H_
#define _UBEAM_UTILS_FIFO_H_

#include <stdint.h>
#include <stdbool.h>
#include <ubeam/setup.h>

/*! \defgroup ubeam_utils_fifo General purpose FIFO container
 *  \ingroup ubeam_utils
 *  \brief General purpose FIFO container
 */

/*! \addtogroup ubeam_utils_fifo
 *  @{
 */

//! FIFO struct
typedef struct
{
	uint8_t* buffer;					//!< Data buffer ptr
	uint32_t size;						//!< Data buffer size
	uint32_t start;						//!< Start index of valid data
	uint32_t end;						//!< End index of valid data
} UBFifo;

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \param buffer Data buffer ptr (must not be NULL)
 *  \param size Size of data buffer
 */
void UBFifo_Configure(UBFifo* fifo, uint8_t* buffer, uint32_t size);

/*! Remove all data from FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 */
void UBFifo_Clear(UBFifo* fifo);

/*! Put single byte to FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \param data Byte to be put
 *  \return TRUE on success, FALSE when FIFO is full
 */
bool UBFifo_PutByte(UBFifo* fifo, uint8_t data);

/*! Put data to FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \param data Data to be put (must not be NULL)
 *  \param dataSize Size of data
 *  \return Size that has been enqueued (may be less than dataSize when FIFO gets full)
 */
uint32_t UBFifo_Put(UBFifo* fifo, const uint8_t* data, uint32_t dataSize);

/*! Get data size in FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \return Number of bytes in FIFO
 */
uint32_t UBFifo_GetCount(UBFifo* fifo);

/*! Get free space size in FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \return Number of free bytes available in FIFO
 */
uint32_t UBFifo_GetAvailable(UBFifo* fifo);

/*! Get single byte from FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \param data Data destination ptr
 *  \return 1 on success, 0 when no data in FIFO
 */
uint32_t UBFifo_GetByte(UBFifo* fifo, uint8_t* data);

/*! Get data from FIFO
 *  \param fifo FIFO struct ptr (must not be NULL)
 *  \param data Data destination ptr
 *  \param dataSize Size of data to be get
 *  \return Number of bytes get from FIFO, 0 when no data in FIFO
 */
uint32_t UBFifo_GetData(UBFifo* fifo, uint8_t* data, uint32_t dataSize);

/*! Move data from source FIFO to destination FIFO
 *  \param fifoTo Destination FIFO struct ptr (must not be NULL)
 *  \param fifoFrom Source FIFO struct ptr (must not be NULL)
 *  \return Number of data moved
 */
uint32_t UBFifo_Move(UBFifo* fifoTo, UBFifo* fifoFrom);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_UTILS_FIFO_H_
