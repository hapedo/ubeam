/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   setup.h
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_SETUP_H_
#define _UBEAM_SETUP_H_

#include <stdint.h>
#include <stdbool.h>
#include <ubeam/config.h>
#include <ubeam/setup_definitions.h>

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UBEAM_CPU_CORE_ARCH
    #define UBEAM_CPU_CORE_ARCH                     UBEAM_CPU_CORE_ARCH_UNKNOWN 
#endif

#ifndef UBEAM_CPU_CORE_TYPE
    #define UBEAM_CPU_CORE_TYPE                     UBEAM_CPU_CORE_TYPE_UNKNOWN 
#endif

#ifndef UBEAM_CPU_MODEL
    #define UBEAM_CPU_MODEL                         UBEAM_CPU_MODEL_UNKNOWN
#endif

#ifndef UBEAM_PLATFORM_TYPE
#define UBEAM_PLATFORM_TYPE							UBEAM_PLATFORM_TYPE_NONE
#endif

/*******************************************************************************
    DO NOT MODIFY CODE BELOW !
*******************************************************************************/

#define UBEAM_CPU_CODE              ((UBEAM_CPU_CORE_ARCH << 24) | (UBEAM_CPU_CORE_TYPE << 16) | UBEAM_CPU_MODEL)

// Set manufacturer
#if ((UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x4) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x6) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x8) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030xC) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F031) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F042) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F051) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F070x6) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F070xB) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F072) || \
	 (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F091))
    #define UBEAM_CPU_MANUF                         UBEAM_CPU_MANUF_STM
    #define UBEAM_CPU_FAMILY                        UBEAM_CPU_FAMILY_STM32F0
#elif ((UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L100xB) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L100xBA) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L100xC) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xB) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xC) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xCA) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xD) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xDX) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xE) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xB) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xBA) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xC) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xCA) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xD) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xDX) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xE) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xC) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xCA) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xD) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xDX) || \
     (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xE))
    #define UBEAM_CPU_MANUF                         UBEAM_CPU_MANUF_STM
    #define UBEAM_CPU_FAMILY                        UBEAM_CPU_FAMILY_STM32L1
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_GENERIC_X86)
    #define UBEAM_CPU_MANUF                         UBEAM_CPU_MANUF_INTEL
    #define UBEAM_CPU_FAMILY                        UBEAM_CPU_FAMILY_X86
#else
    #error Unknown CPU settings combination!
    #define UBEAM_CPU_MANUF                         UBEAM_CPU_MANUF_UNKNOWN
    #define UBEAM_CPU_FAMILY                        UBEAM_CPU_FAMILY_UNKNOWN
#endif

// Platform / architecture related definitions

#if (UBEAM_CPU_CORE_TYPE == UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
    //! Peripheral handle
    typedef void*                                   UBPeriHandle;
    //! GPIO pin mask
    typedef uint32_t                                UBGpioPinMask;
    //! Interrupt id
    typedef int8_t                                  UBIntId;
    //! Interrupt priority
    typedef uint32_t								UBIntPrio;
    //! Timestamp type
    typedef uint64_t								UBTimestamp;
    //! TimeInterval type
    typedef uint64_t								UBTimeInterval;
    //! Maximal number of threads
#ifndef UBEAM_MAX_THREAD_COUNT
	#define UBEAM_MAX_THREAD_COUNT					16
#endif // UBEAM_MAX_THREAD_COUNT
#endif

#if (UBEAM_CPU_CORE_TYPE == UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M3)
    //! Peripheral handle
    typedef void*                                   UBPeriHandle;
    //! GPIO pin mask
    typedef uint32_t                                UBGpioPinMask;
    //! Interrupt id
    typedef int8_t                                  UBIntId;
    //! Interrupt priority
    typedef uint32_t								UBIntPrio;
    //! Timestamp type
    typedef uint64_t								UBTimestamp;
    //! TimeInterval type
    typedef uint64_t								UBTimeInterval;
    //! Maximal number of threads
#ifndef UBEAM_MAX_THREAD_COUNT
	#define UBEAM_MAX_THREAD_COUNT					16
#endif // UBEAM_MAX_THREAD_COUNT
#endif

#if (UBEAM_CPU_CORE_TYPE == UBEAM_CPU_CORE_TYPE_X86)
    //! Peripheral handle
    typedef void*                                   UBPeriHandle;
    //! GPIO pin mask
    typedef uint32_t                                UBGpioPinMask;
    //! Interrupt id
    typedef int8_t                                  UBIntId;
    //! Interrupt priority
    typedef uint32_t								UBIntPrio;
    //! Timestamp type
    typedef uint64_t								UBTimestamp;
    //! TimeInterval type
    typedef uint64_t								UBTimeInterval;
    //! Maximal number of threads
#ifndef UBEAM_MAX_THREAD_COUNT
	#define UBEAM_MAX_THREAD_COUNT					64
#endif // UBEAM_MAX_THREAD_COUNT
#endif

//! Frequency in Hz
typedef uint32_t UBFrequency;

#if defined(__GNUC__)
#define UBEAM_INLINE								inline
#endif // __GNUC__

#if !defined(__STATIC_INLINE)
#define __STATIC_INLINE static inline
#endif

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_SETUP_H_
