/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   spi_master.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_SPI_MASTER_H_
#define _UBEAM_DRIVERS_SPI_MASTER_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/spi_master_types.h>

/*! \defgroup ubeam_drivers_spimaster SPI master
 *  \ingroup ubeam_drivers
 *  \brief SPI master driver
 */

/*! \addtogroup ubeam_drivers_spimaster
 *  @{
 */

//! SPI master driver struct
typedef struct
{
	UBPeripheral peri;							//!< Peripheral instance
	UBGpioPin* mosi;							//!< MOSI pin instance ptr
	UBGpioPin* miso;							//!< MISO pin instance ptr
	UBGpioPin* sck;								//!< SCK pin instance ptr
	UBGpioPin* cs;								//!< CS pin instance ptr
	void* platform;								//!< Platform dependent data
} UBSpiMasterBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
#include <ubeam/stm32f0/drivers/spi_master.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/spi_master.h>
#else
typedef struct
{
    UBSpiMasterBase base;
} UBSpiMaster;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure SPI master driver. Also performs GPIO pin configuration and power enable.
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \param handle SPI peripheral handle
 *  \param mosi MOSI pin instance ptr (may be NULL)
 *  \param miso MISO pin instance ptr (may be NULL)
 *  \param sck SCK pin instance ptr (may be NULL)
 *  \param cs CS pin instance ptr (may be NULL)
 */
void UBSpiMaster_Configure(UBSpiMaster* master, UBPeriHandle handle, UBGpioPin* mosi, UBGpioPin* miso, UBGpioPin* sck, UBGpioPin* cs);

/*! Enable SPI interrupts
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 */
void UBSpiMaster_EnableInt(UBSpiMaster* master);

/*! Disable SPI interrupts
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 */
void UBSpiMaster_DisableInt(UBSpiMaster* master);

/*! Get driver current state
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \return Current state (UBSpiMasterState enum value)
 */
UBSpiMasterState UBSpiMaster_GetState(UBSpiMaster* master);

/*! Initialize SPI master driver
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \param cpol CPOL settings
 *  \param cpha CPHA settings
 *  \param msbFirst MSB first when set to true
 *  \return UBSMR_OK on success
 */
UBSpiMasterResult UBSpiMaster_Init(UBSpiMaster* master, UBSpiMasterCpol cpol, UBSpiMasterCpha cpha, bool msbFirst);

/*! Initialize SPI master driver with specific SCK frequency
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \param sckFreq SCK frequency in Hz to be set
 *  \param cpol CPOL settings
 *  \param cpha CPHA settings
 *  \param msbFirst MSB first when set to true
 *  \return UBSMR_OK on success
 */
UBSpiMasterResult UBSpiMaster_InitWithFreq(UBSpiMaster* master, UBFrequency sckFreq, UBSpiMasterCpol cpol, UBSpiMasterCpha cpha, bool msbFirst);

/*! Set SCK frequency in Hz
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \param sckFreq SCK frequency in Hz to be set
 *  \return UBSMR_OK on success
 */
UBSpiMasterResult UBSpiMaster_SetSckFrequency(UBSpiMaster* master, UBFrequency sckFreq);

/*! Get current SCK frequency in Hz
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \return Current SCK frequency in Hz (0 on error)
 */
UBFrequency UBSpiMaster_GetSckFrequency(UBSpiMaster* master);

/*! Perform synchronous (blocking) transaction
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \param txData Data to be transmitted ptr. 0xFF is transmitted when set to NULL.
 *  \param rxData Pointer to rx data buffer (may be NULL)
 *  \param size Transaction size in bytes
 *  \return UBSMR_OK on success
 */
UBSpiMasterResult UBSpiMaster_TransceiveSync(UBSpiMaster* master, const uint8_t* txData, uint8_t* rxData, uint16_t size);

/*! Perform asynchronous (non-blocking) transaction. Check driver state for finish detection.
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \param txData Data to be transmitted ptr. 0xFF is transmitted when set to NULL.
 *  \param rxData Pointer to rx data buffer (may be NULL)
 *  \param size Transaction size in bytes
 *  \return UBSMR_OK on success
 */
UBSpiMasterResult UBSpiMaster_TransceiveAsync(UBSpiMaster* master, const uint8_t* txData, uint8_t* rxData, uint16_t size);

/*! Terminate pending asynchronous transaction
 *  \param master SPI master driver struct instance ptr (must not be NULL)
 *  \return UBSMR_OK on success
 */
UBSpiMasterResult UBSpiMaster_TerminateAsync(UBSpiMaster* master);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_SPI_MASTER_H_
