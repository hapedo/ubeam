/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   rtc_types.h
 * \brief  microBeam SDK
 * \date   31.7.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_RTC_TYPES_H_
#define _UBEAM_DRIVERS_RTC_TYPES_H_

#include <stdint.h>
#include <stdbool.h>

/*! \defgroup ubeam_drivers_rtc RTC
 *  \ingroup ubeam_drivers
 *  \brief RTC driver
 */

/*! \addtogroup ubeam_drivers_rtc
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

//! RTC driver state
typedef enum
{
    UBRTCS_NOT_CONFIGURED = 0,              //!< Driver not configured
    UBRTCS_NOT_INITIALIZED,                 //!< Driver not initialized
    UBRTCS_BUSY,                            //!< Driver is busy
    UBRTCS_READY                            //!< Driver ready
} UBRtcState;

//! RTC result
typedef enum
{
    UBRTCR_OK = 0,                          //!< Success
    UBRTCR_NOT_CONFIGURED,                  //!< Driver not configured
    UBRTCR_NOT_INITIALIZED,                 //!< Driver not initialized
    UBRTCR_NOT_SUPPORTED,                   //!< Operation not supported
    UBRTCR_BAD_PARAM,                       //!< Bad parameter
    UBRTCR_BUSY,                            //!< Driver is busy
    UBRTCR_ERROR                            //!< General error
} UBRtcResult;

//! RTC day of week
typedef enum
{
    UBRTCWD_SUNDAY = 0,
    UBRTCWD_MONDAY,
    UBRTCWD_TUESDAY,
    UBRTCWD_WEDNESDAY,
    UBRTCWD_THURSDAY,
    UBRTCWD_FRIDAY,
    UBRTCWD_SATURDAY
} UBRtcWeekDay;

//! RTC date time structure
typedef struct
{
    uint8_t hour;                           //!< Hour (0 to 23)
    uint8_t minute;                         //!< Minute (0 to 59)
    uint8_t second;                         //!< Second (0 to 59)
    uint16_t milli;                         //!< Millisecond (may not be supported by all platforms)
    uint8_t day;                            //!< Day (1 to 31)
    uint8_t month;                          //!< Month (1 to 12)
    uint16_t year;                          //!< Year (1970 to 2099)
    UBRtcWeekDay weekDay;                   //!< Day of week (may not be supported by all platforms)
} UBRtcDateTime;

//! RTC alarm structure
typedef struct
{
    uint8_t hour;                           //!< Alarm hour
    uint8_t minute;                         //!< Alarm minute
    bool dayMaskEnabled;                    //!< Day mask is applied when true
    uint8_t dayMask;                        //!< Day mask (bit 0 = Sun, bit 1 = Mon, ...)
    bool dateEnabled;                       //!< Enable day / month alarm condition
    uint8_t day;                            //!< Alarm day
    uint8_t month;                          //!< Alarm month
} UBRtcAlarm;

//! RTC timestamp
typedef uint32_t UBRtcTimestamp;

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_RTC_TYPES_H_
