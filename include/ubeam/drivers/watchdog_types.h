/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   watchdog_types.h
 * \brief  microBeam SDK
 * \date   10.08.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_WATCHDOG_TYPES_H_
#define _UBEAM_DRIVERS_WATCHDOG_TYPES_H_

/*! \addtogroup ubeam_drivers_watchdog
 *  @{
 */

//! Watchdog driver result
typedef enum
{
	UBWDGR_OK = 0,								//!< Result OK (no error)
	UBWDGR_NOT_CONFIGURED,						//!< Driver not configured
	UBWDGR_NOT_INITIALIZED,						//!< Driver configured but not initialized
	UBWDGR_BUSY,								//!< Driver is busy
	UBWDGR_LOCKED,								//!< Driver is locked by another thread
	UBWDGR_NOT_SUPPORTED,						//!< Function not supported
    UBWDGR_BAD_PARAM,                           //!< Bad parameter given
	UBWDGR_ERROR								//!< Generic error
} UBWdgResult;

//! Watchdog driver state
typedef enum
{
	UBWDGS_NOT_CONFIGURED = 0,					//!< Driver not configured
	UBWDGS_NOT_INITIALIZED,						//!< Driver configured but not initialized
	UBWDGS_IDLE,								//!< Driver is idle
	UBWDGS_BUSY,								//!< Driver is busy
} UBWdgState;

/*! @} */

#endif // _UBEAM_DRIVERS_WATCHDOG_TYPES_H_
