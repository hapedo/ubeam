/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   interrupt_ctrl.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_INTERRUPT_CTRL_H_
#define _UBEAM_DRIVERS_INTERRUPT_CTRL_H_

#include <stdbool.h>
#include <ubeam/setup.h>

#if (UBEAM_CPU_CORE_ARCH == UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M)
#include <ubeam/cortex-m/drivers/nvic.h>
#endif

/*! \defgroup ubeam_drivers_interruptctrl Interrupt controller
 *  \ingroup ubeam_drivers
 *  \brief Interrupt controller
 */

/*! \addtogroup ubeam_drivers_interruptctrl
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*! Enable global interrupts
 */
void UBInt_EnableGlobal();

/*! Disable global interrupts
 *  \return true when interrupts were enabled, false otherwise
 */
bool UBInt_DisableGlobal();

/*! Enable specific interrupt
 *  \param id Interrupt ID
 */
void UBInt_EnableInt(UBIntId id);

/*! Disable specific interrupt
 *  \param id Interrupt ID
 */
void UBInt_DisableInt(UBIntId id);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_INTERRUPT_CTRL_H_ 
