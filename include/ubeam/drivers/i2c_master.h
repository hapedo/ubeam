/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   i2c_master.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_I2C_MASTER_H_
#define _UBEAM_DRIVERS_I2C_MASTER_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/i2c_master_types.h>

/*! \defgroup ubeam_drivers_spimaster SPI master
 *  \ingroup ubeam_drivers
 *  \brief SPI master driver
 */

/*! \addtogroup ubeam_drivers_spimaster
 *  @{
 */

//! I2C master driver struct
typedef struct
{
	UBPeripheral peri;							//!< I2C peripheral struct
	UBGpioPin* sda;								//!< SDA pin struct instance ptr
	UBGpioPin* scl;								//!< SCL pin struct instance ptr
	void* platform;								//!< Platform dependent data
} UBI2cMasterBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
#include <ubeam/stm32f0/drivers/i2c_master.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/i2c_master.h>
#else
typedef struct
{
    UBI2cMasterBase base;
} UBI2cMaster;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure I2C master driver. Also performs GPIO pin configuration and power enable.
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \param handle I2C peripheral handle
 *  \param sda SDA pin struct ptr (may be NULL)
 *  \param scl SCL pin struct ptr (may be NULL)
 */
void UBI2cMaster_Configure(UBI2cMaster* master, UBPeriHandle handle, UBGpioPin* sda, UBGpioPin* scl);

/*! Enable I2C interrupts
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 */
void UBI2cMaster_EnableInt(UBI2cMaster* master);

/*! Disable I2C interrupts
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 */
void UBI2cMaster_DisableInt(UBI2cMaster* master);

/*! Get driver current state
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \return Current state (UBI2cMasterState enum value)
 */
UBI2cMasterState UBI2cMaster_GetState(UBI2cMaster* master);

/*! Initialize configured driver
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \return UBI2MR_OK on success
 */
UBI2cMasterResult UBI2cMaster_Init(UBI2cMaster* master);

/*! Initialize configured driver with specific SCL frequency
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \param sclFreq SCL frequency to be set
 *  \return UBI2MR_OK on success
 */
UBI2cMasterResult UBI2cMaster_InitWithFreq(UBI2cMaster* master, UBFrequency sclFreq);

/*! Set SCL frequency. Driver must be configured.
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \param sclFreq SCL frequency to be set
 *  \return UBI2MR_OK on success
 */
UBI2cMasterResult UBI2cMaster_SetSclFrequency(UBI2cMaster* master, UBFrequency sclFreq);

/*! Get current SCL frequency
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \return Current SCL frequency in Hz
 */
UBFrequency UBI2cMaster_GetSclFrequency(UBI2cMaster* master);

/*! Perform synchronous (blocking) write transaction
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \param slaveAddress Slave I2C address (without R/W bit - shifted 1 bit right)
 *  \param data I/O data buffer ptr
 *  \param size Data size in bytes
 *  \return UBI2MR_OK on success
 */
UBI2cMasterResult UBI2cMaster_WriteSync(UBI2cMaster* master, uint8_t slaveAddress, uint8_t* data, uint8_t size);

/*! Perform synchronous (blocking) read transaction
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 *  \param slaveAddress Slave I2C address (without R/W bit - shifted 1 bit right)
 *  \param data I/O data buffer ptr
 *  \param size Data size in bytes
 *  \return UBI2MR_OK on success
 */
UBI2cMasterResult UBI2cMaster_ReadSync(UBI2cMaster* master, uint8_t slaveAddress, uint8_t* data, uint8_t size);

/*! Terminate pending transaction
 *  \param master UBI2cMasterBase instance ptr (must not be NULL)
 */
void UBI2cMaster_Terminate(UBI2cMaster* master);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_I2C_MASTER_H_
