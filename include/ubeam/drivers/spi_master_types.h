/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   spi_master_types.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_SPI_MASTER_TYPES_H_
#define _UBEAM_DRIVERS_SPI_MASTER_TYPES_H_

/*! \addtogroup ubeam_drivers_spimaster
 *  @{
 */

//! SPI master driver result
typedef enum
{
	UBSMR_OK = 0,								//!< Result OK (no error)
	UBSMR_NOT_CONFIGURED,						//!< Driver not configured
	UBSMR_NOT_INITIALIZED,						//!< Driver configured but not initialized
	UBSMR_BUSY,									//!< Driver is busy
	UBSMR_LOCKED,								//!< Driver is locked by another thread
	UBSMR_NOT_SUPPORTED,						//!< Function not supported
	UBSMR_ERROR									//!< Generic error
} UBSpiMasterResult;

//! SPI master driver state
typedef enum
{
	UBSMS_NOT_CONFIGURED = 0,					//!< Driver not configured
	UBSMS_NOT_INITIALIZED,						//!< Driver configured but not initialized
	UBSMS_IDLE,									//!< Driver is idle
	UBSMS_BUSY,									//!< Driver is busy
} UBSpiMasterState;

//! SPI master CPOL settings
typedef enum
{
	UBSMCPOL_0 = 0,								//!< CPOL = 0
	UBSMCPOL_1									//!< CPOL = 1
} UBSpiMasterCpol;

//! SPI master CPHA settings
typedef enum
{
	UBSMCPHA_0 = 0,								//!< CPHA = 0
	UBSMCPHA_1									//!< CPHA = 1
} UBSpiMasterCpha;

/*! @} */

#endif // _UBEAM_DRIVERS_SPI_MASTER_TYPES_H_
