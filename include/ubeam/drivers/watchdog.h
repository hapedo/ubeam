/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   watchdog.h
 * \brief  microBeam SDK
 * \date   10.08.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_WATCHDOG_H_
#define _UBEAM_DRIVERS_WATCHDOG_H_

#include <stdlib.h>
#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/watchdog_types.h>

/*! \defgroup ubeam_drivers_watchdog On-chip watchdog driver
 *  \ingroup ubeam_drivers
 *  \brief On-chip watchdog driver
 */

/*! \addtogroup ubeam_drivers_watchdog
 *  @{
 */

//! Watchdog driver struct
typedef struct
{
    UBPeripheral peri;							//!< Peripheral instance
    void* platform;								//!< Platform dependent data
} UBWdgBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
//#include <ubeam/stm32f0/drivers/watchdog.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/watchdog.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_X86)
#include <ubeam/x86/drivers/watchdog.h>
#else
typedef struct
{
    UBWdgBase base;
} UBWdg;
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*! Configure watchdog driver.
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \param handle Watchdog peripheral handle
 */
void UBWdg_Configure(UBWdg* wdg, UBPeriHandle handle);

/*! Initialize driver
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \param timeoutMilli Watchdog timeout in milliseconds
 *  \return UBWDGR_OK on success
 */
UBWdgResult UBWdg_Init(UBWdg* wdg, uint32_t timeoutMilli);

/*! Get watchdog timeout
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \param timeoutMilli Timeout in milliseconds result ptr (must not be NULL)
 *  \return UBWDGR_OK on success
 */
UBWdgResult UBWdg_GetTimeoutMilli(UBWdg* wdg, uint32_t* timeoutMilli);

/*! Enable watchdog
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \return UBWDGR_OK on success
 */
UBWdgResult UBWdg_Enable(UBWdg* wdg);

/*! Disable watchdog
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \return UBWDGR_OK on success
 */
UBWdgResult UBWdg_Disable(UBWdg* wdg);

/*! Check if watchdog is enabled
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \param enabled Enabled result ptr (must not be NULL)
 *  \return UBWDGR_OK on success
 */
UBWdgResult UBWdg_IsEnabled(UBWdg* wdg, bool* enabled);

/*! Feed watchdog
 *  \param wdg Watchdog driver struct instance ptr (must not be NULL)
 *  \return UBWDGR_OK on success
 */
UBWdgResult UBWdg_Feed(UBWdg* wdg);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_WATCHDOG_H_
