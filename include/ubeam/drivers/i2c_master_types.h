/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   i2c_master_types.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_I2C_MASTER_TYPES_H_
#define _UBEAM_DRIVERS_I2C_MASTER_TYPES_H_

/*! \addtogroup ubeam_drivers_spimaster
 *  @{
 */

//! I2C master result
typedef enum
{
	UBI2MR_OK = 0,								//!< Result OK (no error)
	UBI2MR_NOT_CONFIGURED,						//!< Driver not configured
	UBI2MR_NOT_INITIALIZED,						//!< Driver configured but not initialized
    UBI2MR_BAD_PARAM,							//!< Bad parameter given
	UBI2MR_BUSY,								//!< Driver is busy
	UBI2MR_NACK,								//!< Not acknowledged
	UBI2MR_LOCKED,								//!< Driver is locked by another thread
	UBI2MR_NOT_SUPPORTED,						//!< Functionality not supported
	UBI2MR_ERROR								//!< Generic error
} UBI2cMasterResult;

//! I2C master driver state
typedef enum
{
	UBI2MS_NOT_CONFIGURED = 0,					//!< Driver not configured
	UBI2MS_NOT_INITIALIZED,						//!< Driver configured but not initialized
	UBI2MS_IDLE,								//!< Driver idle
	UBI2MS_BUSY,								//!< Driver is busy
} UBI2cMasterState;

/*! @} */

#endif // _UBEAM_DRIVERS_I2C_MASTER_TYPES_H_
