/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   gpio_types.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_GPIO_DEFINITIONS_H_
#define _UBEAM_DRIVERS_GPIO_DEFINITIONS_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*! \addtogroup ubeam_drivers_gpio
 *  @{
 */

//! Gpio pin 0 mask
#define UBGpioPin0                            (0x00000001)
//! Gpio pin 1 mask
#define UBGpioPin1                            (0x00000002)
//! Gpio pin 2 mask
#define UBGpioPin2                            (0x00000004)
//! Gpio pin 3 mask
#define UBGpioPin3                            (0x00000008)
//! Gpio pin 4 mask
#define UBGpioPin4                            (0x00000010)
//! Gpio pin 5 mask
#define UBGpioPin5                            (0x00000020)
//! Gpio pin 6 mask
#define UBGpioPin6                            (0x00000040)
//! Gpio pin 7 mask
#define UBGpioPin7                            (0x00000080)
//! Gpio pin 8 mask
#define UBGpioPin8                            (0x00000100)
//! Gpio pin 9 mask
#define UBGpioPin9                            (0x00000200)
//! Gpio pin 10 mask
#define UBGpioPin10                            (0x00000400)
//! Gpio pin 11 mask
#define UBGpioPin11                            (0x00000800)
//! Gpio pin 12 mask
#define UBGpioPin12                            (0x00001000)
//! Gpio pin 13 mask
#define UBGpioPin13                            (0x00002000)
//! Gpio pin 14 mask
#define UBGpioPin14                            (0x00004000)
//! Gpio pin 15 mask
#define UBGpioPin15                            (0x00008000)
//! Gpio pin 16 mask
#define UBGpioPin16                            (0x00010000)
//! Gpio pin 17 mask
#define UBGpioPin17                            (0x00020000)
//! Gpio pin 18 mask
#define UBGpioPin18                            (0x00040000)
//! Gpio pin 19 mask
#define UBGpioPin19                            (0x00080000)
//! Gpio pin 20 mask
#define UBGpioPin20                            (0x00100000)
//! Gpio pin 21 mask
#define UBGpioPin21                            (0x00200000)
//! Gpio pin 22 mask
#define UBGpioPin22                            (0x00400000)
//! Gpio pin 23 mask
#define UBGpioPin23                            (0x00800000)
//! Gpio pin 24 mask
#define UBGpioPin24                            (0x01000000)
//! Gpio pin 25 mask
#define UBGpioPin25                            (0x02000000)
//! Gpio pin 26 mask
#define UBGpioPin26                            (0x04000000)
//! Gpio pin 27 mask
#define UBGpioPin27                            (0x08000000)
//! Gpio pin 28 mask
#define UBGpioPin28                            (0x10000000)
//! Gpio pin 29 mask
#define UBGpioPin29                            (0x20000000)
//! Gpio pin 30 mask
#define UBGpioPin30                            (0x40000000)
//! Gpio pin 31 mask
#define UBGpioPin31                            (0x80000000)

//! GPIO function
typedef enum
{
    UBGPF_GPIO = 0,
    UBGPF_UNDEFINED,
    UBGPF_SYSTEM,
    UBGPF_CLOCK,
    UBGPF_DEBUG,
	UBGPF_ADC_1,
	UBGPF_ADC_2,
	UBGPF_ADC_3,
	UBGPF_DAC_1,
	UBGPF_DAC_2,
    UBGPF_USART_1,
    UBGPF_USART_2,
    UBGPF_USART_3,
    UBGPF_TIMER_1,
    UBGPF_TIMER_2,
    UBGPF_TIMER_3,
    UBGPF_TIMER_4,
    UBGPF_TIMER_5,
    UBGPF_TIMER_6,
    UBGPF_TIMER_7,
    UBGPF_TIMER_8,
    UBGPF_TIMER_9,
    UBGPF_TIMER_10,
    UBGPF_TIMER_11,
    UBGPF_TIMER_12,
    UBGPF_TIMER_13,
    UBGPF_TIMER_14,
    UBGPF_TIMER_15,
    UBGPF_TIMER_16,
    UBGPF_TIMER_17,
    UBGPF_I2C_1,
    UBGPF_I2C_2,
    UBGPF_SPI_1,
    UBGPF_SPI_2,
    UBGPF_SPI_3,
    UBGPF_IR,
    UBGPF_LCD,
    UBGPF_EVENT,
} UBGpioPinFunc;

//! GPIO pin speed settings
typedef enum
{
    UBGPS_DEFAULT = 0,												//!< Default
    UBGPS_LOW = UBGPS_DEFAULT,										//!< Low speed
    UBGPS_MEDIUM,													//!< Medium speed
    UBGPS_HIGH,														//!< High speed
} UBGpioPinSpeed;

//! GPIO pull resistor settings
typedef enum
{
    UBGPP_DEFAULT = 0,												//!< Default
    UBGPP_NONE = UBGPP_DEFAULT,										//!< No pull resistor
    UBGPP_UP,														//!< Pull-up resistor
    UBGPP_DOWN														//!< Pull-down resistor
} UBGpioPinPull;

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_GPIO_DEFINITIONS_H_ 
