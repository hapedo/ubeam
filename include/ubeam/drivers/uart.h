/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   uart.h
 * \brief  microBeam SDK
 * \date   7.11.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_UART_H_
#define _UBEAM_DRIVERS_UART_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/uart_types.h>
#include <ubeam/utils/fifo.h>

/*! \defgroup ubeam_drivers_uart UART
 *  \ingroup ubeam_drivers
 *  \brief UART driver
 */

/*! \addtogroup ubeam_drivers_uart
 *  @{
 */

//! UART driver struct
typedef struct
{
	UBPeripheral peri;							//!< Peripheral instance
	UBGpioPin* rx;								//!< RX pin instance ptr
	UBGpioPin* tx;								//!< TX pin instance ptr
	UBFifo* rxFifo;								//!< RX FIFO
	UBFifo* txFifo;								//!< TX FIFO
    UBFifo* rxMonFifo;							//!< RX monitoring FIFO
    UBFifo* txMonFifo;							//!< TX monitoring FIFO
    void* platform;								//!< Platform dependent data
} UBUartBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
#include <ubeam/stm32f0/drivers/uart.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/uart.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_X86)
#include <ubeam/x86/drivers/uart.h>
#else
typedef struct
{
    UBUartBase base;
} UBUart;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure UART driver. Also performs GPIO pin configuration and power enable.
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *  \param handle SPI peripheral handle
 *  \param rx RX pin instance ptr (may be NULL)
 *  \param tx TX pin instance ptr (may be NULL)
 */
void UBUart_Configure(UBUart* uart, UBPeriHandle handle, UBGpioPin* rx, UBGpioPin* tx, UBFifo* rxFifo, UBFifo* txFifo);

/*! Enable UART interrupts
 *  \param uart UART driver struct instance ptr (must not be NULL)
 */
void UBUart_EnableInt(UBUart* uart);

/*! Disable UART interrupts
 *  \param uart UART driver struct instance ptr (must not be NULL)
 */
void UBUart_DisableInt(UBUart* uart);

/*! Get driver current state
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *  \return Current state (UBUartState enum value)
 */
UBUartState UBUart_GetState(UBUart* uart);

/*! Initialize UART driver
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *	\param baudrate UART baudrate
 *  \return UBUR_OK on success
 */
UBUartResult UBUart_Init(UBUart* uart, UBFrequency baudrate);

/*! Close UART driver
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *  \return UBUR_OK on success
 */
UBUartResult UBUart_Close(UBUart* uart);

/*! Set UART baudrate
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *	\param baudrate UART baudrate
 *  \return UBUR_OK on success
 */
UBUartResult UBUart_SetBaudrate(UBUart* uart, UBFrequency baudrate);

/*! Set RX/TX monitoring FIFO
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *	\param rxMonFifo RX monitoring FIFO (may be NULL)
 *	\param txMonFifo TX monitoring FIFO (may be NULL)
 *  \return UBUR_OK on success
 */
UBUartResult UBUart_SetMonitor(UBUart* uart, UBFifo* rxMonFifo, UBFifo* txMonFifo);

/*! Enqueue data to UART TX FIFO
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *  \param data Data to be enqueued (must not be NULL)
 *  \param size Data size
 *  \param enqueued Data size that has been enqueued (result ptr)
 *  \return UBUR_OK on success
 */
UBUartResult UBUart_Enqueue(UBUart* uart, const uint8_t* data, uint32_t size, uint32_t* enqueued);

/*! Get size of free space in TX FIFO
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *  \return Size of free space in TX FIFO
 */
uint32_t UBUart_TxFifoAvailable(UBUart* uart);

/*! Start transmission
 *  \param uart UART driver struct instance ptr (must not be NULL)
 *  \return UBUR_OK on success
 */
UBUartResult UBUart_Transmit(UBUart* uart);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_UART_H_
