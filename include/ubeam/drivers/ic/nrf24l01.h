/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   nrf24l01.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_IC_NRF24L01_H_
#define _UBEAM_DRIVERS_IC_NRF24L01_H_

#include <stdint.h>
#include <stdbool.h>
#include <ubeam/drivers/spi_master.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/utils/system_time.h>
#include <ubeam/os/mutex.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*! \defgroup ubeam_drivers_ic_nrf24l01 NRF24L01
 *  \ingroup ubeam_drivers_ic
 *  \brief Nordic NRF24L01 2.4GHz transceiver driver
 */

/*! \addtogroup ubeam_drivers_ic_nrf24l01
 *  @{
 */

//! Transmission timeout [ms]
#define NRF24L01_TIMEOUT_TX                             100
//! Default RX poll period [ms]
#define NRF24L01_DEFAULT_RX_POLL_PERIOD                 1
//! Maximal payload size
#define NRF24L01_MAX_PAYLOAD_SIZE                       32
//! Pipe count
#define NRF24L01_PIPE_COUNT                             5
//! Pipe count
#define NRF24L01_ADDRESS_SIZE                           5

//! NRF24L01 pipe address type
typedef uint8_t NrfAddress[NRF24L01_ADDRESS_SIZE];

//! Operation result
typedef enum
{
    NRFR_OK = 0,                      	//!< Result OK
	NRFR_NOT_CONFIGURED,				//!< Driver not configured
    NRFR_NOT_INITIALIZED,             	//!< Driver not initialized
	NRFR_BUSY,
    NRFR_SPI_ERROR,                   	//!< SPI transceive error
    NRFR_BAD_PARAM,                   	//!< Bad parameter
    NRFR_TIMEOUT,                     	//!< Operation timeouted
	NRFR_ERROR
} NrfResult;

//! Driver state
typedef enum
{
    NRFS_NOT_CONFIGURED,				//!< Driver not configured
    NRFS_NOT_INITIALIZED,				//!< Driver configured but not initialized
	NRFS_IDLE,							//!< Driver idle
	NRFS_BUSY							//!< Driver busy
} NrfState;

//! Data rate
typedef enum
{
    NRFRT_250KBPS,                       //!< 250 kBps
    NRFRT_1MBPS,                         //!< 1 MBps
    NRFRT_2MBPS                          //!< 2 MBps
} NrfDataRate;

//! Packet CRC length
typedef enum
{
    NRFCL_NONE,                     	//!< No CRC checking
    NRFCL_1BYTE,                    	//!< 1 byte CRC
    NRFCL_2BYTES                    	//!< 2 byte CRC
} NrfCrcLength;

//! Output power
typedef enum
{
    NRFOP_18DBM,                        //!< -18 dBm
    NRFOP_12DBM,                        //!< -12 dBm
    NRFOP_6DBM,                         //!< -6 dBm
    NRFOP_0DBM                          //!< 0 dBm
} NrfOutputPower;

//! Driver struct
typedef struct
{
    UBMutex mutex;						//!< Driver mutex
    NrfState state;						//!< Driver state
    UBSpiMaster* spi;					//!< Underlying SPI driver struct instance ptr
    UBGpioPin* pinCsn;					//!< CSN pin struct instance ptr
    UBGpioPin* pinCe;					//!< CE pin struct instance ptr
    NrfAddress address;					//!< RX address
    NrfAddress frameDestAddress;		//!< TX address
    uint8_t mode;						//!< Current mode
    bool rxEnabled;						//!< RX is enabled
    bool ackPayload;					//!< ACK payload enabled
    UBTimestamp rxTimeStamp;			//!< RX timestamp
    UBTimeInterval rxPollPeriod;		//!< RX poll period
} Nrf24l01;

/*! Configure driver
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param spi Underlying SPI master driver struct instance ptr (must not be NULL)
 *  \param pinCsn CSN pin struct instance ptr (must not be NULL)
 *  \param pinCe CE pin struct instance ptr (must not be NULL)
 */
void Nrf_Configure(Nrf24l01* nrf, UBSpiMaster* spi, UBGpioPin* pinCsn, UBGpioPin* pinCe);

/*! Initialize chip
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param address RX address
 *  \param autoAck Auto ACK enabled when true
 *  \param dynamicPayload Dynamic payload enabled when true
 *  \param dataRate Data rate to be used
 *  \param ackPayload ACK payload enabled when true
 *  \return NRFR_OK on success
 */
NrfResult Nrf_Init(Nrf24l01* nrf, NrfAddress address, bool autoAck, bool dynamicPayload, NrfDataRate dataRate, bool ackPayload);

/*! Enable chip power
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param enable Enable power when true, disable power otherwise
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnablePower(Nrf24l01* nrf, bool enable);

/*! Enable data reception
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param enable Enable reception when true, disable otherwise
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnableReception(Nrf24l01* nrf, bool enable);

/*! Flush reception FIFO
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \return NRFR_OK on success
 */
NrfResult Nrf_FlushReceptionFifo(Nrf24l01* nrf);

/*! Set pipe address
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param address Address to be set
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetPipeAddress(Nrf24l01* nrf, uint8_t pipe, NrfAddress address);

/*! Enable pipe for data reception
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param enable Enable reception when true, disable otherwise
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnablePipeReception(Nrf24l01* nrf, uint8_t pipe, bool enable);

/*! Set pipe payload size
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param size Payload size in bytes (must be less or equal to NRF24L01_MAX_PAYLOAD_SIZE)
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetPipePayloadSize(Nrf24l01* nrf, uint8_t pipe, uint8_t size);

/*! Set all pipes payload size
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param size Payload size in bytes (must be less or equal to NRF24L01_MAX_PAYLOAD_SIZE)
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetPipePayloadSizeAll(Nrf24l01* nrf, uint8_t size);

/*! Enable dynamic payload for specific pipe
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param enable Enable when true, disable when false
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnableDynamicPayload(Nrf24l01* nrf, uint8_t pipe, bool enable);

/*! Enable dynamic payload for all pipes
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param enable Enable when true, disable when false
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnableDynamicPayloadAll(Nrf24l01* nrf, bool enable);

/*! Enable ACK payload
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param enable Enable when true, disable when false
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnableAckPayload(Nrf24l01* nrf, bool enable);

/*! Write ACK payload
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param buffer Data buffer (input)
 *  \param size Number of bytes to write
 *  \return NRFR_OK on success
 */
NrfResult Nrf_WriteAckPayload(Nrf24l01* nrf, uint8_t pipe, const uint8_t* buffer, uint8_t size);

/*! Enable auto ACK for specific pipe
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param enable Enable when true, disable when false
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnableAutoAck(Nrf24l01* nrf, uint8_t pipe, bool enable);

/*! Enable auto ACK for all pipes
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number
 *  \param enable Enable when true, disable when false
 *  \return NRFR_OK on success
 */
NrfResult Nrf_EnableAutoAckAll(Nrf24l01* nrf, bool enable);

/*! Set transmission power
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param power Power to be set
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetOutputPower(Nrf24l01* nrf, NrfOutputPower power);

/*! Get current transmission power
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \return Current power
 */
NrfOutputPower Nrf_GetOutputPower(Nrf24l01* nrf);

/*! Set data rate
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param dataRate Data rate to be set
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetDataRate(Nrf24l01* nrf, NrfDataRate dataRate);

/*! Get current data rate
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \return Current data rate
 */
NrfDataRate Nrf_GetDataRate(Nrf24l01* nrf);

/*! Set CRC length
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param crcLength CRC length to be used
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetCrcLength(Nrf24l01* nrf, NrfCrcLength crcLength);

/*! Get current CRC length
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \return Current CRC length
 */
NrfCrcLength Nrf_GetCrcLength(Nrf24l01* nrf);

/*! Set maximal transmission retry count and delay
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param retryDelay Retry delay (0 to 0x0f)
 *  \param retryCount Maximal number of retries (0 to 0x0f)
 *  \return NRFR_OK on success
 */
NrfResult Nrf_SetRetrySettings(Nrf24l01* nrf, uint8_t retryDelay, uint8_t retryCount);

/*! Get current maximal transmission retry count and delay
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param retryDelay Retry delay (output)
 *  \param retryCount Maximal number of retries (output)
 *  \return NRFR_OK on success
 */
NrfResult Nrf_GetRetrySettings(Nrf24l01* nrf, uint8_t* retryDelay, uint8_t* retryCount);

/*! Transmit packet
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param address Destination address
 *  \param payload Payload to be transmited
 *  \param payloadSize Payload size in bytes
 *  \param ackPayloadBuffer Received ACK payload (may be NULL)
 *  \param ackPayloadSize Received ACK payload size (output, may be NULL)
 *  \return NRFR_OK on success
 */
NrfResult Nrf_Transmit(Nrf24l01* nrf, NrfAddress address, uint8_t* payload, uint8_t payloadSize, uint8_t* ackPayloadBuffer, uint8_t* ackPayloadSize);

/*! Receive packet
 *  \param nrf Nrf24l01 struct instance ptr (must not be NULL)
 *  \param pipe Pipe number that received packed (output, must not be NULL)
 *  \param payloadBuffer Payload received (output, must not be NULL)
 *  \param payloadSize Received payload size in bytes (output, must not be NULL)
 *  \param timeout Maximal reception timeout
 *  \return NRFR_OK on success
 */
NrfResult Nrf_Receive(Nrf24l01* nrf, uint8_t* pipe, uint8_t* payloadBuffer, uint8_t* payloadSize, UBTimeInterval timeout);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_IC_NRF24L01_H_ 
