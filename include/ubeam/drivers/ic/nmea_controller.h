/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   nmea_controller.h
 * \brief  microBeam SDK
 * \date   08.01.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_IC_NMEA_CONTROLLER_H_
#define _UBEAM_DRIVERS_IC_NMEA_CONTROLLER_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/uart.h>
#include <ubeam/utils/system_time.h>
#include <ubeam/os/mutex.h>
#include <ubeam/os/thread.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*! \defgroup ubeam_drivers_ic_nmeactrl NMEA Controller
 *  \ingroup ubeam_drivers_ic
 *  \brief NMEA protocol controller
 */

/*! \addtogroup ubeam_drivers_ic_nmeactrl
 *  @{
 */

//! Max NMEA command string length
#define NMEACTRL_MAX_COMMAND_LENGTH		8
//! Max NMEA command result string length
#define NMEACTRL_MAX_RESULT_LENGTH		128
//! Max NMEA command argument count
#define NMEACTRL_MAX_ARG_COUNT			16

//! NMEA controller result
typedef enum
{
	NMEACR_OK = 0,						//!< Result OK (no error)
	NMEACR_NOT_CONFIGURED,				//!< NMEA controller not configured
	NMEACR_NOT_INITIALIZED,				//!< NMEA controller not initialized
	NMEACR_BUSY,						//!< NMEA controller is busy (already processing command)
	NMEACR_TIMEOUT,						//!< Command has timeouted
	NMEACR_BAD_PARRAM,					//!< Bad parameter given
	NMEACR_UART_ERROR,					//!< Underlying UART error
	NMEACR_ERROR						//!< Generic (or unknown) error
} NmeaCtrlResult;

//! NMEA controller state
typedef enum
{
	NMEACS_NOT_CONFIGURED = 0,			//!< Not configured
	NMEACS_NOT_INITIALIZED,				//!< Configured bud not initialized
	NMEACS_IDLE,						//!< Idle
	NMEACS_COMMAND,						//!< Processing command
} NmeaCtrlState;

//! NMEA controller command response callback
typedef struct
{
	char command[NMEACTRL_MAX_COMMAND_LENGTH + 1];		//!< Command string
	void* callback;						//!< Callback function ptr
} NmeaCtrlCbDesc;

//! AT controller struct
typedef struct
{
	UBUart* uart;						//!< Underlying UART (must not be NULL)
	UBMutex mutex;						//!< Controller mutex
	UBThread thread;					//!< Controller thread
	void* customData;					//!< Custom (user) data
	NmeaCtrlState state;				//!< Current state
	NmeaCtrlCbDesc** cbDescs;			//!< Callback descriptors
	uint8_t cbDescCount;				//!< Number of callback descriptors
	char nmeaBuffer[NMEACTRL_MAX_RESULT_LENGTH + 1];	//!< NMEA buffer
	char nmeaResult[NMEACTRL_MAX_RESULT_LENGTH + 1];	//!< NMEA command result string
	uint16_t nmeaBufferCount;			//!< Number of data in atBuffer
} NmeaCtrl;

//! NMEA controller callback
typedef void(NmeaCtrlCallback)(NmeaCtrl* ctrl, const NmeaCtrlCbDesc* desc, const char** args, uint8_t argCount);

/*! Configure NMEA controller
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \param uart Underlying UART instance (must not be NULL)
 */
void NmeaCtrl_Configure(NmeaCtrl* ctrl, UBUart* uart);

/*! Set NMEA controller custom (user) data
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \param customData Custom data to be set (may be NULL)
 */
void NmeaCtrl_SetCustomData(NmeaCtrl* ctrl, void* customData);

/*! Get NMEA controller custom (user) data
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \return Custom (user) data
 */
void* NmeaCtrl_GetCustomData(const NmeaCtrl* ctrl);

/*! Get current state
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \return Current state enum value
 */
NmeaCtrlState NmeaCtrl_GetState(const NmeaCtrl* ctrl);

/*! Initialize NMEA controller
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \param baudrate Modem baudrate (will be set on underlying UART)
 *  \return NMEACR_OK on success
 */
NmeaCtrlResult NmeaCtrl_Init(NmeaCtrl* ctrl, UBFrequency baudrate);

/*! Hook command callback
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \param command NMEA command string
 *  \param callback Callback that will be called when NMEA command result is received by controller
 *  \return NMEACR_OK on success
 */
NmeaCtrlResult NmeaCtrl_HookCallback(NmeaCtrl* ctrl, const char* command, NmeaCtrlCallback* callback);

/*! Send NMEA command
 *  \param ctrl NMEA controller struct instance ptr (must not be NULL)
 *  \param command NMEA command string to be send (must not be NULL)
 *  \return ATCR_OK on success
 */
NmeaCtrlResult NmeaCtrl_SendCommand(NmeaCtrl* ctrl, const char* command);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_IC_NMEA_CONTROLLER_H_

