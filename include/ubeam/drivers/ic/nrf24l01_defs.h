/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   nrf24l01_defs.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_IC_NRF24L01_DEFS_H_
#define _UBEAM_DRIVERS_IC_NRF24L01_DEFS_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*! \addtogroup ubeam_drivers_ic_nrf24l01
 *  @{
 */

// NRF24L01 registers
#define NRF_REG_CONFIG          0x00
#define NRF_REG_EN_AA           0x01
#define NRF_REG_EN_RXADDR       0x02
#define NRF_REG_SETUP_AW        0x03
#define NRF_REG_SETUP_RETR      0x04
#define NRF_REG_RF_CH           0x05
#define NRF_REG_RF_SETUP        0x06
#define NRF_REG_STATUS          0x07
#define NRF_REG_OBSERVE_TX      0x08
#define NRF_REG_RPD             0x09
#define NRF_REG_RX_ADDR_P0      0x0a
#define NRF_REG_RX_ADDR_P1      0x0b
#define NRF_REG_RX_ADDR_P2      0x0c
#define NRF_REG_RX_ADDR_P3      0x0d
#define NRF_REG_RX_ADDR_P4      0x0e
#define NRF_REG_RX_ADDR_P5      0x0f
#define NRF_REG_TX_ADDR         0x10
#define NRF_REG_RX_PW_P0        0x11
#define NRF_REG_RX_PW_P1        0x12
#define NRF_REG_RX_PW_P2        0x13
#define NRF_REG_RX_PW_P3        0x14
#define NRF_REG_RX_PW_P4        0x15
#define NRF_REG_RX_PW_P5        0x16
#define NRF_REG_FIFO_STATUS     0x17
#define NRF_REG_DYNPD           0x1c
#define NRF_REG_FEATURE         0x1d

// NRF24L01 bits
#define NRF_BIT_MASK_RX_DR  0x40
#define NRF_BIT_MASK_TX_DS  0x20
#define NRF_BIT_MASK_MAX_RT 0x10
#define NRF_BIT_EN_CRC      0x08
#define NRF_BIT_CRCO        0x04
#define NRF_BIT_PWR_UP      0x02
#define NRF_BIT_PRIM_RX     0x01
#define NRF_BIT_ENAA_P5     0x20
#define NRF_BIT_ENAA_P4     0x10
#define NRF_BIT_ENAA_P3     0x08
#define NRF_BIT_ENAA_P2     0x04
#define NRF_BIT_ENAA_P1     0x02
#define NRF_BIT_ENAA_P0     0x01
#define NRF_BIT_ERX_P5      0x20
#define NRF_BIT_ERX_P4      0x10
#define NRF_BIT_ERX_P3      0x08
#define NRF_BIT_ERX_P2      0x04
#define NRF_BIT_ERX_P1      0x02
#define NRF_BIT_ERX_P0      0x01
#define NRF_BIT_AW          0x01
#define NRF_BIT_ARD         0x10
#define NRF_BIT_ARC         0x01
#define NRF_BIT_PLL_LOCK    0x10
#define NRF_BIT_RF_DR_LOW   0x20
#define NRF_BIT_RF_DR_HIGH  0x08
#define NRF_BIT_RF_PWR      0x40
#define NRF_BIT_RX_DR       0x40
#define NRF_BIT_TX_DS       0x20
#define NRF_BIT_MAX_RT      0x10
#define NRF_BIT_RX_P_NO     0x02
#define NRF_BIT_TX_FULL     0x01
#define NRF_BIT_PLOS_CNT    0x10
#define NRF_BIT_ARC_CNT     0x01
#define NRF_BIT_TX_REUSE    0x40
#define NRF_BIT_FIFO_FULL   0x20
#define NRF_BIT_TX_EMPTY    0x10
#define NRF_BIT_RX_FULL     0x02
#define NRF_BIT_RX_EMPTY    0x01
#define NRF_BIT_DPL_P5      0x20
#define NRF_BIT_DPL_P4      0x10
#define NRF_BIT_DPL_P3      0x08
#define NRF_BIT_DPL_P2      0x04
#define NRF_BIT_DPL_P1      0x02
#define NRF_BIT_DPL_P0      0x01
#define NRF_BIT_EN_DPL      0x04
#define NRF_BIT_EN_ACK_PAY  0x02
#define NRF_BIT_EN_DYN_ACK  0x01

// NRF24L01 instructions
#define NRF_INSTR_RREGISTER     0x00
#define NRF_INSTR_WREGISTER     0x20
#define NRF_INSTR_ACTIVATE      0x50
#define NRF_INSTR_RRXPAYWIDTH   0x60
#define NRF_INSTR_RRXPAY        0x61
#define NRF_INSTR_WTXPAY        0xa0
#define NRF_INSTR_WACKPAY       0xa8
#define NRF_INSTR_FLUSHTX       0xe1
#define NRF_INSTR_FLUSHRX       0xe2
#define NRF_INSTR_REUSETXPAY    0xe3
#define NRF_INSTR_NOP           0xff

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_IC_NRF24L01_DEFS_H_
