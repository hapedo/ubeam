/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   quectel_bc95.h
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_IC_QUECTEL_BC95_H_
#define _UBEAM_DRIVERS_IC_QUECTEL_BC95_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/uart.h>
#include <ubeam/os/mutex.h>
#include <ubeam/drivers/ic/at_controller.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*! \defgroup ubeam_drivers_ic_quectelbc95 Quectel BC95 driver
 *  \ingroup ubeam_drivers_ic
 *  \brief Quectel BC95 NB IoT modem driver
 */

/*! \addtogroup ubeam_drivers_ic_quectelbc95
 *  @{
 */

//! Operation result
typedef enum
{
    BC95R_OK = 0,                      	//!< Result OK
	BC95R_NOT_CONFIGURED,				//!< Driver not configured
	BC95R_NOT_INITIALIZED,             	//!< Driver not initialized
	BC95R_BUSY,
	BC95R_UART_ERROR,                  	//!< UART transceive error
	BC95R_BAD_PARAM,                   	//!< Bad parameter
	BC95R_TIMEOUT,                     	//!< Operation timeouted
	BC95R_NO_SERVICE,					//!< Not connected to network
	BC95R_SOCKET_ERROR,					//!< Socket related error
	BC95R_AT_ERROR,						//!< AT command controller error
	BC95R_ERROR							//!< Generic error
} BC95Result;

//! Driver state
typedef enum
{
	BC95S_NOT_CONFIGURED,				//!< Driver not configured
	BC95S_NOT_INITIALIZED,				//!< Driver configured but not initialized
	BC95S_INITIALISING,					//!< Driver is initializing modem
	BC95S_WAITING_FOR_SERVICE,			//!< Waiting for service availibility
	BC95S_SERVICE_RECOVER,				//!< Recovering lost service
	BC95S_IDLE,							//!< Driver idle, but not connected to network
	BC95S_BUSY							//!< Driver busy
} BC95State;

//! IPv4 address
typedef uint8_t QuectelIpAddress[4];

//! Quectel BC95 driver struct
typedef struct
{
	BC95State state;					//!< Current state
	uint16_t stateStep;					//!< State step counter
	UBTimestamp stateTimeout;			//!< Inner timeout timestamp
	UBTimestamp stateTimer;				//!< Inner step timer timestamp
	UBMutex mutex;						//!< Driver mutex
	UBThread thread;					//!< Driver thread
	AtCtrl atCtrl;						//!< AT command controller instance
	UBUart* uart;						//!< Underlying UART instance ptr
	UBGpioPin* resetPin;				//!< Reset GPIO pin instance
	bool resetActiveHigh;				//!< Reset pin polarity
	char iccid[24];						//!< Modem ICCID
} QuectelBC95;

//! Quectel BC95 UDP socket struct
typedef struct
{
	QuectelBC95* bc;					//!< Modem driver struct ptr
	uint8_t id;							//!< Socket ID
	bool opened;						//!< Socket opened flag
} QuectelBC95Socket;

/*! Configure Quectel BC95 driver
 *  \param bc Quectel BC95 driver struct ptr (must not be NULL)
 *  \param uart Underlying UART driver struct ptr (must not be NULL)
 *  \param txModemMonitor Optional monitor FIFO (may be NULL)
 *  \param resetPin Reset GPIO pin (may be NULL)
 *  \param resetActiveHigh Reset pin is active H when TRUE
 */
void BC95_Configure(QuectelBC95* bc, UBUart* uart, UBFifo* txModemMonitor, UBGpioPin* resetPin, bool resetActiveHigh);

/*! Initialize driver
 *  \param bc Quectel BC95 driver struct ptr (must not be NULL)
 *  \param baudrate Baudrate to be set
 *  \return BC95R_OK on success
 */
BC95Result BC95_Init(QuectelBC95* bc, UBFrequency baudrate);

/*! Check whether service is available
 *  \param bc Quectel BC95 driver struct ptr (must not be NULL)
 *  \return TRUE when service is available
 */
bool BC95_HasService(QuectelBC95* bc);

/*! Configure UDP socket
 *  \param socket UDP socket struct ptr (must not be NULL)
 *  \param bc Quectel BC95 driver struct ptr (must not be NULL)
 */
void BC95_UdpSocketConfigure(QuectelBC95Socket* socket, QuectelBC95* bc);

/*! Open UDP socket
 *  \param socket UDP socket struct ptr (must not be NULL)
 *  \param port UDP port
 *  \return BC95R_OK on success
 */
BC95Result BC95_OpenUdpSocket(QuectelBC95Socket* socket, uint16_t port);

/*! Close UDP socket
 *  \param socket UDP socket struct ptr (must not be NULL)
 *  \return BC95R_OK on success
 */
BC95Result BC95_CloseUdpSocket(QuectelBC95Socket* socket);

/*! Check whether UDP socket is opened
 *  \param socket UDP socket struct ptr (must not be NULL)
 *  \return TRUE when socket is opened
 */
bool BC95_IsUdpSocketOpened(QuectelBC95Socket* socket);

/*! Send data over UDP socket
 *  \param socket UDP socket struct ptr (must not be NULL)
 *  \param destination Destination IP address
 *  \param port Destination UDP port
 *  \param data Data to be sent (must not be NULL)
 *  \param size Data size
 *  \return BC95R_OK on success
 */
BC95Result BC95_SendUdp(QuectelBC95Socket* socket, QuectelIpAddress destination, uint16_t port, const uint8_t* data, uint16_t size);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_IC_QUECTEL_BC95_H_
