/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   at_controller.h
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_IC_AT_CONTROLLER_H_
#define _UBEAM_DRIVERS_IC_AT_CONTROLLER_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/uart.h>
#include <ubeam/utils/system_time.h>
#include <ubeam/os/mutex.h>
#include <ubeam/os/thread.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*! \defgroup ubeam_drivers_ic_atctrl AT Controller
 *  \ingroup ubeam_drivers_ic
 *  \brief AT commands controller
 */

/*! \addtogroup ubeam_drivers_ic_atctrl
 *  @{
 */

//! Max AT command string length
#define ATCTRL_MAX_COMMAND_LENGTH		16
//! Max AT command result string length
#define ATCTRL_MAX_RESULT_LENGTH		128
//! Max AT response timeout in millis
#define ATCTRL_RESP_TIMEOUT_MILLI		100

//! AT controller result
typedef enum
{
	ATCR_OK = 0,						//!< Result OK (no error)
	ATCR_NOT_CONFIGURED,				//!< AT controller not configured
	ATCR_NOT_INITIALIZED,				//!< AT controller not initialized
	ATCR_BUSY,							//!< AT controller is busy (already processing command)
	ATCR_TIMEOUT,						//!< Command has timeouted
	ATCR_BAD_PARRAM,					//!< Bad parameter given
	ATCR_UART_ERROR,					//!< Underlying UART error
    ATCR_ERROR,							//!< Generic (or unknown) error
    ATCR_ASYNC_DATA
} AtCtrlResult;

//! AT controller state
typedef enum
{
	ATCS_NOT_CONFIGURED = 0,			//!< Not configured
	ATCS_NOT_INITIALIZED,				//!< Configured bud not initialized
	ATCS_WAIT_FOR_TURN_ON,				//!< Waiting for modem turn on
	ATCS_IDLE,							//!< Idle
	ATCS_IDLE_LINE,						//!< Idle but processing unsolicited command result line
	ATCS_COMMAND,						//!< Processing command
    ATCS_COMMAND_LINE,  				//!< Processing command response line
    ATCS_RAW_READ                       //!< Raw data read
} AtCtrlState;

//! AT controller command response callback
typedef struct
{
	char command[ATCTRL_MAX_COMMAND_LENGTH + 1];		//!< Command string
	void* callback;						//!< Callback function ptr
} AtCtrlCbDesc;

//! AT controller struct
typedef struct
{
	UBUart* uart;						//!< Underlying UART (must not be NULL)
	UBFifo* txMonitor;					//!< Optional monitor FIFO (may be NULL)
	UBMutex mutex;						//!< Controller mutex
	UBThread thread;					//!< Controller thread
	void* customData;					//!< Custom (user) data
	AtCtrlState state;					//!< Current state
	AtCtrlCbDesc** cbDescs;				//!< Callback descriptors
	uint8_t cbDescCount;				//!< Number of callback descriptors
	char atResult[ATCTRL_MAX_RESULT_LENGTH + 1];	//!< AT command result string
	char atBuffer[ATCTRL_MAX_RESULT_LENGTH + 1];	//!< AT buffer
    char atCustomResult[ATCTRL_MAX_RESULT_LENGTH + 1];      //! Custom "OK" reply check string
    bool isAtCustomResultStrict;        //!< Custom result must match when true, may contain some additional text when false
	char* syncResult;					//!< AT command result for synchronous requests (may be NULL)
	uint16_t syncResultLength;			//!< Maximal synchronous result length
	bool wasError;						//!< Was error flag
	uint16_t atBufferCount;				//!< Number of data in atBuffer
	UBTimestamp timeout;				//!< Timeout timestamp
	AtCtrlCbDesc* currentDesc;			//!< Current callback descriptor ptr
    AtCtrlCbDesc asyncDesc;             //!< Callback descriptor for async
	uint8_t line;						//!< AT result line index
    uint8_t* rawData;                   //!< Raw data buffer
    uint16_t rawDataRemaining;          //!< Raw data remaining
    uint8_t rawDataLineSkip;            //!< Number of lines to skip before reading raw data
} AtCtrl;

//! AT controller callback
typedef void(AtCtrlCallback)(AtCtrl* ctrl, const AtCtrlCbDesc* desc, const char* data, uint16_t dataSize, uint8_t lineIndex, AtCtrlResult result);

/*! Configure AT controller
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param uart Underlying UART instance (must not be NULL)
 *  \param txMonitor Optional TX monitor FIFO (may be NULL)
 */
void AtCtrl_Configure(AtCtrl* ctrl, UBUart* uart, UBFifo* txMonitor);

/*! Set AT controller custom (user) data
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param customData Custom data to be set (may be NULL)
 */
void AtCtrl_SetCustomData(AtCtrl* ctrl, void* customData);

/*! Get AT controller custom (user) data
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \return Custom (user) data
 */
void* AtCtrl_GetCustomData(const AtCtrl* ctrl);

/*! Get current state
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \return Current state enum value
 */
AtCtrlState AtCtrl_GetState(const AtCtrl* ctrl);

/*! Initialize AT controller
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param baudrate Modem baudrate (will be set on underlying UART)
 *  \param forceIdle Forces IDLE state after init (does not wait for modem turn on) when TRUE
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_Init(AtCtrl* ctrl, UBFrequency baudrate, bool forceIdle);

/*! Enter SW reset state
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_EnterSoftwareReset(AtCtrl* ctrl);

/*! Hook command callback
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param command AT command string
 *  \param callback Callback that will be called when AT command result is received by controller
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_HookCallback(AtCtrl* ctrl, const char* command, AtCtrlCallback* callback);

/*! Set custom modem OK reply. Default OK modem reply is "OK". You can change it using this function. It is set back to default after every command.
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param result Modem reply to be set
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_SetCustomOkResult(AtCtrl* ctrl, const char* result, bool isStrict);

AtCtrlResult AtCtrl_PrepareRawRead(AtCtrl* ctrl, uint8_t* buffer, uint16_t sizeToRead, uint8_t linesToSkip);

AtCtrlResult AtCtrl_PrepareRawReadUnsafe(AtCtrl* ctrl, uint8_t* buffer, uint16_t sizeToRead, uint8_t linesToSkip);

AtCtrlResult AtCtrl_TerminateRawRead(AtCtrl* ctrl);

/*! Send partial AT command
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param isEnd Indicates last part of AT command chunk
 *  \param command Command chunk string
 *  \param result AT command result string destination
 *  \param maxResultLength Maximal length of AT command result string
 *  \param timeout Maximal AT command result timeout
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_SendAtPartial(AtCtrl* ctrl, bool isEnd, bool sendCrlf, const char* command, char* result, uint16_t maxResultLength, UBTimeInterval timeout);

/*! Send AT command synchronously (blocks until result is received or timeout is reached)
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param command AT command string to be send (must not be NULL)
 *  \param result AT command result string destination
 *  \param maxResultLength Maximal length of AT command result string
 *  \param timeout Maximal AT command result timeout
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_SendAtSync(AtCtrl* ctrl, const char* command, char* result, uint16_t maxResultLength, UBTimeInterval timeout);

/*! Send AT command asynchronously (does not block, it calls callback when result is received)
 *  \param ctrl AT controller struct instance ptr (must not be NULL)
 *  \param command AT command string to be send (must not be NULL)
 *  \param callback AT command result callback
 *  \param timeout Maximal AT command result timeout
 *  \return ATCR_OK on success
 */
AtCtrlResult AtCtrl_SendAtAsync(AtCtrl* ctrl, const char* command, AtCtrlCallback* callback, UBTimeInterval timeout);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_IC_AT_CONTROLLER_H_ 
