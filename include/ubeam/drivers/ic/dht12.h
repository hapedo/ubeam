/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   dht12.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_IC_DHT12_H_
#define _UBEAM_DRIVERS_IC_DHT12_H_

#include <stdint.h>
#include <stdbool.h>
#include <ubeam/drivers/i2c_master.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*! \defgroup ubeam_drivers_ic_dht12 DHT12 Temperature sensor driver
 *  \ingroup ubeam_drivers_ic
 *  \brief DHT12 Temperature sensor driver
 */

/*! \addtogroup ubeam_drivers_ic_dht12
 *  @{
 */

//! Operation result
typedef enum
{
    DHT12R_OK = 0,                      	//!< Result OK
	DHT12R_NOT_CONFIGURED,				    //!< Driver not configured
    DHT12R_NOT_INITIALIZED,             	//!< Driver not initialized
	DHT12R_BUSY,							//!< Driver is busy
    DHT12R_I2C_ERROR,                   	//!< SPI transceive error
    DHT12R_BAD_PARAM,                   	//!< Bad parameter
    DHT12R_TIMEOUT,                     	//!< Operation timeouted
	DHT12R_ERROR
} Dht12Result;

//! Driver state
typedef enum
{
    DHT12S_NOT_CONFIGURED,					//!< Driver not configured
    DHT12S_NOT_INITIALIZED,					//!< Driver configured but not initialized
	DHT12S_IDLE,							//!< Driver is idle
	DHT12S_BUSY								//!< Driver is busy
} Dht12State;

//! DHT12 driver struct
typedef struct
{
    UBMutex mutex;							//!< Driver mutex
    UBI2cMaster* i2c;						//!< Underlying I2C driver struct ptr
    Dht12State state;						//!< Driver state
} Dht12;

/*! Configure DHT12 driver
 *	\param dht Dht struct instance ptr
 *	\param i2c Underlying I2C driver struct ptr
 */
void Dht12_Configure(Dht12* dht, UBI2cMaster* i2c);

/*! Initialize driver
 *	\param dht Dht struct instance ptr
 *	\return DHT12R_OK on success
 */
Dht12Result Dht12_Init(Dht12* dht);

/*! Synchronous temperature and humidity read
 *	\param dht Dht struct instance ptr
 *  \param temperature Temperature destination (returns temperature in deg C * 10, e.g. 255 = 25.5C)
 *  \param humidity Humidity destination (returns humidity in % * 10, e.g. 541 = 54.1%)
 *  \return DHT12R_OK on success
 */
Dht12Result Dht12_GetTempHumSync(Dht12* dht, int16_t* temperature, uint16_t* humidity);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_IC_DHT12_H_
