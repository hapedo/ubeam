/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   eeprom.h
 * \brief  microBeam SDK
 * \date   31.07.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_EEPROM_H_
#define _UBEAM_DRIVERS_EEPROM_H_

#include <stdlib.h>
#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/eeprom_types.h>

/*! \defgroup ubeam_drivers_eeprom On-chip EEPROM memory driver
 *  \ingroup ubeam_drivers
 *  \brief On-chip EEPROM memory driver
 */

/*! \addtogroup ubeam_drivers_eeprom
 *  @{
 */

//! EEPROM memory driver struct
typedef struct
{
    UBPeripheral peri;							//!< Peripheral instance
    void* platform;								//!< Platform dependent data
} UBEepBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
//#include <ubeam/stm32f0/drivers/eeprom.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/eeprom.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_X86)
#include <ubeam/x86/drivers/eeprom.h>
#else
typedef struct
{
    UBEepBase base;
} UBEep;
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*! Initialize driver
 *  \param eep EEPROM mem driver instance ptr (must not be NULL)
 *  \return UBEEPR_OK on success
 */
UBEepResult UBEep_Init(UBEep* nvm);

/*! Mass erase memory
 *  \param eep EEPROM mem driver instance ptr (must not be NULL)
 *  \param offset Block offset
 *  \return UBEEPR_OK on success
 */
UBEepResult UBEep_MassErase(UBEep* eep);

/*! Write memory
 *  \param eep EEPROM mem driver instance ptr (must not be NULL)
 *  \param offset Start offset
 *  \param data Data to write (must not be NULL)
 *  \param size Data size to write
 *  \param sizeWritten Number of bytes written (may be NULL)
 *  \return UBEEPR_OK on success
 */
UBEepResult UBEep_Write(UBEep* eep, uint32_t offset, const uint8_t* data, size_t size, size_t* sizeWritten);

/*! Read memory
 *  \param eep EEPROM mem driver instance ptr (must not be NULL)
 *  \param offset Start offset
 *  \param data Destination for data read (must not be NULL)
 *  \param size Data size to read
 *  \param sizeRead Number of bytes read (may be NULL)
 *  \return UBEEPR_OK on success
 */
UBEepResult UBEep_Read(UBEep* eep, uint32_t offset, uint8_t* data, size_t size, size_t* sizeRead);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_EEPROM_H_
