/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   rtc.h
 * \brief  microBeam SDK
 * \date   31.7.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_RTC_H_
#define _UBEAM_DRIVERS_RTC_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/rtc_types.h>

/*! \defgroup ubeam_drivers_rtc RTC
 *  \ingroup ubeam_drivers
 *  \brief RTC driver
 */

/*! \addtogroup ubeam_drivers_rtc
 *  @{
 */

//! RTC driver struct
typedef struct
{
	UBPeripheral peri;							//!< Peripheral instance
    void* platform;								//!< Platform dependent data
} UBRtcBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
//#include <ubeam/stm32f0/drivers/rtc.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/rtc.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_X86)
#include <ubeam/x86/drivers/rtc.h>
#else
typedef struct
{
    UBRtcBase base;
} UBRtc;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure RTC driver.
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param handle RTC peripheral handle
 */
void UBRtc_Configure(UBRtc* rtc, UBPeriHandle handle);

/*! Enable RTC interrupts
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 */
void UBRtc_EnableInt(UBRtc* rtc);

/*! Disable RTC interrupts
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 */
void UBRtc_DisableInt(UBRtc* rtc);

/*! Get driver current state
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \return Current state (UBRtcState enum value)
 */
UBRtcState UBRtc_GetState(UBRtc* rtc);

/*! Initialize RTC driver
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *	\param startRtc Start RTC clock if not running when true
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_Init(UBRtc* rtc, bool startRtc);

/*! Start RTC
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_Start(UBRtc* rtc);

/*! Stop RTC
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_Stop(UBRtc* rtc);

/*! Check whether RTC is running
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param isRunning Result ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_IsRunning(UBRtc* rtc, bool* isRunning);

/*! Get current clock
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param dateTime Result destination ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_GetTime(UBRtc* rtc, UBRtcDateTime* dateTime);

/*! Get current clock as timestamp
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param timestamp Result destination ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_GetTimestamp(UBRtc* rtc, UBRtcTimestamp* timestamp);

/*! Set current clock
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param dateTime Date/time ptr to be set (must not be NULL)
 *  \param setDate Sets also date when true, sets time only when false
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_SetTime(UBRtc* rtc, const UBRtcDateTime* dateTime, bool setDate);

/*! Convert date/time to timestamp
 *  \param dateTime Date/time ptr to convert (must not be NULL)
 *  \param timestamp Destination ptr (must not be NULL)
 *  \return true on success, false when out of range or error
 */
 bool UBRtc_ToTimestamp(const UBRtcDateTime* dateTime, UBRtcTimestamp* timestmp);

/*! Convert timestamp to date/time
 *  \param timestamp Timestamp to convert
 *  \param dateTime Date/time result destination ptr (must not be NULL)
 *  \return true on success, false when out of range
 */
bool UBRtc_FromTimestamp(UBRtcTimestamp timestamp, UBRtcDateTime* dateTime);

/*! Get maximal number of alarms supported
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param count Result destination ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_GetMaxAlarms(UBRtc* rtc, uint8_t* count);

/*! Set alram
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param index Alarm index to be set (0 to UBRtc_GetMaxAlarms)
 *  \param alarm Alarm ptr to be set (must not be NULL)
 *  \param enable Elable alarm after set when true
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_SetAlarm(UBRtc* rtc, uint8_t index, const UBRtcAlarm* alarm, bool enable);

/*! Get alram
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param index Alarm index to be set (0 to UBRtc_GetMaxAlarms)
 *  \param alarm Alarm ptr to be set (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_GetAlarm(UBRtc* rtc, uint8_t index, UBRtcAlarm* alarm);

/*! Enable alarm
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param index Alarm index to be enabled (0 to UBRtc_GetMaxAlarms)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_EnableAlarm(UBRtc* rtc, uint8_t index);

/*! Disable alarm
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param index Alarm index to be disabled (0 to UBRtc_GetMaxAlarms)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_DisableAlarm(UBRtc* rtc, uint8_t index);

/*! Check whether alarm is enabled
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param index Alarm index to be checked (0 to UBRtc_GetMaxAlarms)
 *  \param isEnabled Result ptr (must not be NULL)
 *  \return UBRTCR_OK on success
 */
UBRtcResult UBRtc_AlarmIsEnabled(UBRtc* rtc, uint8_t index, bool* isEnabled);

/*! Sleep and wait for alarm. This function may never return (depends on platform)
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param force Force sleep no matter in which state RTC driver is when set to true
 *  \param UBRTCR_OK after successful alarm wakeup
 */
UBRtcResult UBRtc_SleepAndWaitAlarm(UBRtc* rtc, bool force);

/*! Store data to RTC backup memory
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param offset Memory offset to write to
 *  \param data Data ptr to be written (must not be NULL)
 *  \param size Data size
 *  \param UBRTCR_OK after successful alarm wakeup
 */
UBRtcResult UBRtc_StoreBackup(UBRtc* rtc, uint32_t offset, const uint8_t* data, uint32_t size);

/*! Read data from RTC backup memory
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param offset Memory offset to read from
 *  \param data Destination data ptr (must not be NULL)
 *  \param size Data size
 *  \param UBRTCR_OK after successful alarm wakeup
 */
UBRtcResult UBRtc_ReadBackup(UBRtc* rtc, uint32_t offset, uint8_t* data, uint32_t size);

/*! Get RTC backup memory size
 *  \param rtc RTC driver struct instance ptr (must not be NULL)
 *  \param size Data size destination ptr (must not be NULL)
 *  \param UBRTCR_OK after successful alarm wakeup
 */
UBRtcResult UBRtc_GetBackupSize(UBRtc* rtc, uint32_t* size);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_RTC_H_

