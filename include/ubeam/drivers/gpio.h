/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   gpio.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_GPIO_H_
#define _UBEAM_DRIVERS_GPIO_H_

#include <stdint.h>
#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>
#include <ubeam/drivers/gpio_types.h>

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
#include <ubeam/stm32f0/drivers/definitions.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/definitions.h>
#endif

/*! \defgroup ubeam_drivers_gpio GPIO
 *  \ingroup ubeam_drivers
 */

/*! \addtogroup ubeam_drivers_gpio
 *  @{
 \brief Set of functions for GPIO setup and control

 \section Usage
 UBGpioPort struct acts like GPIO port object that holds all information about
 port. UBGpioPort_Configure() should be used first to initialize and control
 UBGpioPort struct instance.

 UBGpioPin struct acts like GPIO pin object that holds all information about
 pin and port. UBGpioPin_Configure() should be used first to initialize and control
 UBGpioPin struct instance.

 \section example1 Code example - output (STM32F0xx)

 \code
    UBGpioPin pinLedAct;

	UBGpioPin_Configure(&pinLedAct, (UBPeriHandle)GPIOB_BASE, UBGpioPin12);
	UBPower_EnablePower(pinLedAct.port.handle);
	UBGpioPin_Output(&pinLedAct);

	UBTimestamp blinkTimer;
	while(1)
	{
		if (UBSysTime_HasPassed(blinkTimer))
		{
			blinkTimer = UBSysTime_GetFutureMilli(500);
			UBGpioPin_Invert(&pinLedAct);
		}
	}
 \endcode

 \section example2 Code example - alternative function (STM32F0xx)

 \code
	UBGpioPin pinNrfSck;
	UBGpioPin pinNrfMiso;
	UBGpioPin pinNrfMosi;
	UBGpioPin pinNrfCsn;
	UBGpioPin pinNrfCe;

	UBSpiMaster spi;
	Nrf24l01 nrf;
	NrfAddress address;

	UBGpioPin_Configure(&pinNrfSck, (UBPeriHandle)GPIOA_BASE, UBGpioPin5);
	UBGpioPin_Configure(&pinNrfMiso, (UBPeriHandle)GPIOA_BASE, UBGpioPin6);
	UBGpioPin_Configure(&pinNrfMosi, (UBPeriHandle)GPIOA_BASE, UBGpioPin7);
	UBGpioPin_Configure(&pinNrfCsn, (UBPeriHandle)GPIOA_BASE, UBGpioPin4);
	UBGpioPin_Configure(&pinNrfCe, (UBPeriHandle)GPIOB_BASE, UBGpioPin0);

	UBPower_EnablePower(pinNrfSck.port.handle);
	UBPower_EnablePower(pinNrfMiso.port.handle);
	UBPower_EnablePower(pinNrfMosi.port.handle);
	UBPower_EnablePower(pinNrfCsn.port.handle);
	UBPower_EnablePower(pinNrfCe.port.handle);

	UBGpioPin_SetFunction(&pinNrfSck, UBGPF_SPI_1);
	UBGpioPin_SetFunction(&pinNrfMiso, UBGPF_SPI_1);
	UBGpioPin_SetFunction(&pinNrfMosi, UBGPF_SPI_1);

	UBGpioPin_Output(&pinNrfCsn);
	UBGpioPin_Output(&pinNrfCe);

	UBSpiMaster_Configure(&spi, (UBPeriHandle)SPI1, &pinNrfMiso, &pinNrfMosi, &pinNrfSck, 0);
	UBSpiMaster_Init(&spi, UBSMCPOL_0, UBSMCPHA_0, true);

	address[0] = 10;
	address[1] = 4;
	address[2] = 3;
	address[3] = 2;
	address[4] = 1;

	Nrf_Configure(&nrf, &spi, &pinNrfCsn, &pinNrfCe);
	Nrf_Init(&nrf, address, true, true, NRFCL_2BYTES, true);
	...
 \endcode

 */

/*! \brief GPIO port object
 UBGpioPort struct holds the information about GPIO port. You should not write
 to any of member directly. Always use appropriate UBGpioPort_ function.

 platform member holds platform dependent data.
 */
typedef struct
{
    UBPeriHandle handle;								//!< GPIO port handle
    void* platform;										//!< Platform dependent data ptr
} UBGpioPortBase;

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_X86)
#include <ubeam/x86/drivers/gpio.h>
#else
typedef struct
{
    UBGpioPortBase base;
} UBGpioPort;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! \brief GPIO pin object
 UBGpioPin struct holds the information about GPIO pin. You should not write
 to any of member directly. Always use appropriate UBGpioPin_ function.

 platform member holds platform dependent data.
 */
typedef struct
{
    UBGpioPort port;									//!< GPIO port struct instance
    UBGpioPinMask mask;									//!< GPIO pin mask
    void* platform;										//!< Platform dependent data ptr
} UBGpioPin;

/*! Configure GPIO port
 * \param port GPIO port struct ptr
 * \param portHandle GPIO port handle
 */
void UBGpioPort_Configure(UBGpioPort* port, UBPeriHandle portHandle);

/*! Set port pins to H
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be set to H
 */
void UBGpioPort_Set(UBGpioPort* port, UBGpioPinMask mask);

/*! Set port pins to L
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be set to L
 */
void UBGpioPort_Clear(UBGpioPort* port, UBGpioPinMask mask);

/*! Invert port pins (H to L, L to H)
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be inverted
 */
void UBGpioPort_Invert(UBGpioPort* port, UBGpioPinMask mask);

/*! Raw write to GPIO port
 *  \param port GPIO port struct ptr
 *  \param value Value to be written to port
 */
void UBGpioPort_Write(UBGpioPort* port, UBGpioPinMask value);

/*! Raw read from GPIO port
 *  \param port GPIO port struct ptr
 *  \return Value read from port
 */
UBGpioPinMask UBGpioPort_Read(UBGpioPort* port);

/*! Check whether port pins are in H
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be checked
 *  \return true when all pins specified in mask are in H
 */
bool UBGpioPort_IsSet(UBGpioPort* port, UBGpioPinMask mask);

/*! Check whether port pins are in L
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be checked
 *  \return true when all pins specified in mask are in L
 */
bool UBGpioPort_IsCleared(UBGpioPort* port, UBGpioPinMask mask);

/*! Configure GPIO port pins to outputs
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be set
 */
void UBGpioPort_Output(UBGpioPort* port, UBGpioPinMask mask);

/*! Configure GPIO port pins to inputs
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be set
 */
void UBGpioPort_Input(UBGpioPort* port, UBGpioPinMask mask);

/*! Check if GPIO port pins are configured to outputs
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be checked
 *  \return true when all pins specified in mask are configured as outputs
 */
bool UBGpioPort_IsOutput(UBGpioPort* port, UBGpioPinMask mask);

/*! Check if GPIO port pins are configured to inputs
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be checked
 *  \return true when all pins specified in mask are configured as inputs
 */
bool UBGpioPort_IsInput(UBGpioPort* port, UBGpioPinMask mask);

/*! Configure GPIO port pins function
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be configured
 *  \param func Pin functionality to be set
 *	\return true on success
 */
bool UBGpioPort_SetFunction(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinFunc func);

/*! Configure GPIO port pins speed
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be configured
 *  \param speed Pin speed to be set
 */
void UBGpioPort_SetSpeed(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinSpeed speed);

/*! Configure GPIO port pins pull resistor
 *  \param port GPIO port struct ptr
 *  \param mask Pin mask to be configured
 *  \param pull Pull settings to be set
 */
void UBGpioPort_SetPullResistor(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinPull pull);

/*! Configure GPIO pin
 * \param pin GPIO pin struct ptr
 * \param portHandle GPIO port handle
 * \param mask GPIO pin mask
 */
void UBGpioPin_Configure(UBGpioPin* pin, UBPeriHandle portHandle, UBGpioPinMask mask);

/*! Set GPIO pin to H
 *  \param pin GPIO pin struct ptr
 */
void UBGpioPin_Set(UBGpioPin* pin);

/*! Set GPIO pin to L
 *  \param pin GPIO pin struct ptr
 */
void UBGpioPin_Clear(UBGpioPin* pin);

/*! Invert GPIO pin (H to L, L to H)
 *  \param pin GPIO pin struct ptr
 */
void UBGpioPin_Invert(UBGpioPin* pin);

/*! Raw write to GPIO pin
 *  \param pin GPIO pin struct ptr
 */
void UBGpioPin_Write(UBGpioPin* pin, bool value);

/*! Raw read from GPIO pin
 *  \param pin GPIO pin struct ptr
 *  \return true when pin is in H
 */
bool UBGpioPin_Read(UBGpioPin* pin);

/*! Check whether pin is in H
 *  \param pin GPIO pin struct ptr
 *  \return true when pin is in H
 */
bool UBGpioPin_IsSet(UBGpioPin* pin);

/*! Check whether pin is in L
 *  \param pin GPIO pin struct ptr
 *  \return true when pin is in L
 */
bool UBGpioPin_IsCleared(UBGpioPin* pin);

/*! Configure GPIO pin to output
 *  \param pin GPIO pin struct ptr
 */
void UBGpioPin_Output(UBGpioPin* pin);

/*! Configure GPIO pin to input
 *  \param pin GPIO pin struct ptr
 */
void UBGpioPin_Input(UBGpioPin* pin);

/*! Check if GPIO pin is configured to output
 *  \param pin GPIO pin struct ptr
 *  \return true when pin is configured as output
 */
bool UBGpioPin_IsOutput(UBGpioPin* pin);

/*! Check if GPIO pin is configured to input
 *  \param pin GPIO pin struct ptr
 *  \return true when pin is configured as input
 */
bool UBGpioPin_IsInput(UBGpioPin* pin);

/*! Configure GPIO pin function
 *  \param pin GPIO pin struct ptr
 *  \param func Pin functionality to be set
 *	\return true on success
 */
bool UBGpioPin_SetFunction(UBGpioPin* pin, UBGpioPinFunc func);

/*! Configure GPIO pin speed
 *  \param pin GPIO pin struct ptr
 *  \param speed Pin speed to be set
 */
void UBGpioPin_SetSpeed(UBGpioPin* pin, UBGpioPinSpeed speed);

/*! Configure GPIO pin pull resistor
 *  \param pin GPIO pin struct ptr
 *  \param pull Pull settings to be set
 */
void UBGpioPin_SetPullResistor(UBGpioPin* pin, UBGpioPinPull pull);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_DRIVERS_GPIO_H_ 
