/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   power_management.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_DRIVERS_POWER_MANAGEMENT_H_
#define _UBEAM_DRIVERS_POWER_MANAGEMENT_H_

#include <stdbool.h>
#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
#include <ubeam/stm32f0/drivers/power_management.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/power_management.h>
#endif

/*! \defgroup ubeam_drivers_powermgm Power management
 *  \ingroup ubeam_drivers
 *  \brief Power management driver
 */

/*! \addtogroup ubeam_drivers_powermgm
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*! Enable peripheral power
 *  \param handle Peripeheral handle to be enabled
 *  \return true on success
 */
bool UBPower_EnablePower(UBPeriHandle handle);

/*! Disable peripheral power
 *  \param handle Peripeheral handle to be disabled
 *  \return truen on success
 */
bool UBPower_DisablePower(UBPeriHandle handle);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_DRIVERS_POWER_MANAGEMENT_H_
