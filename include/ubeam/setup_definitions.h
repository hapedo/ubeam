/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   setup_definitions.h
 * \brief  microBeam SDK
 * \date   22.12.2019
 * \note
 * \todo
 */

/*! \ingroup ubeam
 *	@{
 */

#ifndef _UBEAM_SETUP_DEFINITIONS_
#define _UBEAM_SETUP_DEFINITIONS_

#ifdef __cplusplus
extern "C"
{
#endif

//! Unknown CPU architecture
#define UBEAM_CPU_CORE_ARCH_UNKNOWN                     (0x00)
//! ARM Cortex-M CPU architecture
#define UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M                (0x01)
//! x86 64bit CPU architecture
#define UBEAM_CPU_CORE_ARCH_X86_64                      (0x02)

//! Unknown CPU core type
#define UBEAM_CPU_CORE_TYPE_UNKNOWN                     (0x00)
//! ARM Cortex-M0 CPU core type
#define UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0               (0x01)
//! ARM Cortex-M3 CPU core type
#define UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M3               (0x02)
//! x86 CPU core type
#define UBEAM_CPU_CORE_TYPE_X86                         (0x10)

//! Unknown CPU model
#define UBEAM_CPU_MODEL_UNKNOWN                         (0x0100)
//! STM32F030x4 CPU model
#define UBEAM_CPU_MODEL_STM32F030x4                     (0x0101)
//! STM32F030x6 CPU model
#define UBEAM_CPU_MODEL_STM32F030x6                     (0x0102)
//! STM32F030x8 CPU model
#define UBEAM_CPU_MODEL_STM32F030x8                     (0x0103)
//! STM32F030xC CPU model
#define UBEAM_CPU_MODEL_STM32F030xC                     (0x0104)
//! STM32F031 CPU model
#define UBEAM_CPU_MODEL_STM32F031                       (0x0105)
//! STM32F042 CPU model
#define UBEAM_CPU_MODEL_STM32F042                       (0x0106)
//! STM32F051 CPU model
#define UBEAM_CPU_MODEL_STM32F051                       (0x0107)
//! STM32F070x6 CPU model
#define UBEAM_CPU_MODEL_STM32F070x6                     (0x0108)
//! STM32F070x8 CPU model
#define UBEAM_CPU_MODEL_STM32F070xB                     (0x0109)
//! STM32F072 CPU model
#define UBEAM_CPU_MODEL_STM32F072                       (0x010a)
//! STM32F091 CPU model
#define UBEAM_CPU_MODEL_STM32F091                       (0x010b)

//! STM32L100xB CPU model
#define UBEAM_CPU_MODEL_STM32L100xB                     (0x0200)
//! STM32L100xBA CPU model
#define UBEAM_CPU_MODEL_STM32L100xBA                    (0x0201)
//! STM32L100xC CPU model
#define UBEAM_CPU_MODEL_STM32L100xC                     (0x0202)
//! STM32L151xB CPU model
#define UBEAM_CPU_MODEL_STM32L151xB                     (0x0203)
//! STM32L151xBA CPU model
#define UBEAM_CPU_MODEL_STM32L151xBA                    (0x0204)
//! STM32L151xC CPU model
#define UBEAM_CPU_MODEL_STM32L151xC                     (0x0205)
//! STM32L151xCA CPU model
#define UBEAM_CPU_MODEL_STM32L151xCA                    (0x0206)
//! STM32L151xD CPU model
#define UBEAM_CPU_MODEL_STM32L151xD                     (0x0207)
//! STM32L151xE CPU model
#define UBEAM_CPU_MODEL_STM32L151xE                     (0x0208)
//! STM32L151xB CPU model
#define UBEAM_CPU_MODEL_STM32L152xB                     (0x0209)
//! STM32L152xBA CPU model
#define UBEAM_CPU_MODEL_STM32L152xBA                    (0x020a)
//! STM32L152xC CPU model
#define UBEAM_CPU_MODEL_STM32L152xC                     (0x020b)
//! STM32L152xCA CPU model
#define UBEAM_CPU_MODEL_STM32L152xCA                    (0x020c)
//! STM32L152xD CPU model
#define UBEAM_CPU_MODEL_STM32L152xD                     (0x020d)
//! STM32L152xDX CPU model
#define UBEAM_CPU_MODEL_STM32L152xDX                    (0x020e)
//! STM32L152xE CPU model
#define UBEAM_CPU_MODEL_STM32L152xE                     (0x020f)
//! STM32L162xC CPU model
#define UBEAM_CPU_MODEL_STM32L162xC                     (0x0210)
//! STM32L162xCA CPU model
#define UBEAM_CPU_MODEL_STM32L162xCA                    (0x0211)
//! STM32L162xD CPU model
#define UBEAM_CPU_MODEL_STM32L162xD                     (0x0212)
//! STM32L162xDX CPU model
#define UBEAM_CPU_MODEL_STM32L162xDX                    (0x0213)
//! STM32L162xE CPU model
#define UBEAM_CPU_MODEL_STM32L162xE                     (0x0214)

//! Generic x86 CPU model
#define UBEAM_CPU_MODEL_GENERIC_X86                     (0x1000)

//! Unknown CPU family
#define UBEAM_CPU_FAMILY_UNKNOWN                        (0)

//! STM32F0 family
#define UBEAM_CPU_FAMILY_STM32F0                        (0x0100)
//! STM32L1 family
#define UBEAM_CPU_FAMILY_STM32L1                        (0x0200)
//! Generic x86 family
#define UBEAM_CPU_FAMILY_X86                            (0x1000)

//! Unknown CPU manufacturer
#define UBEAM_CPU_MANUF_UNKNOWN                         (0x0000)
//! ST CPU manufacturer
#define UBEAM_CPU_MANUF_STM                             (0x0001)
//! Intel CPU manufacturer
#define UBEAM_CPU_MANUF_INTEL                           (0x0002)

//! No operating system (bare metal)
#define UBEAM_PLATFORM_TYPE_NONE						(0x00)
//! Windows operating system
#define UBEAM_PLATFORM_TYPE_WINDOWS 					(0x01)

/*! @} */

#ifdef __cplusplus
}
#endif

#endif // _UBEAM_SETUP_DEFINITIONS_ 


/*! \defgroup ubeam microBeam SDK
 *	\brief microBeam SDK
 */

/*! \defgroup ubeam_drivers On-chip drivers
 *  \ingroup ubeam
 *	\brief On-chip drivers
 */

/*! \defgroup ubeam_drivers_cortexm ARM Cortex-M on-chip drivers
 *  \ingroup ubeam_drivers
 *	\brief ARM Cortex-M on-chip drivers
 */

/*! \defgroup ubeam_drivers_stm32f0 STM32F0 on-chip drivers
 *  \ingroup ubeam_drivers
 *	\brief STM32F0 on-chip drivers
 */

 /*! \defgroup ubeam_drivers_stm32l1 STM32L1 on-chip drivers
 *  \ingroup ubeam_drivers
 *	\brief STM32L1 on-chip drivers
 */

/*! \defgroup ubeam_drivers_ic External IC drivers
 *  \ingroup ubeam_drivers
 *	\brief External IC drivers
 */

/*! \defgroup ubeam_os Operating system
 *  \ingroup ubeam
 *	\brief Operating system primitives
 */

/*! \defgroup ubeam_utils Utilities
 *  \ingroup ubeam
 *	\brief Generic utilities
 */

/*! \mainpage microBeam SDK
\section introduction Introduction
microBeam SDK is a complete, lightweight and easy to use library for writing
firmware in C. It is designed to be platform and architecture independent
however the only supported architecture is STM32F0 CPU family with no OS support
(bare metal) at this moment.

\section distribution Distribution
microBeam is distributed as source code in git repository and as precompiled static
library with appropriate set of header files.

\section configuration Configuration
Several definitions must be set before build:
\code
UBEAM_CPU_CORE_ARCH // CPU core architecture (e.g. UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M)
UBEAM_CPU_CORE_TYPE // CPU core type (e.g. UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
UBEAM_CPU_MODEL     // CPU model (e.g. UBEAM_CPU_MODEL_STM32F030x8)
UBEAM_PLATFORM		// Operating system type (e.g. UBEAM_PLATFORM_NONE)
\endcode

You can pass definitions as parameters to cmake (-DUBEAM_CPU_CORE_TYPE=UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
or in config.h.

\section build Build
microBeam library build is cmake driven. All configuration definitions may be passed
as arguments to cmake. Also toolchain should be specified by cmake toolchain file.

Example cmake command line:
\code
cmake pathToSource -DCMAKE_BUILD_TYPE=Debug -DUBEAM_CPU_CORE_TYPE=UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0 -DUBEAM_CPU_CORE_ARCH=UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M -DUBEAM_CPU_MODEL=UBEAM_CPU_MODEL_STM32F030x8 -DCMAKE_TOOLCHAIN_FILE=toolchainCortexM_win.cmake -G "Unix Makefiles"
\endcode

\section usage Usage in project
Several steps must be done to use microBeam in your project:
 1. Add include paths to following directories (Cortex-M0, STM32F0, no OS). Please note that CMSIS path is required to include for ARM Cortex-M:
 \code
   /include
   /platform/none/stm32f0/include
   /platform/none/cortex-m/include
   /platform/none/generic/include
   /platform/none/cortex-m/include/ubeam/cortex-m/3rd_party/CMSIS
 \endcode
 2. Define symbols as defined before microBeam build, example:
 \code
   UBEAM_CPU_CORE_TYPE=UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0
   UBEAM_CPU_CORE_ARCH=UBEAM_CPU_CORE_ARCH_ARM_CORTEX_M
   UBEAM_CPU_MODEL=UBEAM_CPU_MODEL_STM32F030x8
 \endcode
 3. Add ubeam to you link libraries

\section example Code example
\code
#include <string.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/drivers/clock_management.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/drivers/spi_master.h>
#include <ubeam/drivers/ic/nrf24l01.h>
#include <ubeam/utils/system_time.h>
#include <ubeam/os/thread.h>

UBGpioPin pinNrfPower;
UBGpioPin pinModemPower;

UBGpioPin pinNrfSck;
UBGpioPin pinNrfMiso;
UBGpioPin pinNrfMosi;
UBGpioPin pinNrfCsn;
UBGpioPin pinNrfCe;

UBSpiMaster spi;

Nrf24l01 nrf;

NrfAddress address;
NrfAddress destination;

UBTimestamp txTimer;

void MainThread_Init(UBThread* thread)
{
	// Configure NRF pins
	UBGpioPin_Configure(&pinNrfSck, (UBPeriHandle)GPIOA_BASE, UBGpioPin5);
	UBGpioPin_Configure(&pinNrfMiso, (UBPeriHandle)GPIOA_BASE, UBGpioPin6);
	UBGpioPin_Configure(&pinNrfMosi, (UBPeriHandle)GPIOA_BASE, UBGpioPin7);
	UBGpioPin_Configure(&pinNrfCsn, (UBPeriHandle)GPIOA_BASE, UBGpioPin4);
	UBGpioPin_Configure(&pinNrfCe, (UBPeriHandle)GPIOB_BASE, UBGpioPin0);

	UBPower_EnablePower(pinNrfSck.port.handle);
	UBPower_EnablePower(pinNrfMiso.port.handle);
	UBPower_EnablePower(pinNrfMosi.port.handle);
	UBPower_EnablePower(pinNrfCsn.port.handle);
	UBPower_EnablePower(pinNrfCe.port.handle);

	UBGpioPin_SetFunction(&pinNrfSck, UBGPF_SPI_1);
	UBGpioPin_SetFunction(&pinNrfMiso, UBGPF_SPI_1);
	UBGpioPin_SetFunction(&pinNrfMosi, UBGPF_SPI_1);

	UBGpioPin_Output(&pinNrfCsn);
	UBGpioPin_Output(&pinNrfCe);

	// Configure and initialize SPI master driver
	UBSpiMaster_Configure(&spi, (UBPeriHandle)SPI1, &pinNrfMiso, &pinNrfMosi, &pinNrfSck, 0);
	UBSpiMaster_Init(&spi, UBSMCPOL_0, UBSMCPHA_0, true);

	// Configure and initialize NRF instance
	// NRF local address
	address[0] = 10;
	address[1] = 4;
	address[2] = 3;
	address[3] = 2;
	address[4] = 1;

	Nrf_Configure(&nrf, &spi, &pinNrfCsn, &pinNrfCe);
	Nrf_Init(&nrf, address, true, true, NRFCL_2BYTES, true);

	// NRF destination address
	destination[0] = 10;
	destination[1] = 4;
	destination[2] = 3;
	destination[3] = 2;
	destination[4] = 0;

	blinkTimer = 0;
}

void MainThread_Exec(UBThread* thread)
{
	if (UBSysTime_HasPassed(txTimer))
	{
		// Transmit to destination
		txTimer = UBSysTime_GetFutureMilli(500);
		uint8_t data[32];
		memset(data, sizeof(data), 0);
		// Transmit to pipe 0, blocking (timeout set to 0)
		Nrf_Transmit(&nrf, destination, data, 4, 0, 0);
	}
}

int main()
{
	// System configuration
	UBSysTime_Configure();
	UBInt_EnableGlobal();

	// Create main thread
	UBThread mainThread;

	UBThread_Configure(&mainThread, MainThread_Init, 0, 0, MainThread_Exec);

	// Start thread
	UBThread_Start(&mainThread);
	// Start scheduler. We should not return from this function
	UBThread_StartScheduler();
}
\endcode
 */
