import os
import sys
import argparse
import json
import collections
import traceback

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--config', dest='config', default='', help='Generator config file')
        parser.add_argument('--projectname', dest='projectname', default='project', help='Generator config file')
        parser.add_argument('--outputdir', dest='outputdir', default=False, help='Generator output directory')
        parser.add_argument('--sources', dest='sources', default=False, help='Source file list')
        parser.add_argument('--headers', dest='headers', default=False, help='Header file list')
        parser.add_argument('--includes', dest='includes', default=False, help='Include directory list')
        parser.add_argument('--definitions', dest='definitions', default='', help='Definition list')
        args = parser.parse_args()
        print('Custom project generator')
        if (args.config == ''):
            print('No config file specified - terminating')
            exit(0)

        print('  config:             ' + args.config)
        print('  output directory:   ' + args.outputdir)
        print('  project name:       ' + args.projectname)            
        scriptDir = os.path.dirname(os.path.realpath(__file__))
        if (not os.path.isabs(args.config)):
            args.config = os.path.join(scriptDir, args.config)
        config = json.load(open(args.config, 'r'), object_pairs_hook=collections.OrderedDict)
        module = __import__(config["generator"])
        Generator = getattr(module, 'Generator') 
        generator = Generator(config, args.projectname, args.outputdir, args.sources, args.headers, args.includes, args.definitions)
        
    except Exception as exc:
        print('!! Error: ' + str(exc))
        print(traceback.format_exc())
        exit(1)
