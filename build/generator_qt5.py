import os

class Generator:
    def __init__(self, config, project, outputdir, sources, headers, includes, definitions):
    
        self.__scriptDir = os.path.dirname(os.path.realpath(__file__))
        self.__outputDir = outputdir
        self.__project = project
        self.__sources = self.__splitList(sources)
        self.__headers = self.__splitList(headers)
        self.__includes = self.__splitList(includes)
        self.__definitions = self.__splitList(definitions)
        self.__template = config["template"]
        
        result = self.__processTemplate()
        self.__writeProject(result)
        
    def __writeProject(self, content):
        fileName = os.path.join(self.__outputDir, self.__project + '.pro')
        if (not os.path.isabs(fileName)):
            fileName = os.path.join(self.__scriptDir, fileName)
        f = open(fileName, 'w')
        for s in content:
            f.write(s)
        f.close()
    
    def __processTemplate(self):
        fileName = self.__template
        if (not os.path.isabs(fileName)):
            fileName = os.path.join(self.__scriptDir, fileName)
        f = open(fileName, 'r')
        content = f.readlines()
        f.close()
        result = []
        for line in content:
            lastIdx = 0
            procLine = ''
            while (lastIdx != -1):
                idx1 = line.find('%', lastIdx)
                if (idx1 != -1):
                    idx2 = line.find('%', idx1 + 1)
                    if (idx2 != -1):
                        keyword = line[idx1 + 1 : idx2]
                        procLine = procLine + line[lastIdx : idx1] + self.__processKeyword(keyword)
                        lastIdx = idx2 + 1
                    else:
                        procLine = procLine + line[lastIdx:]
                        lastIdx = -1
                else:
                    procLine = procLine + line[lastIdx:]
                    lastIdx = -1
            result.append(procLine)
        return result
            
    def __processKeyword(self, keyword):
        if (keyword == 'SOURCES'):
            return self.__sources
        if (keyword == 'HEADERS'):
            return self.__headers
        if (keyword == 'INCLUDES'):
            return self.__includes
        if (keyword == 'DEFINES'):
            return self.__definitions
        if (keyword == 'PROJECT'):
            return self.__project
        return '%__UNKNOWN__%'
        
    def __splitList(self, list):
        list = list.split(';')
        result = ''
        n = 0;
        for s in list:
            if (n != 0):
                result = result + ' \\ \n    '
            result = result + s
            n = n + 1
        if (n != 0):
            result = result + '\n'
        return result