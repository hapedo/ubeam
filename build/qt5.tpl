#QT       += core serialport

TARGET = %PROJECT%
TEMPLATE = lib
CONFIG += staticlib
CONFIG -= qt

TARGET = ubeam

#QMAKE_CXXFLAGS += -O0
#QMAKE_CFLAGS += -O0

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# Output dirs
Release:DESTDIR = $${_PRO_FILE_PWD_}
Release:OBJECTS_DIR = $${_PRO_FILE_PWD_}/.obj
Release:MOC_DIR = $${_PRO_FILE_PWD_}/.moc
Release:RCC_DIR = $${_PRO_FILE_PWD_}/.rcc
Release:UI_DIR = $${_PRO_FILE_PWD_}/.ui

Debug:DESTDIR = $${_PRO_FILE_PWD_}
Debug:OBJECTS_DIR = $${_PRO_FILE_PWD_}/.obj
Debug:MOC_DIR = $${_PRO_FILE_PWD_}/.moc
Debug:RCC_DIR = $${_PRO_FILE_PWD_}/.rcc
Debug:UI_DIR = $${_PRO_FILE_PWD_}/.ui

DEFINES += %DEFINES%

INCLUDEPATH += %INCLUDES%

SOURCES += %SOURCES%

HEADERS += %HEADERS%

