/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   rtc.h
 * \brief  microBeam SDK
 * \date   31.07.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_X86_DRIVERS_RTC_H_
#define _UBEAM_X86_DRIVERS_RTC_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/rtc.h>
#include <ubeam/drivers/rtc_types.h>
#include <ubeam/drivers/gpio.h>

/*! \defgroup ubeam_drivers_x86_rtc x86 RTC driver
 *  \ingroup ubeam_drivers_x86
 *  \brief x86 RTC driver
 */

/*! \addtogroup ubeam_drivers_x86_rtc
 *  @{
 */

#define UBRTC_X86_ALARM_COUNT       1
#define UBRTC_X86_TAMPER_COUNT      (UBRTCTAMP_3 + 1)
#define UBRTC_X86_BACKUP_MEM_SIZE   (64)

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    UBRTCTAMP_1 = 0,
    UBRTCTAMP_2,
    UBRTCTAMP_3,
} UBRtcTamper;

typedef struct
{
    bool activeHigh;
    bool enabled;
    UBGpioPin* pin;
} UBRtcTamperDesc;

//! Platform specific RTC struct
typedef struct
{
    UBRtcBase base;             //!< Base struct
    UBRtcState state;           //!< Driver state
    UBRtcAlarm alarms[UBRTC_X86_ALARM_COUNT];       //!< Alarms
    bool alarmsEnabled[UBRTC_X86_ALARM_COUNT];
    bool isRunning;
    int32_t relTimestamp;
    UBRtcTamperDesc tamper[UBRTC_X86_TAMPER_COUNT];
    uint8_t backupMem[UBRTC_X86_BACKUP_MEM_SIZE];
} UBRtc;

UBRtcResult UBRtc_SetTamperPin(UBRtc* rtc, UBRtcTamper tamper, UBGpioPin* pin);

UBRtcResult UBRtc_ConfigureTamperPin(UBRtc* rtc, UBRtcTamper tamper, bool activeHigh);

UBRtcResult UBRtc_EnableTamperPin(UBRtc* rtc, UBRtcTamper tamper);

UBRtcResult UBRtc_DisableTamperPin(UBRtc* rtc, UBRtcTamper tamper);

bool UBRtc_IsTamperDetected(UBRtc* rtc, UBRtcTamper tamper, bool clearFlag);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_X86_DRIVERS_GPIO_H_
