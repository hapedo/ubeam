/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   watchdog.h
 * \brief  microBeam SDK
 * \date   10.08.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_X86_DRIVERS_WATCHDOG_H_
#define _UBEAM_X86_DRIVERS_WATCHDOG_H_

#include <stdint.h>
#include <ubeam/setup.h>
#include <ubeam/drivers/watchdog.h>
#include <ubeam/drivers/watchdog_types.h>

/*! \defgroup ubeam_drivers_x86_watchdog x86 watchdog driver
 *  \ingroup ubeam_drivers_x86
 *  \brief x86 watchdog driver
 */

/*! \addtogroup ubeam_drivers_x86_watchdog
 *  @{
 */

//! Platform specific watchdog struct
typedef struct
{
    UBWdgBase base;             //!< Base struct
    UBWdgState state;           //!< Driver state
} UBWdg;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_X86_DRIVERS_WATCHDOG_H_
