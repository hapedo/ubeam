/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   eeprom.h
 * \brief  microBeam SDK
 * \date   31.07.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_X86_DRIVERS_EEPROM_H_
#define _UBEAM_X86_DRIVERS_EEPROM_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/eeprom.h>
#include <ubeam/drivers/eeprom_types.h>
#include <Windows.h>

/*! \defgroup ubeam_drivers_x86_eeprom x86 EEPROM memory driver
 *  \ingroup ubeam_drivers_x86
 *  \brief x86 EEPROM memory driver
 */

/*! \addtogroup ubeam_drivers_x86_eeprom
 *  @{
 */

#ifdef __cplusplus
extern "C" {
#endif

//! Platform specific RTC struct
typedef struct
{
    UBEepBase base;             //!< Base struct
    UBEepState state;           //!< Driver state
    char fileName[256];        //!< File name
    size_t memSize;             //!< Memory size
    HANDLE handle;              //!< File handle
} UBEep;

void UBEep_Configure(UBEep* eep, const char* fileName, size_t memSize);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_X86_DRIVERS_EEPROM_H_
