/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   system_time.h
 * \brief  microBeam SDK
 * \date   28.06.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_X86_UTILS_SYSTEM_TIME_H_
#define _UBEAM_X86_UTILS_SYSTEM_TIME_H_

#include <ubeam/setup.h>

/*! \defgroup ubeam_utils_systime_x86 x86 system time
 *  \ingroup ubeam_utils_systime
 *  \brief x86 system time functions
 */

/*! \addtogroup ubeam_utils_systime_x86
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*! Get x86 ticks per second value
 *  \return Ticks per second value
 */
uint64_t UBx86SysTime_GetTicksPerSec();

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_X86_UTILS_SYSTEM_TIME_H_
