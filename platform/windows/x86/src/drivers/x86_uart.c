/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32f0_uart.c
 * \brief  microBeam SDK
 * \date   7.11.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/uart.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/power_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

static DWORD WINAPI threadExec(LPVOID param)
{
    UBUart* uart = (UBUart*)param;
    OVERLAPPED reader;
    reader.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (reader.hEvent == NULL)
        return 0;
    uint8_t buff[1];
    DWORD nread;
    while(!uart->shouldTerminate)
    {
        if (!ReadFile( uart->winHandle,
                 buff,
                 sizeof(buff),
                 &nread,
                 &reader))
        {
            if (GetLastError() != ERROR_IO_PENDING)
                continue;
            DWORD res = WaitForSingleObject(reader.hEvent, 500);
            switch(res)
            {
            case WAIT_OBJECT_0:
                if (!GetOverlappedResult(uart->winHandle, &reader, &nread, FALSE))
                {
                    break;
                }
                else
                {
                    //char str[2];
                    //str[0] = buff[1];
                    //str[1] = 0;
                    //printf("%s\n", str);
                    UBFifo_Put(uart->base.rxFifo, buff, nread);
                    if (uart->base.rxMonFifo)
                        UBFifo_Put(uart->base.rxMonFifo, buff, nread);
                }
                break;
            case WAIT_TIMEOUT:
                break;
            default:
                break;
            }
        }
        else
        {
            /*char str[2];
            str[0] = buff[1];
            str[1] = 0;
            printf("%s\n", str);*/
           UBFifo_Put(uart->base.rxFifo, buff, nread);
           if (uart->base.rxMonFifo)
               UBFifo_Put(uart->base.rxMonFifo, buff, nread);
        };
    }
    CloseHandle(reader.hEvent);
    return 0;
}

void UBUart_Configure(UBUart* uart, UBPeriHandle handle, UBGpioPin* rx, UBGpioPin* tx, UBFifo* rxFifo, UBFifo* txFifo)
{
	if (uart == 0)
		return;
	uart->base.platform = 0;
	UBPeri_Configure(&uart->base.peri, handle);

	uart->base.platform = uart;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return;
	UBGpioPinFunc func = UBGPF_GPIO;
	uart->base.rx = rx;
	if (rx)
	{
		UBPower_EnablePower(rx->port.base.handle);
		UBGpioPin_SetFunction(rx, func);
	}
	uart->base.tx = tx;
	if (tx)
	{
		UBPower_EnablePower(tx->port.base.handle);
		UBGpioPin_SetFunction(tx, func);
	}
	uart->baudrate = 1000000;
	uart->state = UBUS_NOT_INITIALIZED;
	uart->base.rxFifo = rxFifo;
	uart->base.txFifo = txFifo;
    uart->base.rxMonFifo = 0;
    uart->base.txMonFifo = 0;
    strcpy(uart->portName, "\\\\.\\");
    strcat(uart->portName, (const char*)handle);
    uart->state = UBUS_NOT_INITIALIZED;
	UBMutex_Unlock(&uart->base.peri.mutex);
}

void UBUart_EnableInt(UBUart* uart)
{
	if (uart == 0)
		return;
	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return;
	UBMutex_Unlock(&uart->base.peri.mutex);
}

void UBUart_DisableInt(UBUart* uart)
{
	if (uart == 0)
		return;
	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return;
	UBMutex_Unlock(&uart->base.peri.mutex);
}

UBUartState UBUart_GetState(UBUart* uart)
{
	if (uart == 0)
		return UBUS_NOT_CONFIGURED;
	return uart->state;
}

UBUartResult UBUart_Init(UBUart* uart, UBFrequency baudrate)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

	UBPower_EnablePower(uart->base.peri.handle);
	UBUart_EnableInt(uart);

    //wchar_t wtext[32];
    //mbstowcs(wtext, uart->portName, strlen(uart->portName)+1);
    if (uart->winHandle)
        UBUart_Close(uart);
    uart->winHandle = CreateFileA(uart->portName,
                                 GENERIC_READ | GENERIC_WRITE,
                                 0,
                                 NULL,
                                 OPEN_EXISTING,
                                 FILE_FLAG_OVERLAPPED,
                                 NULL);
    if (uart->winHandle == INVALID_HANDLE_VALUE)
    {
        UBMutex_Unlock(&uart->base.peri.mutex);
        return UBUR_NOT_INITIALIZED;
    }

    DCB dcbSerialParams = { 0 };
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    GetCommState(uart->winHandle, &dcbSerialParams);
    dcbSerialParams.BaudRate = CBR_9600;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity   = NOPARITY;
    SetCommState(uart->winHandle, &dcbSerialParams);
    COMMTIMEOUTS timeouts = {0};
    timeouts.ReadIntervalTimeout = MAXDWORD;
    timeouts.ReadTotalTimeoutConstant = 0;
    timeouts.ReadTotalTimeoutMultiplier = 0;
    timeouts.WriteTotalTimeoutConstant = 0;
    timeouts.WriteTotalTimeoutMultiplier = 0;
    if (!SetCommTimeouts(uart->winHandle, &timeouts))
    {
        UBMutex_Unlock(&uart->base.peri.mutex);
        return UBUR_NOT_INITIALIZED;
    }

    UBUart_SetBaudrate(uart, baudrate);

    uart->shouldTerminate = false;
    uart->thread = CreateThread(NULL,
                                65536,
                                (LPTHREAD_START_ROUTINE)threadExec,
                                uart,
                                0,
                                NULL);
    if (uart->thread == 0)
    {
        CloseHandle(uart->winHandle);
        UBMutex_Unlock(&uart->base.peri.mutex);
        return UBUR_NOT_INITIALIZED;
    }

	uart->state = UBUS_IDLE;
	UBMutex_Unlock(&uart->base.peri.mutex);
	return UBUR_OK;
}

UBUartResult UBUart_Close(UBUart* uart)
{
    if (uart == 0)
        return UBUR_NOT_CONFIGURED;

    if (uart->state == UBUS_NOT_CONFIGURED)
        return UBUR_NOT_CONFIGURED;

    if (uart->state == UBUS_BUSY)
        return UBUR_BUSY;

    if (!UBMutex_TryLock(&uart->base.peri.mutex))
        return UBUR_LOCKED;

    CloseHandle(uart->winHandle);
    uart->shouldTerminate = true;
    WaitForSingleObject(uart->thread, 1000);
    uart->state = UBUS_NOT_INITIALIZED;
    uart->winHandle = 0;
    UBMutex_Unlock(&uart->base.peri.mutex);
    return UBUR_OK;
}

UBUartResult UBUart_SetBaudrate(UBUart* uart, UBFrequency baudrate)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

    DCB dcbSerialParams = { 0 };
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    GetCommState(uart->winHandle, &dcbSerialParams);
    if (baudrate == 1200)
        dcbSerialParams.BaudRate = CBR_1200;
    else if (baudrate == 2400)
        dcbSerialParams.BaudRate = CBR_2400;
    else if (baudrate == 4800)
        dcbSerialParams.BaudRate = CBR_4800;
    else if (baudrate == 9600)
        dcbSerialParams.BaudRate = CBR_9600;
    else if (baudrate == 14400)
        dcbSerialParams.BaudRate = CBR_14400;
    else if (baudrate == 19200)
        dcbSerialParams.BaudRate = CBR_19200;
    else if (baudrate == 38400)
        dcbSerialParams.BaudRate = CBR_38400;
    else if (baudrate == 57600)
        dcbSerialParams.BaudRate = CBR_57600;
    else if (baudrate == 115200)
        dcbSerialParams.BaudRate = CBR_115200;
    else if (baudrate == 128000)
        dcbSerialParams.BaudRate = CBR_128000;
    else if (baudrate == 256000)
        dcbSerialParams.BaudRate = CBR_256000;
    else
    {
        UBMutex_Unlock(&uart->base.peri.mutex);
        return UBUR_NOT_SUPPORTED;
    }
    SetCommState(uart->winHandle, &dcbSerialParams);
    UBMutex_Unlock(&uart->base.peri.mutex);
	return UBUR_OK;
}

UBUartResult UBUart_SetMonitor(UBUart* uart, UBFifo* rxMonFifo, UBFifo* txMonFifo)
{
    if (uart == 0)
        return UBUR_NOT_CONFIGURED;
    if (!UBMutex_TryLock(&uart->base.peri.mutex))
        return UBUR_LOCKED;
    uart->base.rxMonFifo = rxMonFifo;
    uart->base.txMonFifo = txMonFifo;
    UBMutex_Unlock(&uart->base.peri.mutex);
    return UBUR_OK;
}

UBUartResult UBUart_Enqueue(UBUart* uart, const uint8_t* data, uint32_t size, uint32_t* enqueued)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

    uint32_t cnt = 0;

    if ((size) && (uart->base.txFifo))
		cnt = UBFifo_Put(uart->base.txFifo, data, size);

    if (enqueued)
        *enqueued = cnt;

    UBMutex_Unlock(&uart->base.peri.mutex);

	return UBUR_OK;
}

uint32_t UBUart_TxFifoAvailable(UBUart* uart)
{
	if (uart == 0)
		return 0;
	return UBFifo_GetAvailable(uart->base.txFifo);
}

UBUartResult UBUart_Transmit(UBUart* uart)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

    if (uart->base.txFifo)
    {
        OVERLAPPED writer;
        writer.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (writer.hEvent == NULL)
            return UBUR_ERROR;
        while(UBFifo_GetCount(uart->base.txFifo))
        {
            uint8_t data;
            if (UBFifo_GetByte(uart->base.txFifo, &data))
            {
                DWORD written = 0;
                if (uart->base.txMonFifo)
                    UBFifo_PutByte(uart->base.txMonFifo, data);
                if (!WriteFile(uart->winHandle, &data, 1, &written, &writer))
                {
                    if (GetLastError() != ERROR_IO_PENDING)
                    {
                        CloseHandle(writer.hEvent);
                        return UBUR_ERROR;
                    }
                    DWORD res = WaitForSingleObject(writer.hEvent, INFINITE);
                    switch(res)
                    {
                    case WAIT_OBJECT_0:
                        if (!GetOverlappedResult(uart->winHandle, &writer, &written, FALSE))
                        {
                            CloseHandle(writer.hEvent);
                            return UBUR_ERROR;
                        }
                        break;
                    case WAIT_TIMEOUT:
                        CloseHandle(writer.hEvent);
                        return UBUR_ERROR;
                    default:
                        CloseHandle(writer.hEvent);
                        return UBUR_ERROR;
                    }
                }
            }
        }
        CloseHandle(writer.hEvent);
    }

    UBMutex_Unlock(&uart->base.peri.mutex);

	return UBUR_OK;
}

#ifdef __cplusplus
}
#endif
