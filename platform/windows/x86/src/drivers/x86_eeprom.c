/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   x86_eeprom.c
 * \brief  microBeam SDK
 * \date   31.07.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/eeprom.h>
#include <ubeam/utils/intrinsics.h>
#include <string.h>
#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif

static void Open(UBEep* eep)
{
    eep->handle = CreateFileA(eep->fileName,
                       GENERIC_WRITE | GENERIC_READ,
                       0,
                       NULL,
                       OPEN_ALWAYS,
                       FILE_ATTRIBUTE_NORMAL,
                       NULL);
}

static void Close(UBEep* eep)
{
    CloseHandle(eep->handle);
}

void UBEep_Configure(UBEep* eep, const char* fileName, size_t memSize)
{
	if (eep == 0)
		return;
	eep->base.platform = 0;
    UBPeri_Configure(&eep->base.peri, 0);
	if (!UBMutex_TryLock(&eep->base.peri.mutex))
		return;
    strcpy(eep->fileName, fileName);
    Open(eep);
    if (eep->handle)
    {
        DWORD sz = GetFileSize(eep->handle, 0);
        if (sz != memSize)
        {
            LONG distance = (LONG)memSize;
            DWORD dwPtr = SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
            if (dwPtr == INVALID_SET_FILE_POINTER)
            {
                distance = 0;
                SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
                
            } 
            else
            {
                SetEndOfFile(eep->handle);   
            }
            distance = 0;
            SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
        }
        Close(eep);
        eep->memSize = memSize;
        eep->state = UBEEPS_NOT_INITIALIZED;
    }
    else
    {
        eep->state = UBEEPS_NOT_CONFIGURED;
    }  
    UBMutex_Unlock(&eep->base.peri.mutex);
}

UBEepResult UBEep_Init(UBEep* eep)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_BUSY)
        return UBEEPR_BUSY;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_LOCKED;

    eep->state = UBEEPS_IDLE;
    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}

UBEepResult UBEep_MassErase(UBEep* eep)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_INITIALIZED)
        return UBEEPR_NOT_INITIALIZED;

    if (eep->state != UBEEPS_IDLE)
        return UBEEPR_BUSY;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_BUSY;

    Open(eep);
    LONG distance = 0;
    SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
    for(size_t i = 0; i < eep->memSize; i++)
    {
        uint8_t data = 0;
        WriteFile(eep->handle, &data, 1, 0, 0);
    }
    SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
    Close(eep);
    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}

UBEepResult UBEep_Write(UBEep* eep, uint32_t offset, const uint8_t* data, size_t size, size_t* sizeWritten)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_INITIALIZED)
        return UBEEPR_NOT_INITIALIZED;

    if (eep->state != UBEEPS_IDLE)
        return UBEEPR_BUSY;

    if (sizeWritten)
        *sizeWritten = 0;

    if (offset >= eep->memSize)
        return UBEEPR_OK;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_BUSY;

    Open(eep);
    LONG distance = (LONG)offset;
    if (size + offset > eep->memSize)
    {
        size = eep->memSize - offset;
    }

    SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
    WriteFile(eep->handle, data, (DWORD)size, 0, 0);

    if (sizeWritten)
        *sizeWritten = size;

    SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
    Close(eep);
    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}

UBEepResult UBEep_Read(UBEep* eep, uint32_t offset, uint8_t* data, size_t size, size_t* sizeRead)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_INITIALIZED)
        return UBEEPR_NOT_INITIALIZED;

    if (eep->state != UBEEPS_IDLE)
        return UBEEPR_BUSY;

    if (sizeRead)
        *sizeRead = 0;

    if (offset >= eep->memSize)
        return UBEEPR_OK;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_BUSY;

    Open(eep);
    LONG distance = (LONG)offset;
    if (size + offset > eep->memSize)
    {
        size = eep->memSize - offset;
    }

    SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
    ReadFile(eep->handle, data, (DWORD)size, 0, 0);

    if (sizeRead)
        *sizeRead = size;

    SetFilePointer(eep->handle, distance, 0, FILE_BEGIN);
    Close(eep);
    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}


#ifdef __cplusplus
}
#endif
