/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   x86_gpio.c
 * \brief  microBeam SDK
 * \date   26.06.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/gpio.h>
#include <ubeam/x86/drivers/gpio.h>
#include <ubeam/utils/intrinsics.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define GPIO_PORT_COUNT             (4)

typedef struct
{
    UBGpioPinMask state;
    UBGpioPinMask output;
} GpioState;

static GpioState s_gpioState[GPIO_PORT_COUNT];

void UBGpioPort_Set(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return;
    s_gpioState[idx].state |= mask;
}

void UBGpioPort_Clear(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return;
    s_gpioState[idx].state &= ~mask;
}

void UBGpioPort_Invert(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return;
    s_gpioState[idx].state ^= mask;
}

void UBGpioPort_Write(UBGpioPort* port, UBGpioPinMask value)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return;
    s_gpioState[idx].state = value;
}

UBGpioPinMask UBGpioPort_Read(UBGpioPort* port)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return 0;
    return s_gpioState[idx].state;
}

bool UBGpioPort_IsSet(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return false;
    return (s_gpioState[idx].state & mask) == mask;
}

bool UBGpioPort_IsCleared(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return false;
    return (s_gpioState[idx].state & mask) == 0;
}

void UBGpioPort_Output(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return;
    s_gpioState[idx].output |= mask;
}  

void UBGpioPort_Input(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return;
    s_gpioState[idx].output &= ~mask;
}

bool UBGpioPort_IsOutput(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return false;
    return (s_gpioState[idx].output & mask) == mask;
}

bool UBGpioPort_IsInput(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t idx = ((uint64_t)port->base.handle) & 0xff;
    if (idx >= GPIO_PORT_COUNT)
        return false;
    return (s_gpioState[idx].output & mask) == 0;
}

bool UBGpioPort_SetFunction(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinFunc func)
{
    UBUnused(port);
    UBUnused(mask);
    UBUnused(func);
    return true;
}

void UBGpioPort_SetSpeed(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinSpeed speed)
{
    UBUnused(port);
    UBUnused(mask);
    UBUnused(speed);
}

void UBGpioPort_SetPullResistor(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinPull pull)
{
    UBUnused(port);
    UBUnused(mask);
    UBUnused(pull);
}

#ifdef __cplusplus
}
#endif
