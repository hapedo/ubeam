/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   x86_rtc.c
 * \brief  microBeam SDK
 * \date   31.7.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/rtc.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/utils/intrinsics.h>
#include <ubeam/utils/system.h>
#include <ubeam/os/thread.h>
#include <Windows.h>
#include <time.h>
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

void UBRtc_Configure(UBRtc* rtc, UBPeriHandle handle)
{
	if (rtc == 0)
		return;
	rtc->base.platform = 0;
    UBPeri_Configure(&rtc->base.peri, handle);
	if (!UBMutex_TryLock(&rtc->base.peri.mutex))
		return;
    rtc->state = UBRTCS_NOT_INITIALIZED;
    UBMutex_Unlock(&rtc->base.peri.mutex);
}

void UBRtc_EnableInt(UBRtc* rtc)
{
    UBUnused(rtc);
}

void UBRtc_DisableInt(UBRtc* rtc)
{
    UBUnused(rtc);
}

UBRtcState UBRtc_GetState(UBRtc* rtc)
{
	if (rtc == 0)
		return UBRTCS_NOT_CONFIGURED;
	return rtc->state;
}

UBRtcResult UBRtc_Init(UBRtc* rtc, bool startRtc)
{
    UBUnused(startRtc);
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->isRunning = true;
    rtc->relTimestamp = 0;
    memset(rtc->backupMem, 0, UBRTC_X86_BACKUP_MEM_SIZE);

    for(int i = 0; i < UBRTC_X86_ALARM_COUNT; i++)
    {
        rtc->alarmsEnabled[i] = false;
    }

    for(int i = 0; i < UBRTC_X86_TAMPER_COUNT; i++)
    {
        rtc->tamper[i].enabled = false;
        rtc->tamper[i].pin = 0;
    }

    rtc->state = UBRTCS_READY;
    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_Start(UBRtc* rtc)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->isRunning = true;
    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_Stop(UBRtc* rtc)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->isRunning = false;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_IsRunning(UBRtc* rtc, bool* isRunning)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (isRunning == 0)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    *isRunning = rtc->isRunning;
    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetTime(UBRtc* rtc, UBRtcDateTime* dateTime)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (dateTime == 0)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    time_t t;
    time(&t);
    TIME_ZONE_INFORMATION tz;
    GetTimeZoneInformation(&tz);
    t -= rtc->relTimestamp + tz.Bias * 60 + tz.DaylightBias * 60;

    UBRtcTimestamp ts = (UBTimestamp)((int32_t)t + rtc->relTimestamp);

    if (!UBRtc_FromTimestamp(ts, dateTime))
    {
        UBMutex_Unlock(&rtc->base.peri.mutex);
        return UBRTCR_ERROR;
    }

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_SetTime(UBRtc* rtc, const UBRtcDateTime* dateTime, bool setDate)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (dateTime == 0)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    time_t t;
    time(&t);
    TIME_ZONE_INFORMATION tz;
    GetTimeZoneInformation(&tz);
    t += tz.Bias * 60 + tz.DaylightBias * 60;
    struct tm w;
    _localtime64_s(&w, &t);

    UBRtcDateTime date = *dateTime;

    if (!setDate)
    {
        date.day = w.tm_mday;
        date.month = w.tm_mon;
        date.year = w.tm_year;
    }

    UBRtcTimestamp ts;
    if (!UBRtc_ToTimestamp(dateTime, &ts))
    {
        UBMutex_Unlock(&rtc->base.peri.mutex);
        return UBRTCR_BAD_PARAM;
    }

    rtc->relTimestamp = (int32_t)ts - (int32_t)t;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetMaxAlarms(UBRtc* rtc, uint8_t* count)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (count == 0)
        return UBRTCR_BAD_PARAM;

    *count = UBRTC_X86_ALARM_COUNT;

    return UBRTCR_OK;
}

UBRtcResult UBRtc_SetAlarm(UBRtc* rtc, uint8_t index, const UBRtcAlarm* alarm, bool enable)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if ((alarm == 0) || (index >= UBRTC_X86_ALARM_COUNT))
        return UBRTCR_BAD_PARAM;

    if (alarm->dayMaskEnabled)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->alarms[index] = *alarm;
    if (enable)
        rtc->alarmsEnabled[index] = true;
    else
        rtc->alarmsEnabled[index] = false;

    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetAlarm(UBRtc* rtc, uint8_t index, UBRtcAlarm* alarm)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if ((alarm == 0) || (index >= UBRTC_X86_ALARM_COUNT))
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    *alarm = rtc->alarms[index];

    return UBRTCR_OK;
}

UBRtcResult UBRtc_EnableAlarm(UBRtc* rtc, uint8_t index)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (index >= UBRTC_X86_ALARM_COUNT)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->alarmsEnabled[index] = true;

    return UBRTCR_OK;
}

UBRtcResult UBRtc_DisableAlarm(UBRtc* rtc, uint8_t index)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (index >= UBRTC_X86_ALARM_COUNT)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->alarmsEnabled[index] = false;

    return UBRTCR_OK;
}

UBRtcResult UBRtc_AlarmIsEnabled(UBRtc* rtc, uint8_t index, bool* isEnabled)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if ((isEnabled == 0) || (index >= UBRTC_X86_ALARM_COUNT))
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    *isEnabled = rtc->alarmsEnabled[index];

    return UBRTCR_OK;
}

UBRtcResult UBRtc_SleepAndWaitAlarm(UBRtc* rtc)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    UBRtcDateTime date;

    bool isFinished = false;
    while((!isFinished) || (UBThread_ShouldTerminateAll()))
    {
        for(int i = 0; i < UBRTC_X86_TAMPER_COUNT; i++)
        {
            if ((rtc->tamper[i].enabled) && (rtc->tamper[i].pin))
            {
                if ((rtc->tamper[i].activeHigh) && (UBGpioPin_IsSet(rtc->tamper[i].pin)))
                {
                    isFinished = true;
                    break;
                }
                else if ((!rtc->tamper[i].activeHigh) && (UBGpioPin_IsCleared(rtc->tamper[i].pin)))
                {
                    isFinished = true;
                    break;
                }
            }
        }
        for(int i = 0; i < UBRTC_X86_ALARM_COUNT; i++)
        {
            if (UBRtc_GetTime(rtc, &date) != UBRTCR_OK)
            {
                UBMutex_Unlock(&rtc->base.peri.mutex);
                return UBRTCR_ERROR;
            }
            if (rtc->alarmsEnabled[i])
            {
                if (rtc->alarms[i].dateEnabled)
                {
                    if ((date.day == rtc->alarms[i].day) &&
                        (date.month == rtc->alarms[i].month) &&
                        (date.hour == rtc->alarms[i].hour) &&
                        (date.minute == rtc->alarms[i].minute))
                    {
                        isFinished = true;
                        break;
                    }
                }
                else
                {
                    if ((date.hour == rtc->alarms[i].hour) &&
                        (date.minute == rtc->alarms[i].minute))
                    {
                        isFinished = true;
                        break;
                    }
                }
            }
            Sleep(100);
        }
    }
    UBMutex_Unlock(&rtc->base.peri.mutex);
    UBSystem_Reset();
    if (UBThread_ShouldTerminateAll())
        return UBRTCR_ERROR;
    return UBRTCR_OK;
}

UBRtcResult UBRtc_StoreBackup(UBRtc* rtc, uint32_t offset, const uint8_t* data, uint32_t size)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (data == 0)
        return UBRTCR_BAD_PARAM;

    if (offset >= UBRTC_X86_BACKUP_MEM_SIZE)
        return UBRTCR_OK;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    if (size + offset > UBRTC_X86_BACKUP_MEM_SIZE)
        size = UBRTC_X86_BACKUP_MEM_SIZE - offset;

    memcpy(rtc->backupMem + offset, data, size);

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_ReadBackup(UBRtc* rtc, uint32_t offset, uint8_t* data, uint32_t size)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (data == 0)
        return UBRTCR_BAD_PARAM;

    if (offset >= UBRTC_X86_BACKUP_MEM_SIZE)
        return UBRTCR_OK;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    if (size + offset > UBRTC_X86_BACKUP_MEM_SIZE)
        size = UBRTC_X86_BACKUP_MEM_SIZE - offset;

    memcpy(data, rtc->backupMem + offset, size);

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetBackupSize(UBRtc* rtc, uint32_t* size)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (size == 0)
        return UBRTCR_BAD_PARAM;

    *size = UBRTC_X86_BACKUP_MEM_SIZE;
    return UBRTCR_OK;
}

UBRtcResult UBRtc_SetTamperPin(UBRtc* rtc, UBRtcTamper tamper, UBGpioPin* pin)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (tamper >= UBRTC_X86_TAMPER_COUNT)
        return  UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->tamper[tamper].pin = pin;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_ConfigureTamperPin(UBRtc* rtc, UBRtcTamper tamper, bool activeHigh)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (tamper >= UBRTC_X86_TAMPER_COUNT)
        return  UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->tamper[tamper].activeHigh = activeHigh;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_EnableTamperPin(UBRtc* rtc, UBRtcTamper tamper)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (tamper >= UBRTC_X86_TAMPER_COUNT)
        return  UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->tamper[tamper].enabled = true;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_DisableTamperPin(UBRtc* rtc, UBRtcTamper tamper)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (tamper >= UBRTC_X86_TAMPER_COUNT)
        return  UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    rtc->tamper[tamper].enabled = false;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

bool UBRtc_IsTamperDetected(UBRtc* rtc, UBRtcTamper tamper, bool clearFlag)
{
    UBUnused(clearFlag);
    if (rtc == 0)
        return false;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return false;

    if (tamper >= UBRTC_X86_TAMPER_COUNT)
        return false;

    return rtc->tamper[tamper].enabled;
}

#ifdef __cplusplus
}
#endif
