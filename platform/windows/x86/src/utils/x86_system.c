/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   x86_system.c
 * \brief  microBeam SDK
 * \date   03.08.2020
 * \note
 * \todo
 */
#include <windows.h>
#include <ubeam/x86/utils/system.h>
#include <ubeam/os/thread.h>

#ifdef __cplusplus
extern "C"
{
#endif

static SystemResetCallback* s_resetCallback = 0;

void UBSystem_Init()
{
    s_resetCallback = 0;
}

void UBSystem_Reset()
{
    if (s_resetCallback)
        s_resetCallback();
    UBThread_TerminateAll();
}

void UBSystem_SetResetCallback(SystemResetCallback* callback)
{
    s_resetCallback = callback;
}

#ifdef __cplusplus
}
#endif
