/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   none_thread.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <windows.h>
#include <ubeam/os/thread.h>
#include <ubeam/utils/intrinsics.h>

#ifdef __cplusplus
extern "C"
{
#endif

static UBThreadId s_currentThreadId = 0;
static UBThreadId s_threadIdCounter = 0;
static UBThreadId s_threadCount = 0;
static UBThreadId s_threadIndex = 0;
static UBThread* s_threads[UBEAM_MAX_THREAD_COUNT];
static bool s_terminateAllFlag = false;

UBThread* GetCurrent()
{
	if (s_threadIndex < s_threadCount)
		return s_threads[s_threadIndex];
	else
		return 0;
}

UBThreadId UBThread_GetCurrentId()
{
	return s_currentThreadId;
}

const UBThread* UBThread_GetCurrent()
{
	return GetCurrent();
}

static void ExecuteNextThread()
{
	if ((s_threadIndex < s_threadCount) && (s_threads[s_threadIndex]))
	{
		UBThread* thr = s_threads[s_threadIndex];
		if (!thr->platform.blocked)
		{
			if ((thr->platform.sleepTimestamp) && (UBSysTime_HasPassed(thr->platform.sleepTimestamp)))
				thr->platform.sleepTimestamp = 0;
			if (((thr->state == UBTHRS_RUNNING) || (thr->state == UBTHRS_STOPPING) || (thr->state == UBTHRS_SUSPENDING))
					&& (thr->platform.sleepTimestamp == 0))
				((UBThreadExecuteCb*)thr->execCb)(thr);
			switch(thr->state)
			{
			case UBTHRS_STOPPING:
				thr->state = UBTHRS_FINISHED;
				break;
			case UBTHRS_SUSPENDING:
				thr->state = UBTHRS_SUSPENDED;
				break;
			default:
				break;
			}
		}
	}
}

void UBThread_Init()
{
    s_currentThreadId = 0;
    s_threadIdCounter = 0;
    s_threadCount = 0;
    s_threadIndex = 0;
    s_terminateAllFlag = false;
}

void UBThread_StartScheduler()
{
	while(!s_terminateAllFlag)
	{
		ExecuteNextThread();
        SwitchToThread();
		s_threadIndex++;
		if (s_threadIndex >= s_threadCount)
			s_threadIndex = 0;
	}
}

void UBThread_WaitForAllThreads()
{
	// This function makes no sense since StartScheduler() contains infinite loop
}

UBThreadState UBThread_GetState(const UBThread* thread)
{
	if (thread == 0)
		return UBTHRS_NOT_CONFIGURED;
	return thread->state;
}

void UBThread_Configure(UBThread* thread, UBThreadOnInitCb* initCb, UBThreadOnStartCb* startCb, UBThreadOnStopCb* stopCb, UBThreadExecuteCb* exec)
{
	if (thread == 0)
		return;
	UBAssert(s_threadCount < UBEAM_MAX_THREAD_COUNT);
	thread->onInitCb = initCb;
	thread->onStartCb = startCb;
	thread->onStopCb = stopCb;
	thread->execCb = exec;
	thread->id = s_threadIdCounter++;
	thread->state = UBTHRS_STOPPED;
	thread->platform.sleepTimestamp = 0;
	thread->platform.blocked = false;
	s_threads[s_threadCount++] = thread;
	if (thread->onInitCb)
		((UBThreadOnInitCb*)thread->onInitCb)(thread);
}

void UBThread_SetCustomData(UBThread* thread, void* customData)
{
	if (thread == 0)
		return;
	thread->customData = customData;
}

void* UBThread_GetCustomData(const UBThread* thread)
{
	if (thread == 0)
		return 0;
	return thread->customData;
}

void UBThread_Start(UBThread* thread)
{
	if (thread == 0)
		return;
	if ((thread->state == UBTHRS_STOPPED) || (thread->state == UBTHRS_FINISHED))
	{
		thread->state = UBTHRS_STARTING;
		if (thread->onStartCb)
			((UBThreadOnStartCb*)thread->onStartCb)(thread);
		thread->state = UBTHRS_RUNNING;
	}
}

void UBThread_Stop(UBThread* thread)
{
	if (thread == 0)
		return;
	if ((thread->state == UBTHRS_RUNNING) || (thread->state == UBTHRS_SUSPENDED))
	{
		thread->state = UBTHRS_STOPPING;
		if (thread->onStopCb)
			((UBThreadOnStopCb*)thread->onStopCb)(thread);
	}
}

void UBThread_Suspend(UBThread* thread)
{
	if (thread == 0)
		return;
	if (thread->state == UBTHRS_RUNNING)
	{
		thread->state = UBTHRS_SUSPENDING;
	}
}

void UBThread_Sleep(UBThread* thread, UBTimeInterval interval)
{
	if (thread == 0)
		return;
	thread->platform.sleepTimestamp = UBSysTime_GetTimestamp() + interval;
}

void UBThread_SleepMilli(UBThread* thread, uint32_t milli)
{
	if (thread == 0)
		return;
	thread->platform.sleepTimestamp = UBSysTime_GetFutureMilli(milli);
}

bool UBThread_ShouldTerminate(const UBThread* thread)
{
	if (thread == 0)
		return false;
	return thread->state == UBTHRS_STOPPING;
}

bool UBThread_ShouldSuspend(const UBThread* thread)
{
	if (thread == 0)
		return false;
	return thread->state == UBTHRS_SUSPENDING;
}

void UBThread_Yield()
{
	UBThread* thr = GetCurrent();
	bool wasBlocked = false;
	if (thr)
	{
		wasBlocked = thr->platform.blocked;
		thr->platform.blocked = true;
	}
	UBThreadId origId = s_threadIndex;
	do
	{
		s_threadIndex++;
		if (s_threadIndex >= s_threadCount)
			s_threadIndex = 0;
		ExecuteNextThread();
	}
	while (s_threadIndex != origId);
	if (thr)
		thr->platform.blocked = wasBlocked;
}

void UBThread_TerminateAll()
{
    s_terminateAllFlag = true;
}

bool UBThread_ShouldTerminateAll()
{
    return s_terminateAllFlag;
}

#ifdef __cplusplus
}
#endif
