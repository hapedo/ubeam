/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   none_mutex.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/none/os/mutex.h>
#include <ubeam/os/thread.h>
#include <ubeam/utils/intrinsics.h>
#include <ubeam/drivers/interrupt_ctrl.h>

#ifdef __cplusplus
extern "C"
{
#endif

void UBMutex_Configure(UBMutex* mutex, bool isRecursive, bool lock)
{
	mutex->platform.count = 0;
	if (lock)
		mutex->platform.count = 1;
	mutex->isRecursive = isRecursive;
	mutex->platform.owner = UBThread_GetCurrentId();
}

void UBMutex_Lock(UBMutex* mutex)
{
	UBAssert(mutex != 0);
	if (mutex->isRecursive)
		UBAssert(!mutex->platform.count == 0);
	UBThreadId thread = UBThread_GetCurrentId();
	if (mutex->platform.count == 0)
		UBAssert(mutex->platform.owner == thread);
	bool wasEnabled = UBInt_DisableGlobal();
	mutex->platform.count++;
	mutex->platform.owner = thread;
	if (wasEnabled)
		UBInt_EnableGlobal();
}

bool UBMutex_TryLock(UBMutex* mutex)
{
	UBAssert(mutex != 0);
	UBThreadId thread = UBThread_GetCurrentId();
	if ((!mutex->isRecursive) && (mutex->platform.owner == thread) && (mutex->platform.count))
		UBAssert(0);

	if ((mutex->platform.count) && (mutex->platform.owner != thread))
		return false;
	bool wasEnabled = UBInt_DisableGlobal();
	mutex->platform.count++;
	mutex->platform.owner = thread;
	if (wasEnabled)
		UBInt_EnableGlobal();
	return true;
}

void UBMutex_Unlock(UBMutex* mutex)
{
	UBAssert(mutex != 0);
	UBAssert(mutex->platform.count);
	bool wasEnabled = UBInt_DisableGlobal();
	UBThreadId thread = UBThread_GetCurrentId();
	if (mutex->platform.count == 0)
		UBAssert(mutex->platform.owner == thread);
	mutex->platform.count--;
	if (mutex->platform.count == 0)
		mutex->platform.owner = 0;
	if (wasEnabled)
		UBInt_EnableGlobal();
}

#ifdef __cplusplus
}
#endif
