/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   definitions.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_STM32F0_DRIVERS_DEFINITIONS_H_
#define _UBEAM_STM32F0_DRIVERS_DEFINITIONS_H_

#include <ubeam/setup.h>

#ifndef UBEAM_CPU_MODEL
    #error "Error: CPU model not defined!"
#endif

#if ((UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x4) || (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x6) || (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x8))
    #define STM32F030
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030xC)
    #define STM32F030xC
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F031)
    #define STM32F031
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F042)
    #define STM32F042
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F051)
    #define STM32F051
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F070x6)
    #define STM32F070x6
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F070xB)
    #define STM32F070xB
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F072)
    #define STM32F072
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F091)
    #define STM32F091
#endif

#include <ubeam/stm32f0/3rd_party/ST/stm32f0xx.h>

/*! @} */

#endif // _UBEAM_STM32F0_DRIVERS_DEFINITIONS_H_
