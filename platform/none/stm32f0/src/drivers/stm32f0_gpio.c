/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32f0_gpio.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/gpio.h>
#include <ubeam/stm32f0/drivers/definitions.h>

#ifdef __cplusplus
extern "C"
{
#endif

//! Pin output mode setting
#define __GMODE_INPUT                                           (0x00)
//! Pin output mode setting
#define __GMODE_OUTPUT                                          (0x01)

//! Pin speed low
#define __GSPEED_LOW                                            (0x00)
//! Pin speed medium
#define __GSPEED_MEDIUM                                         (0x01)
//! Pin speed high
#define __GSPEED_HIGH                                           (0x03)

//! Pin speed low
#define __GPULL_NONE                                            (0x00)
//! Pin speed medium
#define __GPULL_UP                                              (0x01)
//! Pin speed high
#define __GPULL_DOWN                                            (0x02)

//! Alternate functions count per pin (maximal)
#define __GPIO_FUNCTIONS_PER_PIN                                (7)
#define __GPIO_PINS_PER_PORT                                    (16)
#define __GPIO_PORT_COUNT                                       (2)

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x8)
#define __GPIO_PORT_COUNT                                       (2)
static const uint8_t functionMap[__GPIO_PORT_COUNT * __GPIO_PINS_PER_PORT][__GPIO_FUNCTIONS_PER_PIN] =
  {
    // Port A
    {UBGPF_UNDEFINED,    UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_EVENT,        UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_TIMER_15,     UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_TIMER_15,     UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_TIMER_14,    UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_UNDEFINED,  UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_TIMER_3,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_TIMER_16,      UBGPF_EVENT     },
    {UBGPF_SPI_1,        UBGPF_TIMER_3,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_TIMER_14,    UBGPF_TIMER_17,      UBGPF_EVENT     },
    {UBGPF_CLOCK,        UBGPF_USART_1,    UBGPF_TIMER_1,     UBGPF_EVENT,       UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_TIMER_15,     UBGPF_USART_1,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_TIMER_17,     UBGPF_USART_1,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_EVENT,        UBGPF_USART_1,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_I2C_1,         UBGPF_UNDEFINED },
    {UBGPF_EVENT,        UBGPF_USART_1,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_I2C_1,         UBGPF_UNDEFINED },
    {UBGPF_SWD,          UBGPF_IR,         UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SWD,          UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_USART_2,    UBGPF_UNDEFINED,   UBGPF_EVENT,       UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    // Port B
    {UBGPF_EVENT,        UBGPF_TIMER_3,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_TIMER_14,     UBGPF_TIMER_3,    UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_UNDEFINED,    UBGPF_UNDEFINED,  UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_EVENT,      UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_TIMER_3,    UBGPF_EVENT,       UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_1,        UBGPF_TIMER_3,    UBGPF_TIMER_16,    UBGPF_I2C_1,       UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_USART_1,      UBGPF_I2C_1,      UBGPF_TIMER_16,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_USART_1,      UBGPF_I2C_1,      UBGPF_TIMER_17,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_UNDEFINED,    UBGPF_I2C_1,      UBGPF_TIMER_16,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_IR,           UBGPF_I2C_1,      UBGPF_TIMER_17,    UBGPF_EVENT,       UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_UNDEFINED,    UBGPF_I2C_2,      UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_EVENT,        UBGPF_I2C_2,      UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_2,        UBGPF_EVENT,      UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_2,        UBGPF_UNDEFINED,  UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_2,        UBGPF_TIMER_15,   UBGPF_TIMER_1,     UBGPF_UNDEFINED,   UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
    {UBGPF_SPI_2,        UBGPF_TIMER_15,   UBGPF_TIMER_1,     UBGPF_TIMER_15,    UBGPF_UNDEFINED,   UBGPF_UNDEFINED,     UBGPF_UNDEFINED },
  };
#endif

static uint32_t GetGpioMask(uint32_t mask, uint32_t function)
{
    uint32_t temp = 0;
    mask &= 0xffff;
    while(mask)
    {
        if (mask & 0x01)
            temp |= function;
        mask >>= 1;
        function <<= 2;
    }
    return temp;
}

static int GetGpioPortIndex(const UBPeriHandle handle)
{
#if defined(GPIOA_BASE)
    if (handle == (UBPeriHandle)GPIOA_BASE)
        return 0;
#endif
#if defined(GPIOB_BASE)
    if (handle == (UBPeriHandle)GPIOB_BASE)
        return 1;
#endif
#if defined(GPIOC_BASE)
    if (handle == (UBPeriHandle)GPIOC_BASE)
        return 2;
#endif
#if defined(GPIOD_BASE)
    if (handle == (UBPeriHandle)GPIOD_BASE)
        return 3;
#endif
#if defined(GPIOE_BASE)
    if (handle == (UBPeriHandle)GPIOE_BASE)
        return 4;
#endif
#if defined(GPIOF_BASE)
    if (handle == (UBPeriHandle)GPIOF_BASE)
        return 5;
#endif
#if defined(GPIOG_BASE)
    if (handle == (UBPeriHandle)GPIOG_BASE)
        return 6;
#endif
#if defined(GPIOH_BASE)
    if (handle == (UBPeriHandle)GPIOH_BASE)
        return 7;
#endif
#if defined(GPIOI_BASE)
    if (handle == (UBPeriHandle)GPIOI_BASE)
        return 8;
#endif
#if defined(GPIOJ_BASE)
    if (handle == (UBPeriHandle)GPIOJ_BASE)
        return 9;
#endif
    return -1;
}

void UBGpioPort_Set(UBGpioPort* port, UBGpioPinMask mask)
{
    ((GPIO_TypeDef*)port->base.handle)->BSRR = mask & 0xffff;
}

void UBGpioPort_Clear(UBGpioPort* port, UBGpioPinMask mask)
{
    ((GPIO_TypeDef*)port->base.handle)->BSRR = (mask & 0xffff) << 16;
}

void UBGpioPort_Invert(UBGpioPort* port, UBGpioPinMask mask)
{
    ((GPIO_TypeDef*)port->base.handle)->ODR ^= mask;
}

void UBGpioPort_Write(UBGpioPort* port, UBGpioPinMask value)
{
    value = (value & 0xffff) | ((~value & 0xffff) << 16);
    ((GPIO_TypeDef*)port->base.handle)->BSRR = value;
}

UBGpioPinMask UBGpioPort_Read(UBGpioPort* port)
{
    return ((GPIO_TypeDef*)port->base.handle)->IDR;
}

bool UBGpioPort_IsSet(UBGpioPort* port, UBGpioPinMask mask)
{
    return (((GPIO_TypeDef*)port->base.handle)->IDR & mask) == mask;
}

bool UBGpioPort_IsCleared(UBGpioPort* port, UBGpioPinMask mask)
{
    return (((GPIO_TypeDef*)port->base.handle)->IDR & mask) == 0;
}

void UBGpioPort_Output(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t temp = ((GPIO_TypeDef*)port->base.handle)->MODER & ~GetGpioMask(mask, 0x03);
    ((GPIO_TypeDef*)port->base.handle)->MODER = GetGpioMask(mask, __GMODE_OUTPUT) | temp;
}  

void UBGpioPort_Input(UBGpioPort* port, UBGpioPinMask mask)
{
    uint32_t temp = ((GPIO_TypeDef*)port->base.handle)->MODER & ~GetGpioMask(mask, 0x03);
    ((GPIO_TypeDef*)port->base.handle)->MODER = temp;
}

bool UBGpioPort_IsOutput(UBGpioPort* port, UBGpioPinMask mask)
{
    return (((GPIO_TypeDef*)port->base.handle)->MODER & GetGpioMask(mask, 0x03)) == GetGpioMask(mask, __GMODE_OUTPUT);
}

bool UBGpioPort_IsInput(UBGpioPort* port, UBGpioPinMask mask)
{
    return (((GPIO_TypeDef*)port->base.handle)->MODER & GetGpioMask(mask, 0x03)) == 0;
}

bool UBGpioPort_SetFunction(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinFunc func)
{
    int portIndex = GetGpioPortIndex(port->base.handle);
    if (portIndex < 0)
        return false;

    GPIO_TypeDef* gpio = (GPIO_TypeDef*)port->base.handle;
    UBGpioPinMask pinMask = mask;
      
    int pinIndex = 0;
    while(pinMask)
    {
        if (pinMask & 0x01)
        {
            // Set mode register to GPIO input
            gpio->MODER &= ~(0x03 << (pinIndex * 2));

            if (((func >= UBGPF_ADC_1) && (func <= UBGPF_ADC_3)) ||
                ((func >= UBGPF_DAC_1) && (func <= UBGPF_DAC_2)))
            {
                // Set analog mode
                gpio->MODER |= 0x03 << (pinIndex * 2);
            }
            else if (func != UBGPF_GPIO)
            {
                // Set mode register to alternate function mode
                gpio->MODER |= 0x02 << (pinIndex * 2);
    
				int functionIndex = -1;
				for(unsigned int i = 0; i < __GPIO_FUNCTIONS_PER_PIN; i++)
				{
					if (functionMap[portIndex * __GPIO_PINS_PER_PORT + pinIndex][i] == func)
					{
						functionIndex = i;
					}
				}
				if (functionIndex == -1)
					return false;

				// Set function
				if (pinIndex < 8)
				{
					gpio->AFR[0] &= ~(0x0f << (pinIndex << 2));
					gpio->AFR[0] |= functionIndex << (pinIndex << 2);
				}
				else
				{
					gpio->AFR[1] &= ~(0x0f << ((pinIndex - 8) << 2));
					gpio->AFR[1] |= functionIndex << ((pinIndex - 8) << 2);
				}
				break;
            }
        }
        pinMask >>= 1;
        pinIndex++;
    }

    return true;
}

void UBGpioPort_SetSpeed(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinSpeed speed)
{
    uint32_t value;
    if (speed == UBGPS_LOW)
        value = __GSPEED_LOW;
    else if (speed == UBGPS_MEDIUM)
        value = __GSPEED_MEDIUM;
    else
        value = __GSPEED_HIGH;
    uint32_t temp = ((GPIO_TypeDef*)port->base.handle)->OSPEEDR & ~GetGpioMask(mask, 0x03);
    ((GPIO_TypeDef*)port->base.handle)->OSPEEDR = GetGpioMask(mask, value) | temp;
}

void UBGpioPort_SetPullResistor(UBGpioPort* port, UBGpioPinMask mask, UBGpioPinPull pull)
{
    uint32_t value;
    if (pull == UBGPP_NONE)
        value = __GPULL_NONE;
    else if (pull == UBGPP_UP)
        value = __GPULL_UP;
    else
        value = __GPULL_DOWN;
    uint32_t temp = ((GPIO_TypeDef*)port->base.handle)->PUPDR & ~GetGpioMask(mask, 0x03);
    ((GPIO_TypeDef*)port->base.handle)->PUPDR = GetGpioMask(mask, value) | temp;
}

#ifdef __cplusplus
}
#endif


