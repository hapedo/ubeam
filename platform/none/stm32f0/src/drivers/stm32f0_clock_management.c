/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32f0_clock_management.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/stm32f0/drivers/clock_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32F030x8)
static const uint32_t clockDescMap[][4] = {
    { GPIOF_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOFEN,    UBCLKSRC_HCLK},
    { GPIOE_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOEEN,    UBCLKSRC_HCLK},
    { GPIOD_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIODEN,    UBCLKSRC_HCLK},
    { GPIOC_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOCEN,    UBCLKSRC_HCLK},
    { GPIOB_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOBEN,    UBCLKSRC_HCLK},
    { GPIOA_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOAEN,    UBCLKSRC_HCLK},
    { CRC_BASE      , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_CRCEN,      UBCLKSRC_HCLK},
    { SRAM_BASE     , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_SRAMEN,     UBCLKSRC_HCLK},
    { DMA1_BASE     , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_DMAEN,      UBCLKSRC_HCLK},
    { DMA2_BASE     , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_DMAEN,      UBCLKSRC_HCLK},
    { DBGMCU_BASE   , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_DBGMCUEN,  UBCLKSRC_PCLK},
    { TIM17_BASE    , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM17EN,   UBCLKSRC_TIM},
    { TIM16_BASE    , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM16EN,   UBCLKSRC_TIM},
    { TIM15_BASE    , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM15EN,   UBCLKSRC_TIM},
    { USART1_BASE   , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_USART1EN,  UBCLKSRC_USART1},
    { SPI1_BASE     , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_SPI1EN,    UBCLKSRC_PCLK},
    { TIM1_BASE     , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM1EN,    UBCLKSRC_TIM},
    { ADC_BASE      , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_ADCEN,     UBCLKSRC_ADC},
    { USART6_BASE   , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_USART6EN,  UBCLKSRC_PCLK},
    { SYSCFG_BASE   , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_SYSCFGEN,  UBCLKSRC_PCLK},
    { PWR_BASE      , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_PWREN,     UBCLKSRC_PCLK},
    { I2C2_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_I2C2EN,    UBCLKSRC_PCLK},
    { I2C1_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_I2C1EN,    UBCLKSRC_I2C1},
    { USART5_BASE   , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_USART5EN,  UBCLKSRC_PCLK},
    { USART4_BASE   , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_USART4EN,  UBCLKSRC_PCLK},
    { USART3_BASE   , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_USART3EN,  UBCLKSRC_PCLK},
    { USART2_BASE   , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_USART2EN,  UBCLKSRC_PCLK},
    { SPI2_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_SPI2EN,    UBCLKSRC_PCLK},
    { WWDG_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_WWDGEN,    UBCLKSRC_LSI},
    { TIM14_BASE    , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM14EN,   UBCLKSRC_TIM},
    { TIM7_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM7EN,    UBCLKSRC_TIM},
    { TIM6_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM6EN,    UBCLKSRC_TIM},
    { TIM3_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM3EN,    UBCLKSRC_TIM},
    { RTC_BASE      , 0                            , 0                 ,    UBCLKSRC_RTC},
    { FLASH_BASE    , 0                            , 0                 ,    UBCLKSRC_FLASH},
    { SysTick_BASE  , 0                            , 0                 ,    UBCLKSRC_SYSTIMER},
    { 0             , 0                            , 0                 ,    UBCLKSRC_UNKNOWN}
};
#endif

static UBFrequency s_hseOscFrequency = 0;

static int GetClockDescriptor(UBPeriHandle handle)
{
    int i = 0;

    while(clockDescMap[i][0] != 0)
    {
        if (clockDescMap[i][0] == (uint32_t)handle)
            return i;
        i++;
    }
    return 0;
}

UBFrequency UBClock_GetInputFrequency(UBPeriHandle handle)
{
	int index = GetClockDescriptor(handle);
	if (index != -1)
		return UBClock_GetClockSourceFrequency(clockDescMap[index][3]);
	else
		return 0;
}

UBClockSource UBClock_GetClockSource(UBPeriHandle handle)
{
    int index = GetClockDescriptor(handle);
    if (index != -1)
        return clockDescMap[index][3];
    else
        return UBCLKSRC_UNKNOWN;
}

bool UBClock_EnableClockSource(UBClockSource clkSource)
{
    switch(clkSource)
    {
    case UBCLKSRC_HSI:
        RCC->CR |= 0x00000001;
        break;
    case UBCLKSRC_HSE:
        RCC->CR |= 0x00080000;
        RCC->CR |= 0x00010000;
        break;
    case UBCLKSRC_PLL:
        RCC->CR |= 0x01000000;
        break;
    default:
        return -1;
    }
    return 0;
}

bool UBClock_DisableClockSource(UBClockSource clkSource)
{
    switch(clkSource)
    {
    case UBCLKSRC_HSI:
        RCC->CR &= ~0x00000001;
        while(RCC->CR & 0x00000002);
        break;
    case UBCLKSRC_HSE:
        RCC->CR &= ~0x00010000;
        while(RCC->CR & 0x00020000);
        break;
    case UBCLKSRC_PLL:
        RCC->CR &= ~0x01000000;
        while(RCC->CR & 0x02000000);
        break;
    default:
        return -1;
    }
    return 0;
}

bool UBClock_WaitClockSourceReady(UBClockSource clkSource)
{
    switch(clkSource)
    {
    case UBCLKSRC_HSI:
        while((RCC->CR & 0x00000002) == 0);
        break;
    case UBCLKSRC_HSE:
        while((RCC->CR & 0x00020000) == 0);
        break;
    case UBCLKSRC_PLL:
        while((RCC->CR & 0x02000000) == 0);
        break;
    default:
        return -1;
    }
    return 0;
}

UBFrequency UBClock_GetClockSourceFrequency(UBClockSource clkSource)
{
    uint32_t frequency = 0;
    switch(clkSource)
    {

    case UBCLKSRC_LSE:
        frequency = 32768;
        break;

    case UBCLKSRC_LSI:
        frequency = 40000;
        break;

    case UBCLKSRC_FLASH:
    case UBCLKSRC_HSI:
        frequency = 8000000;
        break;

    case UBCLKSRC_HSE:
        frequency = s_hseOscFrequency;
        break;

    case UBCLKSRC_PLL:
        if (RCC->CFGR & 0x00010000)
            frequency = s_hseOscFrequency / ((RCC->CFGR2 & 0x0000000f) + 1);
        else
            frequency = 4000000;
        frequency *= ((RCC->CFGR >> 18) & 0x0000000f) + 2;
        break;

    case UBCLKSRC_SYSCLK:
        switch(RCC->CFGR & 0x03)
        {
        case 0x00:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSI);
            break;
        case 0x01:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSE);
            break;
        case 0x02:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PLL);
            break;
        }
        break;

    case UBCLKSRC_HCLK:
    {
        uint32_t div = 1;
        if (RCC->CFGR & 0x00000080) {
            uint32_t pre = (RCC->CFGR >> 4) & 0x07;
            if (pre > 3)
                div = 1 << (pre + 2);
            else
                div = 1 << (pre + 1);
        }
        frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_SYSCLK) / div;
        break;
    }

    case UBCLKSRC_PCLK:
        frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HCLK);
        if (RCC->CFGR & 0x00000400)
            frequency >>= (RCC->CFGR >> 8) + 1;
        break;

    case UBCLKSRC_SYSTIMER:
        if (SysTick->CTRL & SysTick_CTRL_CLKSOURCE_Msk)
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK);
        else
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK) >> 3;
        break;

    case UBCLKSRC_I2C1:
        if (RCC->CFGR3 & 0x00000010)
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_SYSCLK);
        else
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSI);
        break;

    case UBCLKSRC_TIM:
        if (RCC->CFGR & 0x00000400)
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK) << 1;
        else
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK);
        break;

    case UBCLKSRC_ADC:
        switch(ADC1->CFGR2 >> 30)
        {
        case 0x00:
            frequency = 14000000;
            break;
        case 0x01:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK) >> 1;
            break;
        case 0x02:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK) >> 2;
            break;
        }
        break;

    case UBCLKSRC_USART1:
        switch(RCC->CFGR3 & 0x03)
        {
        case 0x00:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK);
            break;
        case 0x01:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_SYSCLK);
            break;
        case 0x02:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_LSE);
            break;
        case 0x03:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSI);
            break;
        }
        break;

    case UBCLKSRC_RTC:
        switch ((RCC->BDCR >> 8) & 0x03)
        {
        case 0x00:
            frequency = 0;
            break;
        case 0x01:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_LSE);
            break;
        case 0x02:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_LSI);
            break;
        case 0x03:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSE) >> 5;
            break;
        }
        break;

    case UBCLKSRC_MCO:
        break;

    case UBCLKSRC_UNKNOWN:
    default:
        break;
    }
    return frequency;
}

bool UBClock_SetSysClockSource(UBClockSource clkSource, bool disableCurrSource)
{
    if ((clkSource != UBCLKSRC_HSI) && (clkSource != UBCLKSRC_HSE) && (clkSource != UBCLKSRC_PLL))
        return -1;

    UBClockSource currSource = UBClock_GetSysClockSource();

    if (currSource != clkSource)
    {
    	UBClock_EnableClockSource(clkSource);
    	UBClock_WaitClockSourceReady(clkSource);
        if (UBClock_GetClockSourceFrequency(clkSource) > 24000000)
            FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;
        else
            FLASH->ACR = FLASH_ACR_PRFTBE;

        switch(clkSource)
        {
        case UBCLKSRC_HSI:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc);
            break;
        case UBCLKSRC_HSE:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc) | 0x01;
            break;
        case UBCLKSRC_PLL:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc) | 0x02;
            break;
        default:
            return false;
        }
        if (disableCurrSource)
        	UBClock_DisableClockSource(currSource);
    }
    return true;
}

UBClockSource UBClock_GetSysClockSource()
{
    switch(RCC->CFGR & 0x03)
    {
    case 0x00:
        return UBCLKSRC_HSI;
    case 0x01:
        return UBCLKSRC_HSE;
    case 0x02:
        return UBCLKSRC_PLL;
    default:
        return UBCLKSRC_UNKNOWN;
    }
}

bool UBClock_SetPllConfig(UBClockSource pllSource, uint8_t pllSourceDivisor, uint8_t pllMultiply)
{
    if ((pllMultiply < 2) || (pllMultiply > 16))
        return false;

    if ((pllSource != UBCLKSRC_HSI) && (pllSource != UBCLKSRC_HSE))
        return false;

    if ((pllSource == UBCLKSRC_HSI) && (pllSourceDivisor != 1))
        return false;
    else if ((pllSourceDivisor < 1) || (pllSourceDivisor > 16))
        return false;

    // Cannot change PLL if sysclock source is PLL
    if (UBClock_GetSysClockSource() == UBCLKSRC_PLL)
        return false;

    if (UBClock_EnableClockSource(pllSource) != 0)
        return false;

    if (UBClock_WaitClockSourceReady(pllSource) != 0)
        return false;

    if (UBClock_DisableClockSource(UBCLKSRC_PLL) != 0)
        return false;

    RCC->CFGR2 = pllSourceDivisor - 1;

    RCC->CFGR = (RCC->CFGR & 0xffc3ffff) | ((pllMultiply - 2) << 18);

    if (pllSource == UBCLKSRC_HSI)
        RCC->CFGR &= ~0x00010000;
    else
        RCC->CFGR |= 0x00010000;

    if (UBClock_EnableClockSource(UBCLKSRC_PLL) != 0)
        return false;

    if (UBClock_WaitClockSourceReady(UBCLKSRC_PLL) != 0)
        return false;

    return true;
}

void UBClock_SetHseOscillatorFrequency(uint32_t frequency)
{
    s_hseOscFrequency = frequency;
}


bool UBClock_EnableClock(UBPeriHandle handle)
{
    int index = GetClockDescriptor(handle);
    if ((index != -1) && (clockDescMap[index][1]))
    {
        *(uint32_t*)clockDescMap[index][1] |= clockDescMap[index][2];
        return true;
    }
    return false;
}

bool UBClock_DisableClock(UBPeriHandle handle)
{
    int index = GetClockDescriptor(handle);
    if ((index != -1) && (clockDescMap[index][1]))
    {
        *(uint32_t*)clockDescMap[index][1] &= ~clockDescMap[index][2];
        return true;
    }
    return false;
}

#ifdef __cplusplus
}
#endif
