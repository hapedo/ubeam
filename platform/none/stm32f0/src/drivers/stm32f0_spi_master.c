/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32f0_spi_master.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/spi_master.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/drivers/clock_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

static void TransceiveNext(UBSpiMaster* master, SPI_TypeDef* spi)
{
    // Fill TX FIFO
    uint16_t status = spi->SR;
    if ((master->txCounter) && (status & SPI_SR_TXE))
    {
        if (master->txBuffer)
            *(uint8_t*)&(spi->DR) = *master->txBuffer++;
        else
            *(uint8_t*)&(spi->DR) = 0xff;
        master->txCounter--;
    }
    status = spi->SR;
    // Empty RX FIFO
    if (status & SPI_SR_RXNE)
    {
        if (master->rxBuffer)
        {
            *master->rxBuffer = *(uint8_t*)&(spi->DR);
            master->rxBuffer++;
        }
        else
        {
            volatile uint8_t temp = *(uint8_t*)&(spi->DR);
            (void)temp;
        }
        master->rxCounter++;
    }
}

static void Terminate(UBSpiMaster* master, SPI_TypeDef* spi)
{
	UBSpiMaster_DisableInt(master);

    // There is no possibility to terminate a transfer when there is data in FIFO
    //beam::TimeStamp timer = beam::SystemTime::getFutureMilli(SPI_WAIT_BUSY_TIMEOUT);

    // Wait until TX is finished (empty FIFO)
    while(spi->SR & (SPI_SR_FTLVL_0 | SPI_SR_FTLVL_1))
    {
//        if (beam::SystemTime::hasPassed(timer))
//            return false;
    }

    // Wait until last RX frame is processed
    while(spi->SR & SPI_SR_BSY)
    {
//        if (beam::SystemTime::hasPassed(timer))
//            return false;
    }

    //! Get all data from FIFO
    while(spi->SR & (SPI_SR_FRLVL_0 | SPI_SR_FRLVL_1))
    {
        volatile uint8_t temp = spi->DR;
        (void)temp;
//        if (beam::SystemTime::hasPassed(timer))
//            return false;
    }

/*    if (callback_)
    {
        // Don't forget to notify
        Buffer buffer(destinationBuffer_, rxCounter_);
        callback_(result, buffer);
        callback_ = beam::CallbackNull();
    }*/
}

void UBSpiMaster_Configure(UBSpiMaster* master, UBPeriHandle handle, UBGpioPin* mosi, UBGpioPin* miso, UBGpioPin* sck, UBGpioPin* cs)
{
	if (master == 0)
		return;
	master->base.platform = 0;
	UBPeri_Configure(&master->base.peri, handle);

	master->base.platform = master;

	if (master == 0)
		return;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return;
	UBGpioPinFunc func = UBGPF_GPIO;
#if defined(SPI1)
	if (handle == SPI1)
		func = UBGPF_SPI_1;
#endif
#if defined(SPI2)
	if (handle == SPI2)
		func = UBGPF_SPI_2;
#endif
	master->base.mosi = mosi;
	if (mosi)
	{
		UBPower_EnablePower(mosi->port.base.handle);
		UBGpioPin_SetFunction(mosi, func);
	}
	master->base.miso = miso;
	if (miso)
	{
		UBPower_EnablePower(miso->port.base.handle);
		UBGpioPin_SetFunction(miso, func);
	}
	master->base.sck = sck;
	if (sck)
	{
		UBPower_EnablePower(sck->port.base.handle);
		UBGpioPin_SetFunction(sck, func);
	}
	master->base.cs = cs;
	if (cs)
	{
		UBPower_EnablePower(cs->port.base.handle);
		UBGpioPin_Output(cs);
		UBGpioPin_Set(cs);
	}
    master->sckFrequency = 1000000;
    master->state = UBSMS_NOT_INITIALIZED;
    master->rxBuffer = 0;
    master->txBuffer = 0;
    master->rxCounter = 0;
    master->txCounter = 0;
    master->dataSize = 0;
	UBMutex_Unlock(&master->base.peri.mutex);
}

void UBSpiMaster_EnableInt(UBSpiMaster* master)
{
	if (master == 0)
		return;
	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return;
	if (master->base.peri.handle == SPI1)
		UBInt_EnableInt(SPI1_IRQn);
	else if (master->base.peri.handle == SPI2)
		UBInt_EnableInt(SPI2_IRQn);
	UBMutex_Unlock(&master->base.peri.mutex);
}

void UBSpiMaster_DisableInt(UBSpiMaster* master)
{
	if (master == 0)
		return;
	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return;
	if (master->base.peri.handle == SPI1)
		UBInt_DisableInt(SPI1_IRQn);
	else if (master->base.peri.handle == SPI2)
		UBInt_DisableInt(SPI2_IRQn);
	UBMutex_Unlock(&master->base.peri.mutex);
}

UBSpiMasterState UBSpiMaster_GetState(UBSpiMaster* master)
{
	if (master == 0)
		return UBSMS_NOT_CONFIGURED;
	return master->state;
}

UBSpiMasterResult UBSpiMaster_Init(UBSpiMaster* master, UBSpiMasterCpol cpol, UBSpiMasterCpha cpha, bool msbFirst)
{
	return UBSpiMaster_InitWithFreq(master, master->sckFrequency, cpol, cpha, msbFirst);
}

UBSpiMasterResult UBSpiMaster_InitWithFreq(UBSpiMaster* master, UBFrequency sckFreq, UBSpiMasterCpol cpol, UBSpiMasterCpha cpha, bool msbFirst)
{
	if (master == 0)
		return UBSMR_NOT_CONFIGURED;

	if (master->state == UBSMS_NOT_CONFIGURED)
		return UBSMR_NOT_CONFIGURED;

	if (master->state == UBSMS_BUSY)
		return UBSMR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBSMR_LOCKED;

	UBPower_EnablePower(master->base.peri.handle);
	UBSpiMaster_DisableInt(master);

	SPI_TypeDef* spi = master->base.peri.handle;

	uint16_t cr1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI;
	if (cpol == UBSMCPOL_1)
		cr1 |= SPI_CR1_CPOL;
	if (cpha == UBSMCPHA_1)
		cr1 |= SPI_CR1_CPHA;
	if (msbFirst == false)
		cr1 |= SPI_CR1_LSBFIRST;
	spi->CR1 = cr1;
	UBSpiMaster_SetSckFrequency(master, sckFreq);
	spi->I2SCFGR &= ~SPI_I2SCFGR_I2SMOD;
	spi->CR2 = SPI_CR2_FRXTH | SPI_CR2_DS_2 | SPI_CR2_DS_1 | SPI_CR2_DS_0;
	spi->CR1 |= SPI_CR1_SPE;
	master->state = UBSMS_IDLE;
	UBMutex_Unlock(&master->base.peri.mutex);
	return UBSMR_OK;
}

UBSpiMasterResult UBSpiMaster_SetSckFrequency(UBSpiMaster* master, UBFrequency sckFreq)
{
	if (master == 0)
		return UBSMR_NOT_CONFIGURED;

	if (master->state == UBSMS_NOT_CONFIGURED)
		return UBSMR_NOT_CONFIGURED;

	if (master->state == UBSMS_BUSY)
		return UBSMR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBSMR_LOCKED;

	UBFrequency periFreq = UBClock_GetInputFrequency(master->base.peri.handle);

    uint8_t div = 0;

    // Calculate the divisor
    while(sckFreq < (periFreq >> (div + 1)))
    {
        div++;
        if (div == 7)
            break;
    }
    ((SPI_TypeDef*)master->base.peri.handle)->CR1 = (((SPI_TypeDef*)master->base.peri.handle)->CR1 & 0xffc7) | (div << 3);

    master->sckFrequency = periFreq >> (div + 1);

    UBMutex_Unlock(&master->base.peri.mutex);

	return UBSMR_OK;
}

UBFrequency UBSpiMaster_GetSckFrequency(UBSpiMaster* master)
{
	if (master == 0)
		return 0;

	if (master->state == UBSMS_NOT_CONFIGURED)
		return 0;

	return master->sckFrequency;
}

UBSpiMasterResult UBSpiMaster_TransceiveSync(UBSpiMaster* master, const uint8_t* txData, uint8_t* rxData, uint16_t size)
{
	if (master == 0)
		return UBSMR_NOT_CONFIGURED;

	if (master->state == UBSMS_NOT_CONFIGURED)
		return UBSMR_NOT_CONFIGURED;

	if (master->state == UBSMS_BUSY)
		return UBSMR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBSMR_LOCKED;

	master->txCounter = size;
	master->rxCounter = 0;
	master->rxBuffer = rxData;
	master->txBuffer = txData;
	master->dataSize = size;

	SPI_TypeDef* spi = master->base.peri.handle;

    // Perform transmission
    while(master->rxCounter != size)
    {
        TransceiveNext(master, spi);
        uint16_t status = spi->SR;
        if (status & (SPI_SR_FRE | SPI_SR_OVR | SPI_SR_MODF))
        {
            // Error occurred -> terminate pending transaction and disable SPI
            Terminate(master, spi);
            spi->CR1 &= ~SPI_CR1_SPE;
            UBMutex_Unlock(&master->base.peri.mutex);
            return UBSMR_ERROR;
        }
    }

    // Wait until SPI is not busy
    while(spi->SR & SPI_SR_BSY);

    UBMutex_Unlock(&master->base.peri.mutex);

    return UBSMR_OK;
}

UBSpiMasterResult UBSpiMaster_TransceiveAsync(UBSpiMaster* master, const uint8_t* txData, uint8_t* rxData, uint16_t size)
{
	return UBSMR_NOT_SUPPORTED;
}

UBSpiMasterResult UBSpiMaster_TerminateAsync(UBSpiMaster* master)
{
	return UBSMR_NOT_SUPPORTED;
}

#ifdef __cplusplus
}
#endif
