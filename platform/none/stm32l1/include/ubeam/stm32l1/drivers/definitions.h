/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   definitions.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_STM32L1_DRIVERS_DEFINITIONS_H_
#define _UBEAM_STM32L1_DRIVERS_DEFINITIONS_H_

#include <ubeam/setup.h>

#ifndef UBEAM_CPU_MODEL
    #error "Error: CPU model not defined!"
#endif

#if (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L100xB)
     #define STM32L100xB
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L100xBA)
     #define STM32L100xBA
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L100xC)
     #define STM32L100xC
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xB)
     #define STM32L151xB
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xBA)
     #define STM32L151xBA
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xC)
     #define STM32L151xC
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xCA)
     #define STM32L151xCA
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xD)
     #define STM32L151xD
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L151xE)
     #define STM32L151xE
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xB)
     #define STM32L152xB
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xBA)
     #define STM32L152xBA
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xC)
     #define STM32L152xC
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xCA)
     #define STM32L152xCA
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xD)
     #define STM32L152xD
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xDX)
     #define STM32L152xDX
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L152xE)
     #define STM32L152xE
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xC)
     #define STM32L162xC
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xCA)
     #define STM32L162xCA
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xD)
     #define STM32L162xD
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xDX)
     #define STM32L162xDX
#elif (UBEAM_CPU_MODEL == UBEAM_CPU_MODEL_STM32L162xE)
     #define STM32L162xE
#else
    #error This MCU is not supported
#endif


#include <ubeam/stm32l1/3rd_party/ST/stm32l1xx.h>

/*! @} */

#endif // _UBEAM_STM32F0_DRIVERS_DEFINITIONS_H_
