/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   uart.h
 * \brief  microBeam SDK
 * \date   7.11.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_STM32L1_DRIVERS_UART_H_
#define _UBEAM_STM32L1_DRIVERS_UART_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/uart.h>
#include <ubeam/drivers/uart_types.h>
#include <ubeam/stm32l1/drivers/definitions.h>

/*! \defgroup ubeam_drivers_stm32l1_uart STM32L1 UART driver
 *  \ingroup ubeam_drivers_stm32l1
 *  \brief STM32L1 UART driver
 */

/*! \addtogroup ubeam_drivers_stm32l1_uart
 *  @{
 */

#define UBEAM_UART_PLATFORM

//! STM32L1 UART peripheral struct. Do not acess directly - always use appropriate driver function.
typedef struct
{
	UBUartBase base;							//!< Platform independent SPI driver struct instance
	UBUartState state;							//!< Current driver state
	UBFrequency baudrate;						//!< Current baudrate
} UBUart;

/*! @} */

#endif // _UBEAM_STM32L1_DRIVERS_UART_H_
