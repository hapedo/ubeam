/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   clock_management.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_STM32L1_DRIVERS_CLOCK_MANAGEMENT_H_
#define _UBEAM_STM32L1_DRIVERS_CLOCK_MANAGEMENT_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/clock_management.h>
#include <ubeam/stm32l1/drivers/definitions.h>

/*! \defgroup ubeam_drivers_stm32l1_clockmanagement STM32L1 clock management driver
 *  \ingroup ubeam_drivers_stm32l1
 *  \brief STM32L1 clock management driver
 */

/*! \addtogroup ubeam_drivers_stm32l1_clockmanagement
 *  @{
 */

//! STM32L1 Clock source
typedef enum
{
	UBCLKSRC_UNKNOWN = 0,
	UBCLKSRC_LSI,
	UBCLKSRC_LSE,
	UBCLKSRC_HSI,
    UBCLKSRC_HSE,
	UBCLKSRC_MSI,
	UBCLKSRC_PLL,
    UBCLKSRC_SDIO,
    UBCLKSRC_ADC,
	UBCLKSRC_SYSCLK,
    UBCLKSRC_SYSTIMER,
	UBCLKSRC_HCLK,
	UBCLKSRC_PCLK1,
	UBCLKSRC_PCLK2,
	UBCLKSRC_TIM1,
	UBCLKSRC_TIM2,
	UBCLKSRC_RTC,
	UBCLKSRC_MCO
} UBClockSource;

#ifdef __cplusplus
extern "C"
{
#endif

/*! Get peripheral clock source
 *  \param handle Peripheral handle
 *  \return Clock source, UBCLKSRC_UNKNOWN when unable to get
 */
UBClockSource UBClock_GetClockSource(UBPeriHandle handle);

/*! Enable clock source
 *  \param clkSource Clock source to enable
 *  \return true on success
 */
bool UBClock_EnableClockSource(UBClockSource clkSource);

/*! Disable clock source
 *  \param clkSource Clock source to disabled
 *  \return true on success
 */
bool UBClock_DisableClockSource(UBClockSource clkSource);

/*! Wait clock source to become ready
 *  \param clkSource Clock source to wait for
 *  \return true on success (is ready)
 */
bool UBClock_WaitClockSourceReady(UBClockSource clkSource);

/*! Get clock source frequency in Hz
 *  \param clkSource Clock source to ger
 *  \return Clock source frequency in Hz, 0 when unable to get or disabled
 */
UBFrequency UBClock_GetClockSourceFrequency(UBClockSource clkSource);

/*! Set / change system clock source
 *  \param clkSource Clock source to be set
 *  \disableCurrSource Disable current clock source after change
 *  \return true on success
 */
bool UBClock_SetSysClockSource(UBClockSource clkSource, bool disableCurrSource);

/*! Get system clock source
 *  \return System clock source
 */
UBClockSource UBClock_GetSysClockSource();

/*! Configure PLL
 *  \param pllSource PLL clock source
 *  \param pllSourceDivisor PLL clock source divisor
 *  \param pllMultiply PLL clock multiplier
 *  \return true on success, false when bad clock source, bad config, not ready
 */
bool UBClock_SetPllConfig(UBClockSource pllSource, uint8_t pllSourceDivisor, uint16_t pllMultiply);

/*! Set HSE oscilator frequency in Hz. This shall be called to be able to calculate peripheral input frequencies when HSE is used.
 *  \param frquency HSE oscillator frequency in Hz. Set to crystal / oscillator value
 */
void UBClock_SetHseOscillatorFrequency(uint32_t frequency);

/*! Set AHB prescaller.
 \param presc Prescaller value (2, 4, 8, 16, 64, 128, 256, 512)
  \return true on success
*/
bool UBClock_SetAhbPresc(uint16_t presc);

/*! Set APB1 prescaller where prescaller = 2^value.
 \param presc Prescaller value (0 to 4)
  \return true on success
*/
bool UBClock_SetApb1Presc(uint8_t presc);

/*! Set APB1 prescaller where prescaller = 2^value.
 \param presc Prescaller value (0 to 4)
  \return true on success
*/
bool UBClock_SetApb2Presc(uint8_t presc);

/*! Set RTC/LCD clock source
 \param presc Prescaller value (1 to 4)
  \return true on success
*/
bool UBClock_SetRtcLcdClockSource(UBClockSource clkSource);

/*! Get RTC/LCD clock source
  \return Clock source
*/
UBClockSource UBClock_GetRtcLcdClockSource();

/*! Set RTC/LCD prescaller where prescaller = 2^value.
  \param clkSource Clock source to be set (UBCLKSRC_LSI, UBCLKSRC_LSE, UBCLKSRC_HSE)
  \return true on success
*/
bool UBClock_SetRtcLcdPrescaller(uint8_t presc);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_STM32L1_DRIVERS_CLOCK_MANAGEMENT_H_
