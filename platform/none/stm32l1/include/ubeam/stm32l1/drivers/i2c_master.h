/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   i2c_master.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_STM32L1_DRIVERS_I2C_MASTER_H_
#define _UBEAM_STM32L1_DRIVERS_I2C_MASTER_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/i2c_master.h>
#include <ubeam/drivers/i2c_master_types.h>
#include <ubeam/stm32l1/drivers/definitions.h>

/*! \defgroup ubeam_drivers_stm32l1_i2cmaster STM32L1 I2C master driver
 *  \ingroup ubeam_drivers_stm32l1
 *  \brief STM32L1 I2C master driver
 */

/*! \addtogroup ubeam_drivers_stm32l1_i2cmaster
 *  @{
 */

#define UBEAM_I2C_MASTER_PLATFORM

//! STM32L1 I2C peripheral struct. Do not acess directly - always use appropriate driver function.
typedef struct
{
	UBI2cMasterBase base;						//!< Platform independent I2C driver struct
	UBI2cMasterState state;						//!< Current driver state
	UBFrequency sclFrequency;					//!< Current SCL frequency in Hz
	uint16_t counter;							//!< Transaction counter
	uint16_t dataSize;							//!< Transaction data size in bytes
	uint8_t* data;								//!< I/O buffer ptr
	uint8_t slaveAddress;						//!< Slave address
	UBI2cMasterResult result;					//!< Transaction result (partial result)
	bool addressTransmited;						//!< Address transmited flag
	bool isWriting;								//!< Write transaction flag
} UBI2cMaster;

/*! @} */

#endif // _UBEAM_STM32L1_DRIVERS_I2C_MASTER_H_
