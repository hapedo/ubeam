/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   rtc.h
 * \brief  microBeam SDK
 * \date   31.07.2020
 * \note
 * \todo
 */
#ifndef _UBEAM_STM32L1_DRIVERS_RTC_H_
#define _UBEAM_STM32L1_DRIVERS_RTC_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/rtc.h>
#include <ubeam/drivers/rtc_types.h>

/*! \defgroup ubeam_drivers_stm32l1_rtc STM32L1 RTC driver
 *  \ingroup ubeam_drivers_stm32l1
 *  \brief STM32L1 RTC driver
 */

/*! \addtogroup ubeam_drivers_stm32l1_rtc
 *  @{
 */

//! Platform specific RTC struct
typedef struct
{
    UBRtcBase base;             //!< Base struct
    UBRtcState state;           //!< Driver state
} UBRtc;

typedef enum
{
	UBRTCTAMP_1 = 0,
	UBRTCTAMP_2,
	UBRTCTAMP_3,
} UBRtcTamper;

//! Tamper precharge
typedef enum
{
	UBRTCTPCH_1 = 0,			//!< 1 RTCCLK cycle
	UBRTCTPCH_2,				//!< 2 RTCCLK cycles
	UBRTCTPCH_4,				//!< 4 RTCCLK cycles
	UBRTCTPCH_8					//!< 8 RTCCLK cycles
} URtcTamperPrech;

//! Tamper filter
typedef enum
{
	UBRTCFLT_0 = 0,				//!< Immediate
	UBRTCFLT_2,					//!< 2 consecurive samples
	UBRTCFLT_4,					//!< 4 consecurive samples
	UBRTCFLT_8,					//!< 8 consecurive samples
} URtcTamperFlt;

//! Tamper sampling frequency
typedef enum
{
	UBRTCTPF_1HZ = 0,
	UBRTCTPF_2HZ,
	UBRTCTPF_4HZ,
	UBRTCTPF_8HZ,
	UBRTCTPF_16HZ,
	UBRTCTPF_32HZ,
	UBRTCTPF_64HZ,
	UBRTCTPF_128HZ
} UBRtcTamperFreq;

UBRtcResult UBRtc_ConfigureTamper(UBRtc* rtc, bool disablePullup, URtcTamperPrech precharge, URtcTamperFlt filter, UBRtcTamperFreq sampleFreq);

UBRtcResult UBRtc_ConfigureTamperPin(UBRtc* rtc, UBRtcTamper tamper, bool activeHigh);

UBRtcResult UBRtc_EnableTamperPin(UBRtc* rtc, UBRtcTamper tamper);

UBRtcResult UBRtc_DisableTamperPin(UBRtc* rtc, UBRtcTamper tamper);

bool UBRtc_IsTamperDetected(UBRtc* rtc, UBRtcTamper tamper, bool clearFlag);

/*! @} */

#endif // _UBEAM_STM32L1_DRIVERS_RTC_H_
