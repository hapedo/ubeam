/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32l1_clock_management.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/stm32l1/drivers/clock_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

static const uint32_t clockDescMap[][6] = {
    { GPIOA_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOAEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIOAEN,    UBCLKSRC_HCLK},
    { GPIOB_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOBEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIOBEN,    UBCLKSRC_HCLK},
    { GPIOC_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOCEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIOCEN,    UBCLKSRC_HCLK},
    { GPIOD_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIODEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIODEN,    UBCLKSRC_HCLK},
    { GPIOE_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOEEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIOEEN,    UBCLKSRC_HCLK},
    { GPIOF_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOFEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIOFEN,    UBCLKSRC_HCLK},
    { GPIOG_BASE    , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_GPIOGEN, (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_GPIOGEN,    UBCLKSRC_HCLK},
    { CRC_BASE      , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_CRCEN,   (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_CRCEN,      UBCLKSRC_HCLK},
    { DMA1_BASE     , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_DMA1EN,  (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_DMA1EN,     UBCLKSRC_HCLK},
    { DMA2_BASE     , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_DMA2EN,  (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_DMA2EN,     UBCLKSRC_HCLK},
#ifdef AES_BASE
    { AES_BASE      , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_AESEN,   (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_AESEN,      UBCLKSRC_HCLK},
#endif
#ifdef FSMC_R_BASE
    { FSMC_R_BASE   , (uint32_t)&RCC->AHBENR       , RCC_AHBENR_FSMCEN,  (uint32_t)&RCC->AHBLPENR     , RCC_AHBENR_FSMCEN,     UBCLKSRC_HCLK},
#endif
    { SYSCFG_BASE   , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_SYSCFGEN, (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_SYSCFGEN,  UBCLKSRC_PCLK2},
    { TIM9_BASE     , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM9EN,   (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_TIM9EN,    UBCLKSRC_TIM2},
    { TIM10_BASE    , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM10EN,  (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_TIM10EN,   UBCLKSRC_TIM2},
    { TIM11_BASE    , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_TIM11EN,  (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_TIM11EN,   UBCLKSRC_TIM2},
    { ADC1_BASE     , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_ADC1EN,   (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_ADC1EN,    UBCLKSRC_ADC},
#ifdef SDIO_BASE
    { SDIO_BASE     , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_SDIOEN,   (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_SDIOEN,    UBCLKSRC_SDIO},
#endif
    { SPI1_BASE     , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_SPI1EN,   (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_SPI1EN,    UBCLKSRC_PCLK2},
    { USART1_BASE   , (uint32_t)&RCC->APB2ENR      , RCC_APB2ENR_USART1EN, (uint32_t)&RCC->APB2LPENR  , RCC_APB2ENR_USART1EN,  UBCLKSRC_PCLK2},
    { TIM2_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM2EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_TIM2EN,    UBCLKSRC_TIM1},
    { TIM3_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM3EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_TIM3EN,    UBCLKSRC_TIM1},
    { TIM4_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM4EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_TIM4EN,    UBCLKSRC_TIM1},
    { TIM5_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM5EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_TIM5EN,    UBCLKSRC_TIM1},
    { TIM6_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM6EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_TIM6EN,    UBCLKSRC_TIM1},
    { TIM7_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_TIM7EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_TIM7EN,    UBCLKSRC_TIM1},
#ifdef LCD_BASE
    { LCD_BASE      , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_LCDEN,    (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_LCDEN,     UBCLKSRC_RTC},
#endif
    { WWDG_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_WWDGEN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_WWDGEN,    UBCLKSRC_PCLK1},
    { SPI2_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_SPI2EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_SPI2EN,    UBCLKSRC_PCLK1},
    { SPI3_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_SPI3EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_SPI3EN,    UBCLKSRC_PCLK1},
    { USART2_BASE   , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_USART2EN, (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_USART2EN,  UBCLKSRC_PCLK1},
    { USART3_BASE   , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_USART3EN, (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_USART3EN,  UBCLKSRC_PCLK1},
#ifdef UART4_BASE
    { UART4_BASE    , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_UART4EN,  (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_UART4EN,   UBCLKSRC_PCLK1},
#endif
#ifdef UART5_BASE
    { UART5_BASE    , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_UART5EN,  (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_UART5EN,   UBCLKSRC_PCLK1},
#endif
    { I2C1_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_I2C1EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_I2C1EN,    UBCLKSRC_PCLK1},
    { I2C2_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_I2C2EN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_I2C2EN,    UBCLKSRC_PCLK1},
//    { USB_BASE      , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_I2C1EN,    UBCLKSRC_PCLK1},
    { PWR_BASE      , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_PWREN,    (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_PWREN,     UBCLKSRC_PCLK1},
    { DAC_BASE      , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_DACEN,    (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_DACEN,     UBCLKSRC_PCLK1},
    { COMP_BASE     , (uint32_t)&RCC->APB1ENR      , RCC_APB1ENR_COMPEN,   (uint32_t)&RCC->APB1LPENR  , RCC_APB1ENR_COMPEN,    UBCLKSRC_PCLK1},
    { RTC_BASE      , (uint32_t)&RCC->CSR          , RCC_CSR_RTCEN     ,   0                          , 0                   ,  UBCLKSRC_RTC},
    { FLASH_BASE    , 0                            , 0                 ,   0                          , 0                   ,  UBCLKSRC_HCLK},
    { SysTick_BASE  , 0                            , 0                 ,   0                          , 0                   ,  UBCLKSRC_SYSTIMER},
    { IWDG_BASE     , 0                            , 0                 ,   0                          , 0                   ,  UBCLKSRC_LSI},
    { 0             , 0                            , 0                 ,   0                          , 0                   ,  UBCLKSRC_UNKNOWN}
};

static UBFrequency s_hseOscFrequency = 0;

static uint16_t s_ahbPrescDiv[8] = {2, 4, 8, 16, 64, 128, 256, 512};
static uint16_t s_pllMul[9] = {3, 4, 6, 8, 12, 16, 24, 32, 48};

static int GetClockDescriptor(UBPeriHandle handle)
{
    int i = 0;

    while(clockDescMap[i][0] != 0)
    {
        if (clockDescMap[i][0] == (uint32_t)handle)
            return i;
        i++;
    }
    return 0;
}

UBFrequency UBClock_GetInputFrequency(UBPeriHandle handle)
{
	int index = GetClockDescriptor(handle);
	if (index != -1)
		return UBClock_GetClockSourceFrequency(clockDescMap[index][5]);
	else
		return 0;
}

UBClockSource UBClock_GetClockSource(UBPeriHandle handle)
{
    int index = GetClockDescriptor(handle);
    if (index != -1)
        return clockDescMap[index][5];
    else
        return UBCLKSRC_UNKNOWN;
}

bool UBClock_EnableClockSource(UBClockSource clkSource)
{
    switch(clkSource)
    {
    case UBCLKSRC_MSI:
        RCC->CR |= 0x00000100;
        break;
    case UBCLKSRC_HSI:
        RCC->CR |= 0x00000001;
        break;
    case UBCLKSRC_HSE:
        //RCC->CR |= 0x00080000;
        RCC->CR |= 0x00010000;
        break;
    case UBCLKSRC_LSE:
	    PWR->CR |= 0x10;
        RCC->CSR |= 0x00000100;
      	PWR->CR &= ~0x10;
        break;
    case UBCLKSRC_LSI:
        RCC->CSR |= 0x00000001;
        while((RCC->CSR & 0x00000002) == 0)
        {
        }
        break;
    case UBCLKSRC_PLL:
        RCC->CR |= 0x01000000;
        break;
    default:
        return -1;
    }
    return 0;
}

bool UBClock_DisableClockSource(UBClockSource clkSource)
{
    switch(clkSource)
    {
    case UBCLKSRC_MSI:
        RCC->CR &= ~0x00000100;
        while(RCC->CR & 0x00000200);
        break;
    case UBCLKSRC_HSI:
        RCC->CR &= ~0x00000001;
        while(RCC->CR & 0x00000002);
        break;
    case UBCLKSRC_HSE:
        RCC->CR &= ~0x00010000;
        while(RCC->CR & 0x00020000);
        break;
    case UBCLKSRC_LSE:
	    PWR->CR |= 0x10;
        RCC->CSR &= ~0x00000100;
        PWR->CR &= ~0x10;
        while(RCC->CSR & 0x00000200);
        break;
    case UBCLKSRC_PLL:
        RCC->CR &= ~0x01000000;
        while(RCC->CR & 0x02000000);
        break;
    default:
        return -1;
    }
    return 0;
}

bool UBClock_WaitClockSourceReady(UBClockSource clkSource)
{
    switch(clkSource)
    {
    case UBCLKSRC_MSI:
        while((RCC->CR & 0x00000200) == 0);
        break;
    case UBCLKSRC_HSI:
        while((RCC->CR & 0x00000002) == 0);
        break;
    case UBCLKSRC_HSE:
        while((RCC->CR & 0x00020000) == 0);
        break;
    case UBCLKSRC_LSE:
        while((RCC->CSR & 0x00000200) == 0);
        break;
    case UBCLKSRC_PLL:
        while((RCC->CR & 0x02000000) == 0);
        break;
    default:
        return -1;
    }
    return 0;
}

UBFrequency UBClock_GetClockSourceFrequency(UBClockSource clkSource)
{
    uint32_t frequency = 0;
    switch(clkSource)
    {

    case UBCLKSRC_LSE:
        frequency = 32768;
        break;

    case UBCLKSRC_LSI:
        frequency = 37000;
        break;

    case UBCLKSRC_HSI:
        frequency = 16000000;
        break;

    case UBCLKSRC_MSI:
        {
            uint8_t sel = (RCC->ICSCR >> 13) & 0x07;
            switch(sel)
            {
            case 0:
                frequency = 65536;
                break;                
            case 1:
                frequency = 131072;
                break;                
            case 2:
                frequency = 262144;
                break;                
            case 3:
                frequency = 524288;
                break;                
            case 4:
                frequency = 1048000;
                break;                
            case 5:
                frequency = 2097000;
                break;                
            case 6:
                frequency = 4194000;
                break;
            default:
                frequency = 1000;
                break;                
            }
        }
        break;

    case UBCLKSRC_HSE:
        frequency = s_hseOscFrequency;
        break;

    case UBCLKSRC_PLL:
        {
            frequency = 2 * UBClock_GetClockSourceFrequency(UBCLKSRC_SDIO);
            frequency /= ((RCC->CFGR >> 22) & 0x03) + 1;
            break;
        }
        
    case UBCLKSRC_SDIO:
        {
            if (RCC->CFGR & 0x00010000)
                frequency = s_hseOscFrequency;
            else
                frequency = 16000000;
            uint8_t idx = (RCC->CFGR >> 18) & 0x0f;
            if (idx > 0x08)
                break;
            frequency = (frequency * s_pllMul[idx]) / 2;
            break;
        }

    case UBCLKSRC_ADC:
        frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSI);
        break;
        
    case UBCLKSRC_SYSCLK:
        switch(RCC->CFGR & 0x03)
        {
        case 0x00:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_MSI);
            break;
        case 0x01:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSI);
            break;
        case 0x02:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSE);
            break;
        case 0x03:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PLL);
            break;
        }
        break;

    case UBCLKSRC_SYSTIMER:
        if (SysTick->CTRL & SysTick_CTRL_CLKSOURCE_Msk)
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HCLK);
        else
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HCLK) >> 3;
        break;
        
    case UBCLKSRC_HCLK:
    {
        uint32_t div = 1;
        if (RCC->CFGR & 0x00000080) 
        {
            uint8_t idx = (RCC->CFGR >> 4) & 0x07;
            div = s_ahbPrescDiv[idx];
        }
        frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_SYSCLK) / div;
        break;
    }

    case UBCLKSRC_PCLK1:
    {
        frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HCLK);
        uint8_t div = (RCC->CFGR >> 8) & 0x07; 
        if (div & 0x04)
        {
            frequency >>= (div & 0x03) + 1;
        }
        break;
    }

    case UBCLKSRC_PCLK2:
    {
        frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HCLK);
        uint8_t div = (RCC->CFGR >> 11) & 0x07; 
        if (div & 0x04)
        {
            frequency >>= (div & 0x03) + 1;
        }
        break;
    }

    case UBCLKSRC_TIM1:
        if (RCC->CFGR & 0x00000400)
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK1) << 1;
        else
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK1);
        break;

    case UBCLKSRC_TIM2:
        if (RCC->CFGR & 0x00002000)
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK2) << 1;
        else
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_PCLK2);
        break;

    case UBCLKSRC_RTC:
        switch ((RCC->CSR >> 16) & 0x03)
        {
        case 0x00:
            frequency = 0;
            break;
        case 0x01:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_LSE);
            break;
        case 0x02:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_LSI);
            break;
        case 0x03:
            frequency = UBClock_GetClockSourceFrequency(UBCLKSRC_HSE) >> (((RCC->CR >> 29) & 0x03) + 1);
            break;
        }
        break;

    case UBCLKSRC_MCO:
        break;

    case UBCLKSRC_UNKNOWN:
    default:
        break;
    }
    return frequency;
}

bool UBClock_SetSysClockSource(UBClockSource clkSource, bool disableCurrSource)
{
    if ((clkSource != UBCLKSRC_MSI) && (clkSource != UBCLKSRC_HSI) && (clkSource != UBCLKSRC_HSE) && (clkSource != UBCLKSRC_PLL))
        return -1;

    UBClockSource currSource = UBClock_GetSysClockSource();

    if (currSource != clkSource)
    {
    	UBClock_EnableClockSource(clkSource);
    	UBClock_WaitClockSourceReady(clkSource);
/*        if (UBClock_GetClockSourceFrequency(clkSource) > 24000000)
            FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;
        else
            FLASH->ACR = FLASH_ACR_PRFTBE;*/

        switch(clkSource)
        {
        case UBCLKSRC_MSI:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc);
            break;
        case UBCLKSRC_HSI:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc) | 0x01;
            break;
        case UBCLKSRC_HSE:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc) | 0x02;
            break;
        case UBCLKSRC_PLL:
            RCC->CFGR = (RCC->CFGR & 0xfffffffc) | 0x03;
            break;
        default:
            return false;
        }
        if (disableCurrSource)
        	UBClock_DisableClockSource(currSource);
    }
    return true;
}

UBClockSource UBClock_GetSysClockSource()
{
    switch(RCC->CFGR & 0x03)
    {
    case 0x00:
        return UBCLKSRC_MSI;
    case 0x01:
        return UBCLKSRC_HSI;
    case 0x02:
        return UBCLKSRC_HSE;
    case 0x03:
        return UBCLKSRC_PLL;
    default:
        return UBCLKSRC_UNKNOWN;
    }
}

bool UBClock_SetPllConfig(UBClockSource pllSource, uint8_t pllSourceDivisor, uint16_t pllMultiply)
{
    uint8_t pllIdx = 16;
    for(uint8_t i = 0; i < 9; i++)
    {
        if (s_pllMul[i] == pllMultiply)
        {
            pllIdx = i;
            break;
        }
    }
    
    if (pllIdx == 16)
        return false;

    if ((pllSource != UBCLKSRC_HSI) && (pllSource != UBCLKSRC_HSE))
        return false;

    if ((pllSourceDivisor < 2) || (pllSourceDivisor > 4))
        return false;

    // Cannot change PLL if sysclock source is PLL
    if (UBClock_GetSysClockSource() == UBCLKSRC_PLL)
        return false;

    if (UBClock_EnableClockSource(pllSource) != 0)
        return false;

    if (UBClock_WaitClockSourceReady(pllSource) != 0)
        return false;

    if (UBClock_DisableClockSource(UBCLKSRC_PLL) != 0)
        return false;

    RCC->CFGR = (RCC->CFGR & 0xff3fffff) | ((pllSourceDivisor - 1) << 22);

    RCC->CFGR = (RCC->CFGR & 0xffc3ffff) | (pllIdx << 18);

    if (pllSource == UBCLKSRC_HSI)
        RCC->CFGR &= ~0x00010000;
    else
        RCC->CFGR |= 0x00010000;

    if (UBClock_EnableClockSource(UBCLKSRC_PLL) != 0)
        return false;

    if (UBClock_WaitClockSourceReady(UBCLKSRC_PLL) != 0)
        return false;

    return true;
}

void UBClock_SetHseOscillatorFrequency(uint32_t frequency)
{
    s_hseOscFrequency = frequency;
}


bool UBClock_EnableClock(UBPeriHandle handle)
{
    int index = GetClockDescriptor(handle);
    if ((index != -1) && (clockDescMap[index][1]))
    {
        *(uint32_t*)clockDescMap[index][1] |= clockDescMap[index][2];
        return true;
    }
    return false;
}

bool UBClock_EnableClockInMode(UBPeriHandle handle, UBClockMode mode)
{
	if (mode == UBCM_NORMAL)
		return UBClock_EnableClock(handle);
	else if (mode == UBCM_LOW_POWER)
	{
	    int index = GetClockDescriptor(handle);
	    if ((index != -1) && (clockDescMap[index][3]))
	    {
	        *(uint32_t*)clockDescMap[index][3] |= clockDescMap[index][4];
	        return true;
	    }
	    return false;
	}
	return false;
}

bool UBClock_DisableClock(UBPeriHandle handle)
{
    int index = GetClockDescriptor(handle);
    if ((index != -1) && (clockDescMap[index][1]))
    {
        *(uint32_t*)clockDescMap[index][1] &= ~clockDescMap[index][2];
        return true;
    }
    return false;
}

bool UBClock_DisableClockInMode(UBPeriHandle handle, UBClockMode mode)
{
	if (mode == UBCM_NORMAL)
		return UBClock_DisableClock(handle);
	else if (mode == UBCM_LOW_POWER)
	{
	    int index = GetClockDescriptor(handle);
	    if ((index != -1) && (clockDescMap[index][3]))
	    {
	        *(uint32_t*)clockDescMap[index][3] &= ~clockDescMap[index][4];
	        return true;
	    }
	    return false;
	}
	return false;
}

bool UBClock_SetAhbPresc(uint16_t presc)
{
    uint8_t idx = 16;
    for(uint8_t i = 0; i < 8; i++)
    {
        if (s_ahbPrescDiv[i] == presc)
        {
            idx = i;
            break;
        }
    }
    if (idx == 16)
        return false;
    if (presc > 1)
        idx |= 0x8;
    RCC->CFGR |= (RCC->CFGR & 0xfffffff0) | (((uint32_t)idx) << 4);
    return true;
}

bool UBClock_SetApb1Presc(uint8_t presc)
{
    if (presc <= 4)
    {
        RCC->CFGR |= (RCC->CFGR & 0xfffff8ff) | ((uint32_t)presc << 8);
        return true;
    }
    return false;
}

bool UBClock_SetApb2Presc(uint8_t presc)
{
    if (presc <= 4)
    {
        RCC->CFGR |= (RCC->CFGR & 0xffffc7ff) | ((uint32_t)presc << 11);
        return true;
    }
    return false;
}

bool UBClock_SetRtcLcdClockSource(UBClockSource clkSource)
{
	PWR->CR |= PWR_CR_DBP;
	RCC->CSR |= RCC_CSR_RTCRST;
	RCC->CSR &= ~RCC_CSR_RTCRST;
	switch(clkSource)
	{
	case UBCLKSRC_LSE:
		RCC->CSR = (RCC->CSR & ~0x00030000) | (1 << 16);
		break;
	case UBCLKSRC_LSI:
		RCC->CSR = (RCC->CSR & ~0x00030000) | (2 << 16);
		break;
	case UBCLKSRC_HSE:
		RCC->CSR = (RCC->CSR & ~0x00030000) | (3 << 16);
		break;
	default:
		PWR->CR &= ~PWR_CR_DBP;
		return false;
	}
	PWR->CR &= ~PWR_CR_DBP;
	return true;
}

UBClockSource UBClock_GetRtcLcdClockSource()
{
	uint8_t src = (RCC->CSR >> 16) & 0x03;
	if (src == 1)
		return UBCLKSRC_LSE;
	else if (src == 2)
		return UBCLKSRC_LSI;
	else if (src == 3)
		return UBCLKSRC_HSI;
	return UBCLKSRC_UNKNOWN;
}

bool UBClock_SetRtcLcdPrescaller(uint8_t presc)
{
    if ((presc >= 1) && (presc <= 4))
    {
        RCC->CR |= (RCC->CR & 0x1fffffff) | (((uint32_t)presc - 1) << 29);
        return true;
    }
    return false;
}

#ifdef __cplusplus
}
#endif
