/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32l1_eeprom.c
 * \brief  microBeam SDK
 * \date   31.07.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/eeprom.h>
#include <ubeam/stm32l1/drivers/definitions.h>
#include <ubeam/utils/intrinsics.h>

#ifdef __cplusplus
extern "C" {
#endif

void UBEep_Configure(UBEep* eep, uint32_t memBase, size_t memSize)
{
	if (eep == 0)
		return;
	eep->base.platform = 0;
    UBPeri_Configure(&eep->base.peri, 0);
	if (!UBMutex_TryLock(&eep->base.peri.mutex))
		return;
    eep->memBase = memBase;
    eep->memSize = memSize;
    eep->state = UBEEPS_NOT_INITIALIZED;
    UBMutex_Unlock(&eep->base.peri.mutex);
}

UBEepResult UBEep_Init(UBEep* eep)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_BUSY)
        return UBEEPR_BUSY;

    // EEPROM support only (for now)
    if ((eep->memBase < 0x08080000) || (eep->memSize + eep->memBase > 0x08082000))
    	return UBEEPR_NOT_SUPPORTED;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_LOCKED;

    eep->state = UBEEPS_IDLE;
    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}

UBEepResult UBEep_MassErase(UBEep* eep)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_INITIALIZED)
        return UBEEPR_NOT_INITIALIZED;

    if (eep->state != UBEEPS_IDLE)
        return UBEEPR_BUSY;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_BUSY;

    FLASH->PEKEYR = 0x89abcdef;
    FLASH->PEKEYR = 0x02030405;

    if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0)
    {
        UBMutex_Unlock(&eep->base.peri.mutex);
        return UBEEPR_ERROR;
    }
    FLASH->PECR |= FLASH_PECR_FTDW;

    for(uint32_t i = 0; i < (uint32_t)eep->memSize; i++)
    {
    	uint8_t* ptr = (uint8_t*)(eep->memBase + i);
    	*ptr = 0;
    }

    FLASH->PECR |= FLASH_PECR_PELOCK;

    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}

UBEepResult UBEep_Write(UBEep* eep, uint32_t offset, const uint8_t* data, size_t size, size_t* sizeWritten)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_INITIALIZED)
        return UBEEPR_NOT_INITIALIZED;

    if (eep->state != UBEEPS_IDLE)
        return UBEEPR_BUSY;

    if (sizeWritten)
        *sizeWritten = 0;

    if (offset >= eep->memSize)
        return UBEEPR_OK;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_BUSY;

    if (offset > eep->memSize)
    	offset = eep->memSize;

    if (offset + size > eep->memSize)
    	size = eep->memSize - offset;

    if (size == 0)
    {
        UBMutex_Unlock(&eep->base.peri.mutex);
        return UBEEPR_OK;
    }

    FLASH->PEKEYR = 0x89abcdef;
    FLASH->PEKEYR = 0x02030405;

    if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0)
    {
        UBMutex_Unlock(&eep->base.peri.mutex);
        return UBEEPR_ERROR;
    }
    FLASH->PECR |= FLASH_PECR_FTDW;

    for(uint32_t i = 0; i < (uint32_t)size; i++)
    {
    	uint8_t* ptr = (uint8_t*)(eep->memBase + offset + i);
    	*ptr = data[i];
    }

    FLASH->PECR |= FLASH_PECR_PELOCK;

    if (sizeWritten)
    	*sizeWritten = size;

    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}

UBEepResult UBEep_Read(UBEep* eep, uint32_t offset, uint8_t* data, size_t size, size_t* sizeRead)
{
    if (eep == 0)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_CONFIGURED)
        return UBEEPR_NOT_CONFIGURED;

    if (eep->state == UBEEPS_NOT_INITIALIZED)
        return UBEEPR_NOT_INITIALIZED;

    if (eep->state != UBEEPS_IDLE)
        return UBEEPR_BUSY;

    if (sizeRead)
        *sizeRead = 0;

    if (offset >= eep->memSize)
        return UBEEPR_OK;

    if (!UBMutex_TryLock(&eep->base.peri.mutex))
        return UBEEPR_BUSY;

    if (offset > eep->memSize)
    	offset = eep->memSize;

    if (offset + size > eep->memSize)
    	size = eep->memSize - offset;

    if (size == 0)
    {
        UBMutex_Unlock(&eep->base.peri.mutex);
        return UBEEPR_OK;
    }

    for(uint32_t i = 0; i < (uint32_t)size; i++)
    {
    	uint8_t* ptr = (uint8_t*)(eep->memBase + offset + i);
    	data[i] = *ptr;
    }

    if (sizeRead)
    	*sizeRead = size;

    UBMutex_Unlock(&eep->base.peri.mutex);
    return UBEEPR_OK;
}


#ifdef __cplusplus
}
#endif
