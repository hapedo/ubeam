/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32l1_watchdog.c
 * \brief  microBeam SDK
 * \date   10.08.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/watchdog.h>
#include <ubeam/stm32l1/drivers/definitions.h>
#include <ubeam/drivers/clock_management.h>
#include <ubeam/utils/intrinsics.h>

#ifdef __cplusplus
extern "C" {
#endif

void UBWdg_Configure(UBWdg* wdg, UBPeriHandle handle)
{
	if (wdg == 0)
		return;
	wdg->base.platform = 0;
    UBPeri_Configure(&wdg->base.peri, handle);
	if (!UBMutex_TryLock(&wdg->base.peri.mutex))
		return;
	wdg->state = UBWDGS_NOT_INITIALIZED;
    UBMutex_Unlock(&wdg->base.peri.mutex);
}

UBWdgResult UBWdg_Init(UBWdg* wdg, uint32_t timeoutMilli)
{
    if (wdg == 0)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_NOT_CONFIGURED)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_BUSY)
        return UBWDGR_BUSY;

    if (!UBMutex_TryLock(&wdg->base.peri.mutex))
        return UBWDGR_BUSY;

    IWDG_TypeDef* w = wdg->base.peri.handle;
    UBFrequency freq = UBClock_GetClockSourceFrequency(UBClock_GetClockSource(wdg->base.peri.handle));

    uint16_t pre = 0;
    do
    {
    	uint32_t maxTimeout = 4096 * 1000 / (freq / (4 << pre));
    	if (maxTimeout >= timeoutMilli)
    		break;
    	pre++;
    } while((4 << pre) != 512);

    if ((4 << pre) >= 512)
    {
    	UBMutex_Unlock(&wdg->base.peri.mutex);
    	return UBWDGR_NOT_SUPPORTED;
    }

    uint32_t rl = timeoutMilli * (freq / (4 << pre)) / 1000;

    w->KR = 0x5555;
    w->PR = pre;
    w->RLR = rl;

    wdg->state = UBWDGS_IDLE;

    UBMutex_Unlock(&wdg->base.peri.mutex);
    return UBWDGR_OK;
}

UBWdgResult UBWdg_GetTimeoutMilli(UBWdg* wdg, uint32_t* timeoutMilli)
{
    if (wdg == 0)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_NOT_CONFIGURED)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_BUSY)
        return UBWDGR_BUSY;

    if (timeoutMilli == 0)
        return UBWDGR_BAD_PARAM;

    if (!UBMutex_TryLock(&wdg->base.peri.mutex))
        return UBWDGR_BUSY;

    IWDG_TypeDef* w = wdg->base.peri.handle;
    UBFrequency freq = UBClock_GetClockSourceFrequency(UBClock_GetClockSource(wdg->base.peri.handle));

    *timeoutMilli = w->RLR * 1000 / (freq / (4 << w->PR));

    UBMutex_Unlock(&wdg->base.peri.mutex);
    return UBWDGR_OK;
}

UBWdgResult UBWdg_Enable(UBWdg* wdg)
{
    if (wdg == 0)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_NOT_CONFIGURED)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_BUSY)
        return UBWDGR_BUSY;

    if (!UBMutex_TryLock(&wdg->base.peri.mutex))
        return UBWDGR_BUSY;

    IWDG_TypeDef* w = wdg->base.peri.handle;

    w->KR = 0xcccc;

    UBMutex_Unlock(&wdg->base.peri.mutex);
    return UBWDGR_OK;
}

UBWdgResult UBWdg_Disable(UBWdg* wdg)
{
	return UBWDGR_NOT_SUPPORTED;
}

UBWdgResult UBWdg_IsEnabled(UBWdg* wdg, bool* enabled)
{
	UBUnused(enabled);
	return UBWDGR_NOT_SUPPORTED;
}

UBWdgResult UBWdg_Feed(UBWdg* wdg)
{
    if (wdg == 0)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_NOT_CONFIGURED)
        return UBWDGR_NOT_CONFIGURED;

    if (wdg->state == UBWDGS_BUSY)
        return UBWDGR_BUSY;

    if (!UBMutex_TryLock(&wdg->base.peri.mutex))
        return UBWDGR_BUSY;

    IWDG_TypeDef* w = wdg->base.peri.handle;

    w->KR = 0xaaaa;

    UBMutex_Unlock(&wdg->base.peri.mutex);
    return UBWDGR_OK;
}

#ifdef __cplusplus
}
#endif
