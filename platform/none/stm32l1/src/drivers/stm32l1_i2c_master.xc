/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32l1_i2c_master.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */

#include <ubeam/drivers/i2c_master.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/drivers/clock_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

static void TransceiveNext(UBI2cMaster* master)
{
	I2C_TypeDef* i2c = (I2C_TypeDef*)master->base.peri.handle;
    if (i2c->ISR & (I2C_ISR_TIMEOUT | I2C_ISR_PECERR | I2C_ISR_OVR | I2C_ISR_ARLO | I2C_ISR_BERR))
    {
        master->result = UBI2MR_ERROR;
        return;
    }

    if (master->addressTransmited == false)
    {
        master->counter = 0;
        i2c->CR2 = 0;
        i2c->CR2 = (master->slaveAddress << 1) | (master->dataSize << 16);
        if (master->isWriting == false)
            i2c->CR2 |= I2C_CR2_RD_WRN;

        i2c->CR2 |= I2C_CR2_START;

        master->result = UBI2MR_BUSY;
        master->addressTransmited = true;
    }
    else
    {
        if (master->isWriting == false)
        {
            // Slave must ACK after address byte
            if ((master->counter == 0) && (i2c->ISR & I2C_ISR_NACKF))
            {
                master->counter = master->dataSize;
                master->result= UBI2MR_NACK;
                return;
            }
            if (i2c->ISR & I2C_ISR_RXNE)
            {
                if (master->data)
                    master->data[master->counter] = i2c->RXDR;
                else
                {
                    uint8_t temp = i2c->RXDR;
                    (void)temp;
                }
                master->counter++;
            }
            if (i2c->ISR & I2C_ISR_TC)
            {
                i2c->CR2 |= I2C_CR2_STOP;
                master->result = UBI2MR_OK;
            }
        }
        else
        {
            // Slave must ACK after each byte
            if (i2c->ISR & I2C_ISR_NACKF)
            {
                master->counter = master->dataSize;
                master->result= UBI2MR_NACK;
                return;
            }
            if (i2c->ISR & I2C_ISR_TXE)
            {
                if (master->counter < master->dataSize)
                    i2c->TXDR = master->data[master->counter++];
            }
            if (i2c->ISR & I2C_ISR_TC)
            {
                i2c->CR2 |= I2C_CR2_STOP;
                master->result = UBI2MR_OK;
            }
        }
    }
}

void UBI2cMaster_Configure(UBI2cMaster* master, UBPeriHandle handle, UBGpioPin* sda, UBGpioPin* scl)
{
	if (master == 0)
		return;
	master->base.platform = 0;
	UBPeri_Configure(&master->base.peri, handle);

	master->base.platform = master;

	if (master == 0)
		return;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return;
	UBGpioPinFunc func = UBGPF_GPIO;
#if defined(I2C1)
	if (handle == I2C1)
		func = UBGPF_I2C_1;
#endif
#if defined(I2C2)
	if (handle == I2C2)
		func = UBGPF_I2C_2;
#endif
	master->base.sda = sda;
	if (sda)
	{
		UBPower_EnablePower(sda->port.base.handle);
		UBGpioPin_SetFunction(sda, func);
	}
	master->base.scl = scl;
	if (scl)
	{
		UBPower_EnablePower(scl->port.base.handle);
		UBGpioPin_SetFunction(scl, func);
	}

	UBPower_EnablePower(master->base.peri.handle);

    master->sclFrequency = 100000;
    master->state = UBI2MS_NOT_INITIALIZED;
    master->data = 0;
    master->counter = 0;
    master->dataSize = 0;
	UBMutex_Unlock(&master->base.peri.mutex);
}

void UBI2cMaster_EnableInt(UBI2cMaster* master)
{
	if (master == 0)
		return;
	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return;
#if defined(I2C1)
	if (master->base.peri.handle == I2C1)
		UBInt_EnableInt(I2C1_IRQn);
#endif
#if defined(I2C2)
	if (master->base.peri.handle == I2C2)
		UBInt_EnableInt(I2C2_IRQn);
#endif
	UBMutex_Unlock(&master->base.peri.mutex);
}

void UBI2cMaster_DisableInt(UBI2cMaster* master)
{
	if (master == 0)
		return;
	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return;
#if defined(I2C1)
	if (master->base.peri.handle == I2C1)
		UBInt_DisableInt(I2C1_IRQn);
#endif
#if defined(I2C2)
	if (master->base.peri.handle == I2C2)
		UBInt_DisableInt(I2C2_IRQn);
#endif
	UBMutex_Unlock(&master->base.peri.mutex);
}

UBI2cMasterState UBI2cMaster_GetState(UBI2cMaster* master)
{
	if (master == 0)
		return UBI2MS_NOT_CONFIGURED;
	return master->state;
}

UBI2cMasterResult UBI2cMaster_Init(UBI2cMaster* master)
{
	return UBI2cMaster_InitWithFreq(master, master->sclFrequency);
}

UBI2cMasterResult UBI2cMaster_InitWithFreq(UBI2cMaster* master, UBFrequency sclFreq)
{
	if (master == 0)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_NOT_CONFIGURED)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_BUSY)
		return UBI2MR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBI2MR_LOCKED;

	UBPower_EnablePower(master->base.peri.handle);
	UBI2cMaster_DisableInt(master);

	I2C_TypeDef* i2c = master->base.peri.handle;

	UBI2cMasterResult result = UBI2cMaster_SetSclFrequency(master, sclFreq);
	if (result != UBI2MR_OK)
		return result;

	i2c->CR1 = I2C_CR1_PE;

	master->state = UBI2MS_IDLE;
	UBMutex_Unlock(&master->base.peri.mutex);
	return UBI2MR_OK;
}

UBI2cMasterResult UBI2cMaster_SetSclFrequency(UBI2cMaster* master, UBFrequency sclFreq)
{
	if (master == 0)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_NOT_CONFIGURED)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_BUSY)
		return UBI2MR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBI2MR_LOCKED;

	UBFrequency periFreq = UBClock_GetInputFrequency(master->base.peri.handle);


    uint32_t presc = 1;
    uint32_t freq = periFreq;
    while(freq / sclFreq >= 512)
    {
        presc++;
        freq = periFreq / presc;
        if (presc >= 16)
            return UBI2MR_BAD_PARAM;
    }

    uint8_t sclh = (freq / sclFreq) >> 1;
    uint8_t scll = (freq / sclFreq) - sclh;

    master->sclFrequency = periFreq / presc;

    presc--;
    sclh--;
    scll--;

    ((I2C_TypeDef*)master->base.peri.handle)->TIMINGR = (presc << 28) | (0x02 << 20) | (0x04 << 16) | (sclh << 8) | scll;

    UBMutex_Unlock(&master->base.peri.mutex);

	return UBI2MR_OK;
}

UBFrequency UBI2cMaster_GetSclFrequency(UBI2cMaster* master)
{
	UBFrequency periFreq = UBClock_GetInputFrequency(master->base.peri.handle);
	I2C_TypeDef* i2c = (I2C_TypeDef*)master->base.peri.handle;
    uint32_t presc = ((i2c->TIMINGR >> 28) & 0x0f) + 1;
    uint32_t sclh = ((i2c->TIMINGR >> 8) & 0xff) + 1;
    uint32_t scll = ((i2c->TIMINGR >> 8) & 0xff) + 1;
    return periFreq / (presc * (sclh + scll));
}

UBI2cMasterResult UBI2cMaster_WriteSync(UBI2cMaster* master, uint8_t slaveAddress, uint8_t* data, uint8_t size)
{
	if (master == 0)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_NOT_CONFIGURED)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_BUSY)
		return UBI2MR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBI2MR_LOCKED;

    master->data = data;
    master->dataSize = size;
    master->addressTransmited = false;
    master->slaveAddress = slaveAddress & 0x7f;
    master->isWriting = true;
    master->result = UBI2MR_BUSY;

    do
    {
        TransceiveNext(master);
    }
    while(master->result == UBI2MR_BUSY);

    while (((I2C_TypeDef*)master->base.peri.handle)->ISR & I2C_ISR_BUSY)
    {
    }

    UBI2cMaster_Terminate(master);

    UBMutex_Unlock(&master->base.peri.mutex);

    return master->result;
}

UBI2cMasterResult UBI2cMaster_ReadSync(UBI2cMaster* master, uint8_t slaveAddress, uint8_t* data, uint8_t size)
{
	if (master == 0)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_NOT_CONFIGURED)
		return UBI2MR_NOT_CONFIGURED;

	if (master->state == UBI2MS_BUSY)
		return UBI2MR_BUSY;

	if (!UBMutex_TryLock(&master->base.peri.mutex))
		return UBI2MR_LOCKED;

    master->data = data;
    master->dataSize = size;
    master->addressTransmited = false;
    master->slaveAddress = slaveAddress & 0x7f;
    master->isWriting = false;
    master->result = UBI2MR_BUSY;

    do
    {
        TransceiveNext(master);
    }
    while(master->result == UBI2MR_BUSY);

    while (((I2C_TypeDef*)master->base.peri.handle)->ISR & I2C_ISR_BUSY)
    {
    }

    UBI2cMaster_Terminate(master);

    UBMutex_Unlock(&master->base.peri.mutex);

    return master->result;
}

void UBI2cMaster_Terminate(UBI2cMaster* master)
{
	I2C_TypeDef* i2c = (I2C_TypeDef*)master->base.peri.handle;
    i2c->ICR |= I2C_ICR_ALERTCF | I2C_ICR_TIMOUTCF | I2C_ICR_PECCF | I2C_ICR_OVRCF |
            I2C_ICR_ARLOCF | I2C_ICR_BERRCF | I2C_ICR_STOPCF | I2C_ICR_NACKCF | I2C_ICR_ADDRCF;
    i2c->ISR |= I2C_ISR_RXNE;
}

#ifdef __cplusplus
}
#endif
