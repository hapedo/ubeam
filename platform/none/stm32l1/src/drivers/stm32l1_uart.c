/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32l1_uart.c
 * \brief  microBeam SDK
 * \date   7.11.2019
 * \note
 * \todo
 */
#include <ubeam/drivers/uart.h>
#include <ubeam/drivers/gpio.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/drivers/clock_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

static UBUart** Usart1()
{
	static UBUart* usart = 0;
	return &usart;
}

static UBUart** Usart2()
{
	static UBUart* usart = 0;
	return &usart;
}

static UBUart** Usart3()
{
	static UBUart* usart = 0;
	return &usart;
}

static void IrqHandler(UBUart* uart)
{
	USART_TypeDef* u = uart->base.peri.handle;
	uint32_t sr = u->SR;
	if (sr & USART_SR_TXE)
	{
		if (uart->base.txFifo)
		{
			uint8_t data;
			if (UBFifo_GetByte(uart->base.txFifo, &data))
				u->DR = data;
		}
	}
	if (sr & USART_SR_RXNE)
	{
		uint8_t data = u->DR;
		if (uart->base.rxFifo)
			UBFifo_PutByte(uart->base.rxFifo, data);
	}
	sr = u->SR;
	if (sr & USART_SR_TC)
	{
		u->CR1 &= ~(USART_CR1_TE | USART_CR1_TXEIE | USART_CR1_TCIE);
		u->SR &= ~USART_SR_TC;
	}
	if (sr & USART_SR_PE)
    {
        uint8_t data = u->DR;
        (void)data;
    }
	if (sr & USART_SR_FE)
    {
        uint8_t data = u->DR;
        (void)data;
    }
	if (sr & USART_SR_NE)
    {
        uint8_t data = u->DR;
        (void)data;
    }
	if (sr & USART_SR_ORE)
    {
        uint8_t data = u->DR;
        (void)data;
    }
}

void USART1_IRQHandler()
{
	UBUart** usart = Usart1();
	if (*usart)
	{
		IrqHandler(*usart);
	}
}

void USART2_IRQHandler()
{
	UBUart** usart = Usart2();
	if (*usart)
	{
		IrqHandler(*usart);
	}
}

void USART3_IRQHandler()
{
	UBUart** usart = Usart3();
	if (*usart)
	{
		IrqHandler(*usart);
	}
}

void UBUart_Configure(UBUart* uart, UBPeriHandle handle, UBGpioPin* rx, UBGpioPin* tx, UBFifo* rxFifo, UBFifo* txFifo)
{
	if (uart == 0)
		return;
	uart->base.platform = 0;
	UBPeri_Configure(&uart->base.peri, handle);

	uart->base.platform = uart;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return;
	UBGpioPinFunc func = UBGPF_GPIO;
	if (handle == USART1)
	{
		func = UBGPF_USART_1;
		*Usart1() = uart;
		UBNvic_Hook(USART1_IRQn, USART1_IRQHandler);
	}
	if (handle == USART2)
	{
		func = UBGPF_USART_2;
		*Usart2() = uart;
		UBNvic_Hook(USART2_IRQn, USART2_IRQHandler);
	}
	if (handle == USART3)
	{
		func = UBGPF_USART_3;
		*Usart3() = uart;
		UBNvic_Hook(USART3_IRQn, USART3_IRQHandler);
	}
	uart->base.rx = rx;
	if (rx)
	{
		UBPower_EnablePower(rx->port.base.handle);
		UBGpioPin_SetFunction(rx, func);
	}
	uart->base.tx = tx;
	if (tx)
	{
		UBPower_EnablePower(tx->port.base.handle);
		UBGpioPin_SetFunction(tx, func);
	}
	uart->baudrate = 1000000;
	uart->state = UBUS_NOT_INITIALIZED;
	uart->base.rxFifo = rxFifo;
	uart->base.txFifo = txFifo;
	UBMutex_Unlock(&uart->base.peri.mutex);
}

void UBUart_EnableInt(UBUart* uart)
{
	if (uart == 0)
		return;
	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return;
	if (uart->base.peri.handle == USART1)
		UBInt_EnableInt(USART1_IRQn);
	if (uart->base.peri.handle == USART2)
		UBInt_EnableInt(USART2_IRQn);
	if (uart->base.peri.handle == USART3)
		UBInt_EnableInt(USART3_IRQn);
	UBMutex_Unlock(&uart->base.peri.mutex);
}

void UBUart_DisableInt(UBUart* uart)
{
	if (uart == 0)
		return;
	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return;
	if (uart->base.peri.handle == USART1)
		UBInt_DisableInt(USART1_IRQn);
	if (uart->base.peri.handle == USART2)
		UBInt_DisableInt(USART2_IRQn);
	if (uart->base.peri.handle == USART3)
		UBInt_DisableInt(USART3_IRQn);
	UBMutex_Unlock(&uart->base.peri.mutex);
}

UBUartState UBUart_GetState(UBUart* uart)
{
	if (uart == 0)
		return UBUS_NOT_CONFIGURED;
	return uart->state;
}

UBUartResult UBUart_Init(UBUart* uart, UBFrequency baudrate)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

	UBPower_EnablePower(uart->base.peri.handle);
	UBUart_EnableInt(uart);

	UBUart_SetBaudrate(uart, baudrate);

	USART_TypeDef* u = uart->base.peri.handle;
	u->CR2 = 0;
	u->CR3 = 0;
	u->CR1 = USART_CR1_RXNEIE | USART_CR1_RE | USART_CR1_UE;
	uart->state = UBUS_IDLE;
	UBMutex_Unlock(&uart->base.peri.mutex);
	return UBUR_OK;
}

UBUartResult UBUart_SetBaudrate(UBUart* uart, UBFrequency baudrate)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

	USART_TypeDef* u = uart->base.peri.handle;
	UBFrequency periFreq = UBClock_GetInputFrequency(uart->base.peri.handle);
	u->BRR = periFreq / baudrate;

	uart->baudrate = periFreq / u->BRR;

    UBMutex_Unlock(&uart->base.peri.mutex);

	return UBUR_OK;
}

UBUartResult UBUart_Enqueue(UBUart* uart, const uint8_t* data, uint32_t size, uint32_t* enqueued)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

	uint32_t cnt = 0;

	if ((size) && (uart->base.txFifo))
		cnt = UBFifo_Put(uart->base.txFifo, data, size);

	if (enqueued)
		*enqueued = cnt;

    UBMutex_Unlock(&uart->base.peri.mutex);

	return UBUR_OK;
}

uint32_t UBUart_TxFifoAvailable(UBUart* uart)
{
	if (uart == 0)
		return 0;
	return UBFifo_GetAvailable(uart->base.txFifo);
}

UBUartResult UBUart_Transmit(UBUart* uart)
{
	if (uart == 0)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_NOT_CONFIGURED)
		return UBUR_NOT_CONFIGURED;

	if (uart->state == UBUS_BUSY)
		return UBUR_BUSY;

	if (!UBMutex_TryLock(&uart->base.peri.mutex))
		return UBUR_LOCKED;

	if ((uart->base.txFifo) && (UBFifo_GetCount(uart->base.txFifo)))
	{
		USART_TypeDef* u = uart->base.peri.handle;
		u->CR1 |= USART_CR1_TE | USART_CR1_TXEIE | USART_CR1_TCIE;
	}

    UBMutex_Unlock(&uart->base.peri.mutex);

	return UBUR_OK;
}

#ifdef __cplusplus
}
#endif
