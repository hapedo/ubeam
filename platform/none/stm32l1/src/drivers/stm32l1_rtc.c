/*******************************************************************************
Copyright (C) 2020 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   stm32l1_rtc.c
 * \brief  microBeam SDK
 * \date   31.7.2020
 * \note
 * \todo
 */
#include <ubeam/drivers/rtc.h>
#include <ubeam/drivers/power_management.h>
#include <ubeam/drivers/clock_management.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/utils/system_time.h>
#include <ubeam/utils/intrinsics.h>
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define ALARM_COUNT				(2)

void TAMPER_STAMP_IRQHandler()
{

}

void RTC_WKUP_IRQHandler()
{

}

void UBRtc_Configure(UBRtc* rtc, UBPeriHandle handle)
{
	if (rtc == 0)
		return;
	rtc->base.platform = 0;
    UBPeri_Configure(&rtc->base.peri, handle);
	if (!UBMutex_TryLock(&rtc->base.peri.mutex))
		return;
    rtc->state = UBRTCS_NOT_INITIALIZED;
    UBMutex_Unlock(&rtc->base.peri.mutex);
}

void UBRtc_EnableInt(UBRtc* rtc)
{
    UBUnused(rtc);
}

void UBRtc_DisableInt(UBRtc* rtc)
{
    UBUnused(rtc);
}

UBRtcState UBRtc_GetState(UBRtc* rtc)
{
	if (rtc == 0)
		return UBRTCS_NOT_CONFIGURED;
	return rtc->state;
}

UBRtcResult UBRtc_Init(UBRtc* rtc, bool startRtc)
{
    UBUnused(startRtc);
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    UBNvic_Hook(TAMPER_STAMP_IRQn, TAMPER_STAMP_IRQHandler);
    UBNvic_Hook(RTC_WKUP_IRQn, RTC_WKUP_IRQHandler);

    RCC_TypeDef* c = RCC;

    PWR->CR |= PWR_CR_DBP | PWR_CR_CWUF;

    c->CSR |= RCC_CSR_RTCEN;
    RTC_TypeDef* r = rtc->base.peri.handle;

    UBPower_EnablePower(rtc->base.peri.handle);

    r->WPR = 0xca;
    r->WPR = 0x53;

    r->ISR |= RTC_ISR_INIT;

    while((r->ISR & RTC_ISR_INITF) == 0)
    {
    }

    if (((RCC->CSR >> 16) & 0x03) == 1)
    {
    	// LSE, for 32.768 kHz, async 127, 255 sync
    	r->PRER = 0x000000ff;
    	r->PRER = 0x007f00ff;
    }
    else if (((RCC->CSR >> 16) & 0x03) == 2)
    {
    	// LSI, for 37 kHz, async 127, 255 sync
    	r->PRER = 0x00000127;
    	r->PRER = 0x007c0127;
    }
    else
    {
    	// This is not supported for now
        r->ISR &= ~RTC_ISR_INIT;
        r->WPR = 0x00;

        while((r->ISR & RTC_ISR_INITF) != 0)
        {
        }
        UBMutex_Unlock(&rtc->base.peri.mutex);
        return UBRTCR_ERROR;
    }

    r->ISR &= ~(RTC_ISR_ALRBF | RTC_ISR_ALRAF | RTC_ISR_WUTF);

	r->ISR &= ~RTC_ISR_INIT;

	r->WPR = 0xff;

	while((r->ISR & RTC_ISR_INITF) != 0)
	{
	}

    PWR->CR &= ~PWR_CR_DBP;
    rtc->state = UBRTCS_READY;
    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_Start(UBRtc* rtc)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_Stop(UBRtc* rtc)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_IsRunning(UBRtc* rtc, bool* isRunning)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (isRunning == 0)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    if (r->ISR & RTC_ISR_INIT)
    	*isRunning = false;
    else
    	*isRunning = true;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetTime(UBRtc* rtc, UBRtcDateTime* dateTime)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (dateTime == 0)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    dateTime->hour = ((r->TR >> 20) & 0x03) * 10 + ((r->TR >> 16) & 0x0f);
    dateTime->minute = ((r->TR >> 12) & 0x07) * 10 + ((r->TR >> 8) & 0x0f);
    dateTime->second = ((r->TR >> 4) & 0x07) * 10 + (r->TR & 0x0f);

    dateTime->year = 2000 + ((r->DR >> 20) & 0x0f) * 10 + ((r->DR >> 16) & 0x0f);
    dateTime->month = ((r->DR >> 12) & 0x01) * 10 + ((r->DR >> 8) & 0x0f);
    dateTime->day = ((r->DR >> 4) & 0x03) * 10 + (r->DR & 0x0f);

    uint8_t wd = (r->DR >> 13) & 0x07;
    if (wd == 7)
    	dateTime->weekDay = UBRTCWD_SUNDAY;
    else
    	dateTime->weekDay = (UBRtcWeekDay)wd;

    dateTime->milli = 0;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_SetTime(UBRtc* rtc, const UBRtcDateTime* dateTime, bool setDate)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (dateTime == 0)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    r->ISR |= RTC_ISR_INIT;

    while((r->ISR & RTC_ISR_INITF) == 0)
    {
    }

    r->TR = ((dateTime->hour / 10) << 20) |
    		((dateTime->hour % 10) << 16) |
			((dateTime->minute / 10) << 12) |
			((dateTime->minute % 10) << 8) |
			((dateTime->second / 10) << 4) |
			(dateTime->second % 10);

    if (setDate)
    {
    	uint16_t y = dateTime->year % 100;
    	uint8_t wd = dateTime->weekDay;
    	if (wd == 0)
    		wd = 7;
    	r->DR = ((y / 10) << 20) |
    		    ((y % 10) << 16) |
			    (wd << 13) |
				((dateTime->month / 10) << 12) |
				((dateTime->month % 10) << 8) |
				((dateTime->day / 10) << 4) |
				(dateTime->day % 10);
    }

    r->CR &= ~ RTC_CR_FMT;

    r->TAFCR = 0;

    r->ISR &= ~RTC_ISR_INIT;

    r->WPR = 0xff;

    while((r->ISR & RTC_ISR_INITF) != 0)
    {
    }

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetMaxAlarms(UBRtc* rtc, uint8_t* count)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (count == 0)
        return UBRTCR_BAD_PARAM;

    *count = ALARM_COUNT;

    return UBRTCR_OK;
}

UBRtcResult UBRtc_SetAlarm(UBRtc* rtc, uint8_t index, const UBRtcAlarm* alarm, bool enable)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if ((alarm == 0) || (index >= ALARM_COUNT))
        return UBRTCR_BAD_PARAM;

    if (alarm->dayMaskEnabled)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    if (index == 0)
    {
    	r->CR &= ~(RTC_CR_ALRAE | RTC_CR_ALRAIE);
    	r->ISR &= ~(RTC_ISR_ALRAF);
    }
    else
    {
    	r->CR &= ~(RTC_CR_ALRBE | RTC_CR_ALRBIE);
    	r->ISR &= ~(RTC_ISR_ALRBF);
    }

    uint32_t val = 0;

    if (alarm->dateEnabled)
    {
    	val |= ((alarm->day / 10) & 0x03) << 28;
    	val |= ((alarm->day % 10) & 0x0f) << 24;
    }
    else
    	val |= RTC_ALRMAR_MSK4;

    val |= ((alarm->hour / 10) & 0x03) << 20;
    val |= ((alarm->hour % 10) & 0x0f) << 16;
    val |= ((alarm->minute / 10) & 0x07) << 12;
    val |= ((alarm->minute % 10) & 0x0f) << 8;

    if (index == 0)
    	r->ALRMAR = val;
    else
    	r->ALRMBR = val;

    if (enable)
    {
        if (index == 0)
        {
        	r->ISR &= ~(RTC_ISR_ALRAF);
        	r->CR |= RTC_CR_ALRAE | RTC_CR_ALRAIE;
        }
        else
        {
        	r->ISR &= ~(RTC_ISR_ALRBF);
        	r->CR |= RTC_CR_ALRBE | RTC_CR_ALRBIE;
        }
    }

    r->WPR = 0xff;

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetAlarm(UBRtc* rtc, uint8_t index, UBRtcAlarm* alarm)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if ((alarm == 0) || (index >= ALARM_COUNT))
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    uint32_t val = 0;

    if (index == 0)
    	val = r->ALRMAR;
    else
    	val = r->ALRMBR;

    alarm->day = 1;
    alarm->month = 1;
    if (val & RTC_ALRMAR_MSK4)
    	alarm->dateEnabled = false;
    else
    {
    	alarm->dateEnabled = true;
    	alarm->day = ((val >> 28) & 0x03) * 10 + ((val >> 20) & 0x0f);
    	UBRtcDateTime tm;
    	UBRtc_GetTime(rtc, &tm);
    	alarm->month = tm.month;
    }
    alarm->dayMaskEnabled = false;
    alarm->dayMask = 0;
	alarm->hour = ((val >> 20) & 0x03) * 10 + ((val >> 16) & 0x0f);
	alarm->minute = ((val >> 12) & 0x07) * 10 + ((val >> 8) & 0x0f);

    UBMutex_Unlock(&rtc->base.peri.mutex);

    return UBRTCR_OK;
}

UBRtcResult UBRtc_EnableAlarm(UBRtc* rtc, uint8_t index)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (index >= ALARM_COUNT)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    if (index == 0)
    {
    	r->ISR &= ~(RTC_ISR_ALRAF);
    	r->CR |= RTC_CR_ALRAE | RTC_CR_ALRAIE;
    }
    else
    {
    	r->ISR &= ~(RTC_ISR_ALRBF);
    	r->CR |= RTC_CR_ALRBE | RTC_CR_ALRBIE;
    }

    r->WPR = 0xff;

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);

    return UBRTCR_OK;
}

UBRtcResult UBRtc_DisableAlarm(UBRtc* rtc, uint8_t index)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (index >= ALARM_COUNT)
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    if (index == 0)
    {
    	r->CR &= ~(RTC_CR_ALRAE | RTC_CR_ALRAIE);
    	r->ISR &= ~(RTC_ISR_ALRAF);
    }
    else
    {
    	r->CR &= ~(RTC_CR_ALRBE | RTC_CR_ALRBIE);
    	r->ISR &= ~(RTC_ISR_ALRBF);
    }

    r->WPR = 0xff;

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);

    return UBRTCR_OK;
}

UBRtcResult UBRtc_AlarmIsEnabled(UBRtc* rtc, uint8_t index, bool* isEnabled)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if ((isEnabled == 0) || (index >= ALARM_COUNT))
        return UBRTCR_BAD_PARAM;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    if (index == 0)
    	*isEnabled = (r->CR & RTC_CR_ALRAE) != 0;
    else
    	*isEnabled = (r->CR & RTC_CR_ALRBE) != 0;

    UBMutex_Unlock(&rtc->base.peri.mutex);

    return UBRTCR_OK;
}

UBRtcResult UBRtc_SleepAndWaitAlarm(UBRtc* rtc, bool force)
{
	if (!force)
	{
		if (rtc == 0)
			return UBRTCR_NOT_CONFIGURED;

		if (rtc->state == UBRTCS_NOT_CONFIGURED)
			return UBRTCR_NOT_CONFIGURED;

		if (rtc->state == UBRTCS_BUSY)
			return UBRTCR_BUSY;

		if (!UBMutex_TryLock(&rtc->base.peri.mutex))
			return UBRTCR_BUSY;
	}

    UBSysTime_Stop();
    //PWR->CR |= PWR_CR_PDDS | PWR_CR_LPSDSR;
    //PWR->CR &= ~(PWR_CR_PDDS | PWR_CR_LPSDSR);
    //PWR->CR &= ~PWR_CR_LPSDSR;
    PWR->CR |= PWR_CR_PDDS;
    SCB->SCR |= ((uint32_t)SCB_SCR_SLEEPDEEP_Msk);
    __WFI();

    if (!force)
    {
    	UBMutex_Unlock(&rtc->base.peri.mutex);
    }
    return UBRTCR_OK;
}

UBRtcResult UBRtc_StoreBackup(UBRtc* rtc, uint32_t offset, const uint8_t* data, uint32_t size)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    uint32_t devSize = 0;
    UBRtc_GetBackupSize(rtc, &devSize);

    if (size + offset > devSize)
        size = devSize - offset;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    uint8_t shift = (offset & 3) * 8;
    volatile uint32_t* ptrRtc = (volatile uint32_t*)(&r->BKP0R + (offset >> 2));
    uint32_t mask = 0xff << shift;
    while(size)
    {
    	uint32_t val = *ptrRtc;
    	val = (val & (~mask)) | (*data << shift);
    	*ptrRtc = val;
    	shift += 8;
    	mask <<= 8;
    	if (shift == 32)
    	{
    		shift = 0;
    		mask = 0xff;
    		ptrRtc++;
    	}
    	data++;
    	size--;
    }

    r->WPR = 0xff;

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_ReadBackup(UBRtc* rtc, uint32_t offset, uint8_t* data, uint32_t size)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    uint32_t devSize = 0;
    UBRtc_GetBackupSize(rtc, &devSize);

    if (size + offset > devSize)
        size = devSize - offset;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    uint8_t shift = (offset & 3) * 8;
    volatile uint32_t* ptrRtc = (volatile uint32_t*)(&r->BKP0R + (offset >> 2));
    uint32_t mask = 0xff << shift;
    while(size)
    {
    	*data = (*ptrRtc >> shift) & 0xff;
    	shift += 8;
    	mask <<= 8;
    	if (shift == 32)
    	{
    		shift = 0;
    		mask = 0xff;
    		ptrRtc++;
    	}
    	data++;
    	size--;
    }


    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_GetBackupSize(UBRtc* rtc, uint32_t* size)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (size == 0)
        return UBRTCR_BAD_PARAM;

#if defined(RTC_BKP21R)
    *size = 128;
#elif defined(RTC_BKP5R)
    *size = 80;
#else
    *size = 20;
#endif

    return UBRTCR_OK;
}

UBRtcResult UBRtc_ConfigureTamper(UBRtc* rtc, bool disablePullup, URtcTamperPrech precharge, URtcTamperFlt filter, UBRtcTamperFreq sampleFreq)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    r->ISR |= RTC_ISR_INIT;

    while((r->ISR & RTC_ISR_INITF) == 0)
    {
    }

    uint32_t v = 0;

    if (disablePullup)
    	v |= RTC_TAFCR_TAMPPUDIS;

    v |= (uint32_t)precharge << 13;
    v |= (uint32_t)filter << 11;
    v |= (uint32_t)sampleFreq << 8;

    r->TAFCR = v;

    r->ISR &= ~RTC_ISR_INIT;

    r->WPR = 0xff;

    while((r->ISR & RTC_ISR_INITF) != 0)
    {
    }

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_ConfigureTamperPin(UBRtc* rtc, UBRtcTamper tamper, bool activeHigh)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    r->ISR |= RTC_ISR_INIT;

    while((r->ISR & RTC_ISR_INITF) == 0)
    {
    }

    bool edge = ((r->TAFCR >> 11) & 0x03) == 0;

    if (tamper == UBRTCTAMP_1)
    {
    	if (edge)
    	{
        	if (activeHigh)
        		r->TAFCR &= ~RTC_TAFCR_TAMP1TRG;
        	else
        		r->TAFCR |= RTC_TAFCR_TAMP1TRG;
    	}
    	else
    	{
			if (!activeHigh)
				r->TAFCR &= ~RTC_TAFCR_TAMP1TRG;
			else
				r->TAFCR |= RTC_TAFCR_TAMP1TRG;
    	}
    	r->ISR &= ~(RTC_ISR_TAMP1F);
    }
    else if (tamper == UBRTCTAMP_2)
    {
    	if (edge)
    	{
        	if (activeHigh)
        		r->TAFCR &= ~RTC_TAFCR_TAMP2TRG;
        	else
        		r->TAFCR |= RTC_TAFCR_TAMP2TRG;
    	}
    	else
    	{
			if (!activeHigh)
				r->TAFCR &= ~RTC_TAFCR_TAMP2TRG;
			else
				r->TAFCR |= RTC_TAFCR_TAMP2TRG;
    	}
    	r->ISR &= ~(RTC_ISR_TAMP2F);
    }
    else if (tamper == UBRTCTAMP_3)
    {
    	if (edge)
    	{
        	if (activeHigh)
        		r->TAFCR &= ~RTC_TAFCR_TAMP3TRG;
        	else
        		r->TAFCR |= RTC_TAFCR_TAMP3TRG;
    	}
    	else
    	{
			if (!activeHigh)
				r->TAFCR &= ~RTC_TAFCR_TAMP3TRG;
			else
				r->TAFCR |= RTC_TAFCR_TAMP3TRG;
    	}
    	r->ISR &= ~(RTC_ISR_TAMP3F);
    }

    r->ISR &= ~RTC_ISR_INIT;

    r->WPR = 0xff;

    while((r->ISR & RTC_ISR_INITF) != 0)
    {
    }

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_EnableTamperPin(UBRtc* rtc, UBRtcTamper tamper)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    r->ISR |= RTC_ISR_INIT;

    while((r->ISR & RTC_ISR_INITF) == 0)
    {
    }

    if (tamper == UBRTCTAMP_1)
    {
    	r->ISR &= ~(RTC_ISR_TAMP1F);
   		r->TAFCR |= RTC_TAFCR_TAMP1E | RTC_TAFCR_TAMPIE;
    }
    else if (tamper == UBRTCTAMP_2)
    {
    	r->ISR &= ~(RTC_ISR_TAMP2F);
   		r->TAFCR |= RTC_TAFCR_TAMP2E | RTC_TAFCR_TAMPIE;
    }
    else if (tamper == UBRTCTAMP_3)
    {
    	r->ISR &= ~(RTC_ISR_TAMP3F);
   		r->TAFCR |= RTC_TAFCR_TAMP3E | RTC_TAFCR_TAMPIE;
    }

    r->ISR &= ~RTC_ISR_INIT;

    r->WPR = 0xff;

    while((r->ISR & RTC_ISR_INITF) != 0)
    {
    }

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

UBRtcResult UBRtc_DisableTamperPin(UBRtc* rtc, UBRtcTamper tamper)
{
    if (rtc == 0)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return UBRTCR_NOT_CONFIGURED;

    if (rtc->state == UBRTCS_BUSY)
        return UBRTCR_BUSY;

    if (!UBMutex_TryLock(&rtc->base.peri.mutex))
        return UBRTCR_BUSY;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    PWR->CR |= PWR_CR_DBP;

    r->WPR = 0xca;
    r->WPR = 0x53;

    r->ISR |= RTC_ISR_INIT;

    while((r->ISR & RTC_ISR_INITF) == 0)
    {
    }

    if (tamper == UBRTCTAMP_1)
    {
   		r->TAFCR &= ~(RTC_TAFCR_TAMP1E | RTC_TAFCR_TAMPIE);
    	r->ISR &= ~(RTC_ISR_TAMP1F);
    }
    else if (tamper == UBRTCTAMP_2)
    {
   		r->TAFCR &= ~(RTC_TAFCR_TAMP2E | RTC_TAFCR_TAMPIE);
    	r->ISR &= ~(RTC_ISR_TAMP2F);
    }
    else if (tamper == UBRTCTAMP_3)
    {
   		r->TAFCR &= ~(RTC_TAFCR_TAMP3E | RTC_TAFCR_TAMPIE);
    	r->ISR &= ~(RTC_ISR_TAMP3F);
    }

    r->ISR &= ~RTC_ISR_INIT;

    r->WPR = 0xff;

    while((r->ISR & RTC_ISR_INITF) != 0)
    {
    }

    PWR->CR &= ~PWR_CR_DBP;

    UBMutex_Unlock(&rtc->base.peri.mutex);
    return UBRTCR_OK;
}

bool UBRtc_IsTamperDetected(UBRtc* rtc, UBRtcTamper tamper, bool clearFlag)
{
    if (rtc == 0)
        return false;

    if (rtc->state == UBRTCS_NOT_CONFIGURED)
        return false;

    RTC_TypeDef* r = (RTC_TypeDef*)rtc->base.peri.handle;

    if ((tamper == UBRTCTAMP_1) && (r->ISR & RTC_ISR_TAMP1F))
    {
    	if (clearFlag)
    		r->ISR &= ~RTC_ISR_TAMP1F;
    	return true;
    }
    else if ((tamper == UBRTCTAMP_2) && (r->ISR & RTC_ISR_TAMP2F))
    {
    	if (clearFlag)
    		r->ISR &= ~RTC_ISR_TAMP2F;
    	return true;
    }
    else if ((tamper == UBRTCTAMP_3) && (r->ISR & RTC_ISR_TAMP3F))
    {
    	if (clearFlag)
    		r->ISR &= ~RTC_ISR_TAMP3F;
    	return true;
    }
    return false;
}

#ifdef __cplusplus
}
#endif
