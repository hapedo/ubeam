/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   system_time.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_CORTEX_M_UTILS_SYSTEM_TIME_H_
#define _UBEAM_CORTEX_M_UTILS_SYSTEM_TIME_H_

#include <ubeam/setup.h>

/*! \defgroup ubeam_utils_systime_cortexm ARM Cortex-M system time
 *  \ingroup ubeam_utils_systime
 *  \brief ARM Cortex-M system time functions
 */

/*! \addtogroup ubeam_utils_systime_cortexm
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure Cortex-M systick as system time source
 */
void UBCortexSysTime_Configure();

/*! Stop Cortex-M systick
 */
void UBCortexSysTime_Stop();

/*! Get Cortex-M ticks per second value
 *  \return Ticks per second value
 */
uint64_t UBCortexSysTime_GetTicksPerSec();

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_CORTEX_M_UTILS_SYSTEM_TIME_H_
