/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   systick_timer.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_CORTEX_M_DRIVERS_SYSTICK_TIMER_H_
#define _UBEAM_CORTEX_M_DRIVERS_SYSTICK_TIMER_H_

#include <ubeam/setup.h>
#include <ubeam/drivers/peripheral.h>

/*! \defgroup ubeam_drivers_cortexm_systick ARM Cortex-M systick
 *  \ingroup ubeam_drivers_cortexm
 *  \brief ARM Cortex-M systick driver
 */

/*! \addtogroup ubeam_drivers_cortexm_systick
 *  @{
 */

/*! Systick timer timeout callback
 *  \param systick Systick instance ptr
 */
typedef void(UBCortexSysTickTimeoutCb)(void* systick);

//! Cortex-M systick timer driver struct
typedef struct
{
	UBPeripheral peri;							//!< Peripheral struct instance
	UBCortexSysTickTimeoutCb* timeoutCb;		//!< Timeout callback
} UBCortexSysTick;

#ifdef __cplusplus
extern "C"
{
#endif

/*! Configure Cortex-M systick driver
 *  \param systick Systick driver struct instance ptr
 *  \param reloadValue Systick reload value
 *  \param bool refClock Use reference clock (external clock / alternative clock input)
 */
void UBCortexSysTick_Configure(UBCortexSysTick* systick, uint32_t reloadValue, bool refClock);

/*! Enable systick interrupts
 *  \param systick Systick driver struct instance ptr
 */
void UBCortexSysTick_EnableInt(UBCortexSysTick* systick);

/*! Disable systick interrupts
 *  \param systick Systick driver struct instance ptr
 */
void UBCortexSysTick_DisableInt(UBCortexSysTick* systick);

/*! Get systick input frequency in Hz
 *  \param systick Systick driver struct instance ptr
 *  \return Systick input frequency in Hz
 */
UBFrequency UBCortexSysTick_GetInputFrequency(UBCortexSysTick* systick);

/*! Reset systick counter to specific value
 *  \param systick Systick driver struct instance ptr
 *  \param initialValue Value to be set
 */
void UBCortexSysTick_Reset(UBCortexSysTick* systick, uint32_t initialValue);

/*! Start systick timer
 *  \param systick Systick driver struct instance ptr
 */
void UBCortexSysTick_Start(UBCortexSysTick* systick);

/*! Stop systick timer
 *  \param systick Systick driver struct instance ptr
 */
void UBCortexSysTick_Stop(UBCortexSysTick* systick);

/*! Get current systick value
 *  \param systick Systick driver struct instance ptr
 *  \return Current systick value
 */

uint32_t UBCortexSysTick_GetTicks(UBCortexSysTick*);

/*! Get current UBCortexSysTick struct ptr (set by UBCortexSysTick_Configure()). There is only one systick on Cortex-M systems.
 *  \param systick Systick driver struct instance ptr
 */
UBCortexSysTick* UBCortexSysTick_GetInstance();

#ifdef __cplusplus
}
#endif

/*! @} */

#endif //_UBEAM_CORTEX_M_DRIVERS_SYSTICK_TIMER_H_
