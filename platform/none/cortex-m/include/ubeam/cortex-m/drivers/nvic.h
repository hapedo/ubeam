/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   nvic.h
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#ifndef _UBEAM_CORTEX_M_DRIVERS_NVIC_H_
#define _UBEAM_CORTEX_M_DRIVERS_NVIC_H_

#include <stdbool.h>
#include <ubeam/setup.h>
#include <ubeam/drivers/interrupt_ctrl.h>

/*! \defgroup ubeam_drivers_cortexm_nvic ARM Cortex-M NVIC
 *  \ingroup ubeam_drivers_cortexm
 *  \brief ARM Cortex-M NVIC driver
 */

/*! \addtogroup ubeam_drivers_cortexm_nvic
 *  @{
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*! Check whether interrupt is pending
 *  \param id Interrupt ID to be checked
 *  \return true when interrupt is pending
 */
bool UBNvic_IsPending(UBIntId id);

/*! Set interrupt to be pending
 *  \param id Interrupt ID to be set
 */
void UBNvic_SetPending(UBIntId id);

/*! Set interrupt to be not pending
 *  \param id Interrupt ID to be set
 */
void UBNvic_ClearPending(UBIntId id);

/*! Set interrupt priority
 *  \param id Interrupt ID to be set
 *	\param prio Priority to be set
 */
void UBNvic_SetPriority(UBIntId id, UBIntPrio prio);

/*! Get interrupt priority
 *  \param id Interrupt ID to be get
 *  \return Interrupt priority
 */
UBIntPrio UBNvic_GetPriority(UBIntId id);

/*! Reset system. This function does not return.
 */
void UBNvic_SystemReset();

/*! Hook interrupt handler. May not be supported by all platforms (requires vector table remap support)
 *  \param id Interrupt ID
 *  \param handler Interrupt handler function ptr
 */
bool UBNvic_Hook(UBIntId id, void (*handler)(void));

/*! Unhook interrupt handler. May not be supported by all platforms (requires vector table remap support)
 *  \param id Interrupt ID
 *  \param handler Interrupt handler function ptr
 */
bool UBNvic_Unhook(UBIntId id);

/*! Remap vector table to new location. May not be supported by all platforms (requires vector table remap support)
 *  \param newVectorTable Ptr to new vector table to remap to (please follow memory align rules given by platform)
 *  \param itemsCount Number of vectors in table
 *  \param defaultVectorTable Ptr to original vector table. Original vector table is copied to new vector table location when defaultVectorTable not NULL.
 */
bool UBNvic_RemapVectorTable(uint32_t* newVectorTable, unsigned int itemsCount, uint32_t* defaultVectorTable);

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _UBEAM_CORTEX_M_DRIVERS_NVIC_H_ 
