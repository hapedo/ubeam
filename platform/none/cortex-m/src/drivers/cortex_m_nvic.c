/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   cortex_m_nvic.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/cortex-m/drivers/nvic.h>

#if (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32F0)
#include <ubeam/stm32f0/drivers/definitions.h>
#include <ubeam/cortex-m/3rd_party/CMSIS/core_cm0.h>
#elif (UBEAM_CPU_FAMILY == UBEAM_CPU_FAMILY_STM32L1)
#include <ubeam/stm32l1/drivers/definitions.h>
#include <ubeam/cortex-m/3rd_party/CMSIS/core_cm3.h>
#endif
#include <ubeam/cortex-m/3rd_party/CMSIS/core_cmFunc.h>

#ifdef __cplusplus
extern "C"
{
#endif

#if (UBEAM_CPU_CORE_TYPE != UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
static uint32_t* s_vectorTable = 0;
static unsigned int s_itemsCount = 0;
#endif

bool s_isGlobalEnabled = false;

void NMI_Handler()
{
}

void HardFault_Handler()
{
    while (1)
    {
    }
}

void MemManage_Handler()
{
    while (1)
    {
    }
}

void BusFault_Handler()
{
    while (1)
    {
    }
}

void UsageFault_Handler()
{
    while (1)
    {
    }
}

void SVC_Handler()
{
}

void NvicDefaultHandler()
{
    while(1)
    {
    }
}

void DebugMon_Handler()
{
}

void PendSV_Handler()
{
}

void UBInt_EnableGlobal()
{
	s_isGlobalEnabled = true;
	__enable_irq();
}

bool UBInt_DisableGlobal()
{
	__disable_irq();
	bool result = s_isGlobalEnabled;
	s_isGlobalEnabled = false;
	return result;
}

void UBInt_EnableInt(UBIntId id)
{
	NVIC_EnableIRQ((IRQn_Type)id);
}

void UBInt_DisableInt(UBIntId id)
{
	NVIC_DisableIRQ((IRQn_Type)id);
}

bool UBNvic_IsPending(UBIntId id)
{
	return NVIC_GetPendingIRQ((IRQn_Type)id) == 1;
}

void UBNvic_SetPending(UBIntId id)
{
	NVIC_SetPendingIRQ((IRQn_Type)id);
}

void UBNvic_ClearPending(UBIntId id)
{
	NVIC_ClearPendingIRQ((IRQn_Type)id);
}

void UBNvic_SetPriority(UBIntId id, UBIntPrio prio)
{
	NVIC_SetPriority((IRQn_Type)id, prio);
}

UBIntPrio UBNvic_GetPriority(UBIntId id)
{
	return NVIC_GetPriority((IRQn_Type)id);
}

void UBNvic_SystemReset()
{
	NVIC_SystemReset();
}

bool UBNvic_Hook(UBIntId id, void (*handler)(void))
{
#if (UBEAM_CPU_CORE_TYPE != UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
    if ((s_vectorTable) && (id < s_itemsCount))
    {
        s_vectorTable[(int)id + 16] = (uint32_t)handler;
        return true;
    }
#endif
    return false;
}

bool UBNvic_Unhook(UBIntId id)
{
#if (UBEAM_CPU_CORE_TYPE != UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
	 if ((s_vectorTable) && (id < s_itemsCount))
    {
        s_vectorTable[(int)id + 16] = (uint32_t)&NvicDefaultHandler;
        return true;
    }
#endif
    return false;
}

bool UBNvic_RemapVectorTable(uint32_t* newVectorTable, unsigned int itemsCount, uint32_t* defaultVectorTable)
{
#if (UBEAM_CPU_CORE_TYPE != UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0)
    if (itemsCount >= 16)
    {
        // Check alignment
        if ((((uint32_t)newVectorTable & 0x3f)) != 0)
            return -1;

        s_itemsCount = itemsCount;
        s_vectorTable = newVectorTable;
        if (defaultVectorTable)
        {
            // Copy default vector table to remapped table
            for (int i = 0; i < itemsCount; i++)
                s_vectorTable[i] = defaultVectorTable[i];
        }
        else
        {
            // Initial SP
            newVectorTable[0] = 0;
            // Reset vector
            newVectorTable[1] = 0;
        }
        SCB->VTOR = (uint32_t)s_vectorTable;
        return 0;
    }
#endif
    return -1;
}

#ifdef __cplusplus
}
#endif
