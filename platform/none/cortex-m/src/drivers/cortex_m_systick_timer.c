/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   cortex_m_systick_timer.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/cortex-m/drivers/systick_timer.h>
#include <ubeam/cortex-m/drivers/nvic.h>
#include <ubeam/drivers/interrupt_ctrl.h>
#include <ubeam/drivers/clock_management.h>

#if ((UBEAM_CPU_CORE_TYPE == UBEAM_CPU_CORE_TYPE_ARM_CORTEX_M0) && (UBEAM_CPU_MANUF == UBEAM_CPU_MANUF_STM))
#include <ubeam/stm32f0/drivers/definitions.h>
#include <ubeam/cortex-m/3rd_party/CMSIS/core_cm0.h>
#endif
#include <ubeam/cortex-m/3rd_party/CMSIS/core_cmFunc.h>

#ifdef __cplusplus
extern "C"
{
#endif

static UBCortexSysTick* s_instance = 0;

void SysTick_Handler()
{
    if ((s_instance) && (s_instance->timeoutCb))
    	s_instance->timeoutCb(s_instance);
}

void UBCortexSysTick_Configure(UBCortexSysTick* systick, uint32_t reloadValue, bool refClock)
{
    UBPeri_Configure(&systick->peri, SysTick);

    if (systick == 0)
            return;

    if (!UBMutex_TryLock(&systick->peri.mutex))
            return;

    UBCortexSysTick_Stop(systick);
    s_instance = systick;
    //sysTickIrqCallback_ = bindCallbackMethod(&SysTickTimer::irqCallback, this);
    SysTick->LOAD = reloadValue;
    SysTick->VAL = 0;
    if (refClock == false)
        SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
    else
        SysTick->CTRL &= ~SysTick_CTRL_CLKSOURCE_Msk;

    systick->timeoutCb = 0;
    UBCortexSysTick_DisableInt(systick);
    UBNvic_Hook(SysTick_IRQn, SysTick_Handler);

    UBMutex_Unlock(&systick->peri.mutex);
}

void UBCortexSysTick_EnableInt(UBCortexSysTick* systick)
{
	UBInt_EnableInt(SysTick_IRQn);
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
}

void UBCortexSysTick_DisableInt(UBCortexSysTick* systick)
{
	SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
	UBInt_DisableInt(SysTick_IRQn);
}

UBFrequency UBCortexSysTick_GetInputFrequency(UBCortexSysTick* systick)
{
	return UBClock_GetInputFrequency(SysTick);
}

void UBCortexSysTick_Reset(UBCortexSysTick* systick, uint32_t initialValue)
{
	if (systick == 0)
		return;

	if (!UBMutex_TryLock(&systick->peri.mutex))
		return;

	SysTick->VAL = initialValue;

	UBMutex_Unlock(&systick->peri.mutex);
}

void UBCortexSysTick_Start(UBCortexSysTick* systick)
{
	if (systick == 0)
		return;

	if (!UBMutex_TryLock(&systick->peri.mutex))
		return;

	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

	UBMutex_Unlock(&systick->peri.mutex);
}

void UBCortexSysTick_Stop(UBCortexSysTick* systick)
{
	if (systick == 0)
		return;

	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;

	if (!UBMutex_TryLock(&systick->peri.mutex))
		return;

	UBMutex_Unlock(&systick->peri.mutex);
}

uint32_t UBCortexSysTick_GetTicks(UBCortexSysTick* systick)
{
	return SysTick->LOAD - SysTick->VAL;
}

UBCortexSysTick* UBCortexSysTick_GetInstance()
{
	return s_instance;
}

#ifdef __cplusplus
}
#endif
