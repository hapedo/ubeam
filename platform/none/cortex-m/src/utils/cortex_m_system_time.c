/*******************************************************************************
Copyright (C) 2019 by Petr Hapal
petr@hapal.cz

*Licensed under MIT*

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
/*!
 * \file   cortex_m_system_time.c
 * \brief  microBeam SDK
 * \date   27.10.2019
 * \note
 * \todo
 */
#include <ubeam/cortex-m/utils/system_time.h>
#include <ubeam/cortex-m/drivers/systick_timer.h>
#include <ubeam/drivers/clock_management.h>

#ifdef __cplusplus
extern "C"
{
#endif

static UBCortexSysTick s_systick;
static uint64_t s_epoch = 0;

static void SystickCallback(void* systick)
{
    s_epoch += 0x0000000001000000;
}

void UBCortexSysTime_Configure()
{
    UBCortexSysTick_Configure(&s_systick, 0x00ffffff, false);
    s_epoch = 0;
    s_systick.timeoutCb = SystickCallback;
    UBCortexSysTick_EnableInt(&s_systick);
    UBCortexSysTick_Start(&s_systick);
}

void UBCortexSysTime_Stop()
{
	UBCortexSysTick_Stop(&s_systick);
}

uint64_t UBCortexSysTime_GetTicksPerSec()
{
    return UBClock_GetInputFrequency(s_systick.peri.handle);
}

UBTimestamp UBSysTime_GetTimestamp()
{
    uint64_t epoch = s_epoch;
    uint64_t ticks = UBCortexSysTick_GetTicks(&s_systick);
    if (s_epoch != epoch)
    {
        epoch = s_epoch;
        ticks = UBCortexSysTick_GetTicks(&s_systick);
    }
    return epoch | ticks;
}

#ifdef __cplusplus
}
#endif
